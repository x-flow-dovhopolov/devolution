﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FabrosConfig : ScriptableObject
{
    public const string Version = "1.0.0";
    
    private static FabrosConfig _instance;

    public static FabrosConfig Instance{
        get{
            if (!_instance){
                _instance = (FabrosConfig) Resources.Load<FabrosConfig>("FabrosConfig");
            }

            return _instance;
        }
    }
    
    [Serializable]
    public class ConfigStruct
    {
        public string SpreadsheetID;
        public string ClassName;
        public string FileName;
        public string CodeFilePath;
    }


    public List<ConfigStruct> configs;

    public string credentials;
}
