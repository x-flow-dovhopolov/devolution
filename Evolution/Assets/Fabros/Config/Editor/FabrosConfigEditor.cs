﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(FabrosConfig))]
public class FabrosConfigEditor : UnityEditor.Editor{
    private FabrosConfig _settings;

    public override void OnInspectorGUI(){
        _settings = (FabrosConfig) target;

        if (_settings == null)
            return;

        if (_settings.configs == null)
        {
            _settings.configs = new List<FabrosConfig.ConfigStruct>();
        }

        if (_settings.configs.Count == 0)
        {
            _settings.configs.Add(new FabrosConfig.ConfigStruct());
        }
      
        
        _settings.credentials = EditorGUILayout.TextField(new GUIContent("Credentials", "Path to google service .json file. \nEx: Assets/fabros-service.json"), _settings.credentials);
            
        GUILayout.Space(10);
        
        
         foreach (var item in _settings.configs)
            {
                GUILayout.BeginHorizontal();
                
                item.ClassName = EditorGUILayout.TextField(new GUIContent("Class", "Desired class name. \nEx: GameDB"), item.ClassName );

                if (_settings.configs.Count > 1)
                {
                    
                    GUI.backgroundColor = Color.red;
                    if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.Height(15)))
                    {
                        if (EditorUtility.DisplayDialog("Delete Confirmation", $"Are you sure you want to delete {item.ClassName} config?", "Yes", "No"))
                        {
                            _settings.configs.Remove(item);
                            break;
                        }
                    }
                    GUI.backgroundColor = Color.white;
                }

                GUILayout.EndHorizontal();
                item.SpreadsheetID = EditorGUILayout.TextField(new GUIContent("SpreadsheetID", "Spreadsheet ID from url. \nEx: 12wJNxJkyaFSq-VjeN41BEvUtpfXBH8JrrjxJslTPEPM"), item.SpreadsheetID );
                item.CodeFilePath = EditorGUILayout.TextField(new GUIContent("Code file path", "Code file path. \nEx: 'Assets/Scripts/'"), item.CodeFilePath );
                item.FileName = EditorGUILayout.TextField(new GUIContent("Json File Name", " (will be stored in Resources). \nEx: GameDB"), item.FileName );

                
                GUILayout.BeginHorizontal();

                bool hasPlus = false;
                if (_settings.configs.Last() == item)
                {
                    hasPlus = true;
                    if (GUILayout.Button("+", GUILayout.Width(25), GUILayout.Height(25)))
                    {
                        _settings.configs.Add(new FabrosConfig.ConfigStruct());
                        break;
                    }
                }
                
                bool isLocal =  item.ClassName != null && item.ClassName.IndexOf("locale", StringComparison.OrdinalIgnoreCase) >= 0;
                
                GUILayout.Space(125 + (hasPlus ? 0 : 32));
                
                if (!string.IsNullOrEmpty(item.ClassName) && !string.IsNullOrEmpty(item.CodeFilePath) &&
                    !string.IsNullOrEmpty(item.SpreadsheetID) && !string.IsNullOrEmpty(item.FileName))
                {
                    if (GUILayout.Button("GENERATE", GUILayout.Width(75), GUILayout.Height(25)))
                    {
                        if (_settings.credentials.Length == 0)
                        {
                            Debug.LogError("Credentials field is empty");
                        }
                        else
                        if (!File.Exists(_settings.credentials))
                        {
                            Debug.LogError("Credetials file does not exist: " + _settings.credentials);
                        }
                        else
                        {
                            Fabros.Config.GameDBMenuItems.Generate(_settings.credentials, item.SpreadsheetID, item.ClassName, item.FileName, item.CodeFilePath);
                        }
                    }
                    GUILayout.FlexibleSpace();
                    GUILayout.Space(20);
                    GUI.backgroundColor = Color.blue;
                    if (isLocal && GUILayout.Button("PREPARE", GUILayout.Width(75), GUILayout.Height(25)))
                    {
                        if (_settings.credentials.Length == 0)
                        {
                            Debug.LogError("Credentials field is empty");
                        }
                        else
                        if (!File.Exists(_settings.credentials))
                        {
                            Debug.LogError("Credetials file does not exist: " + _settings.credentials);
                        }
                        else
                        {
                            Fabros.Config.GameDBGenerator.PrepareLocalization(_settings.credentials, item.SpreadsheetID);
                        }
                    }
                    GUILayout.Space(20);
                    GUI.backgroundColor = Color.cyan;
                    if (isLocal && GUILayout.Button("TO JSON", GUILayout.Width(75), GUILayout.Height(25)))
                    {

                        if (!File.Exists(Fabros.Config.GameDBGenerator.LocalizationFile))
                        {
                            Debug.LogError("Localization file does not exist: " + Fabros.Config.GameDBGenerator.LocalizationFile);
                        }
                        else
                        {
                            Fabros.Config.GameDBGenerator.LocalizationConvert();
                        }
                    }

                    GUI.backgroundColor = Color.white;
                    
                    
                }
                GUILayout.EndHorizontal();
                
                
                
                GUILayout.Space(20);
            }
        
        SerializedObject serializedSettings = new UnityEditor.SerializedObject(_settings);
        serializedSettings.ApplyModifiedProperties();
        if (GUI.changed){
            EditorUtility.SetDirty(_settings);
        }
    }
}
