using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Humanizer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using UnityEditor;
using UnityEngine;
using Color = System.Drawing.Color;

namespace Fabros.Config
{
    public static class GameDBMenuItems
    {
        public static void Generate(string credentials, string spreadsheedID, string className, string jsonFileName, string codeFilePath)
        {
            if (!Directory.Exists(codeFilePath))
            {
                Directory.CreateDirectory(codeFilePath);
            }

            //var fileDir = Path.GetDirectoryName(fileName);
            if (!Directory.Exists("Assets/Resources"))
            {
                Directory.CreateDirectory("Assets/Resources");
            }

            var generator = new GameDBGenerator(className);
            foreach (GameDBSheet sheet in GameDBSheet.LoadFromGoogleDrive(spreadsheedID, credentials))
            {
                if (sheet == null)
                {
                    EditorUtility.ClearProgressBar();
                    return;
                }

                if (sheet.IsStatic)
                {
                    generator.ParseStatic(sheet);
                }
                else
                {
                    generator.Parse(sheet);
                }
            }

            File.WriteAllText(Path.Combine(codeFilePath, className + ".cs"), generator.Schema);
            File.WriteAllText(Path.Combine("Assets/Resources", jsonFileName + ".json"), generator.Data);
            AssetDatabase.Refresh();
        }
    }
    
   

    public class GameDBSheet
    {
        private GameDBSheet(string name, Dictionary<string, string> columns, IEnumerable<Entry> entries, bool isStatic)
        {
            Name = name;
            Columns = columns;
            Entries = entries;
            IsStatic = isStatic;
        }

        public string Name { get; }
        
        public bool IsStatic { get; set; }

        public IEnumerable<Entry> Entries { get; }

        // Column name => column type
        public Dictionary<string, string> Columns { get; }

        // Column name => row value
        public class Entry : Dictionary<string, string> { }
        
        public static IEnumerable<GameDBSheet> LoadFromGoogleDrive(string spreadsheetID, string credentials)
        {
            var service = CreateSheetsService(credentials);
            var spreadsheetId = spreadsheetID;
            var spreadsheet = service.Spreadsheets.Get(spreadsheetId).Execute();
            var sheetCount = spreadsheet.Sheets.Count;
            var processed = 0;
            var progressTitle = $"Total sheet count: {sheetCount}";
            
            
            foreach (var sheet in spreadsheet.Sheets)
            {
                var title = sheet.Properties.Title;
                EditorUtility.DisplayProgressBar(
                    progressTitle,
                    $"Processing {title}",
                    processed++ / (float) sheetCount
                );
                if (title.StartsWith("*"))
                {
                    continue;
                }

                var sheetRequest = service.Spreadsheets.Values.Get(spreadsheetId, title);
                sheetRequest.MajorDimension = SpreadsheetsResource.ValuesResource.GetRequest.MajorDimensionEnum.ROWS;
                var sheetData = sheetRequest.Execute().Values;
                yield return ToDBSheet(title, sheetData);
            }
            EditorUtility.ClearProgressBar();
        }

        private static GameDBSheet ToDBSheet(string title, IList<IList<object>> sheetData)
        {
            var columnNames = sheetData[0].Cast<string>().ToList();

            var temp = new List<string>();
            bool hasDublicates = false;
            bool hasStatic = false;

            foreach (var name in columnNames)
            {
                if (name.Equals("_static"))
                {
                    hasStatic = true;
                }

                if (temp.Contains(name))
                {
                    hasDublicates = true;
                    Debug.LogError("Dublicated column name: " + name);
                    continue;
                }

                temp.Add(name);
            }

            if (hasDublicates)
            {
                return null;
            }

            if (!hasStatic)
            {
                var columnTypes = sheetData[1].Cast<string>().ToList();
                EqualizeLength(columnNames, columnTypes);

                var columnMapping = columnNames
                    .Zip(columnTypes, (name, type) => new { name, type = NormalizeType(type) })
                    .Where(pair => !pair.name.StartsWith("*"))
                    .ToDictionary(pair => pair.name, pair => pair.type);

                columnMapping["id"] = "string";
            
                temp.Clear();
 
                //TODO: Check dublicates
            
                var entries = sheetData
                    .Skip(2)
                    .Select(row => row.Cast<string>().ToList())
                    .Where(row => !row[0].StartsWith("*"))
                    .Select(row => ToEntry(row, columnNames))
                    .ToList();

                return new GameDBSheet(title, columnMapping, entries, hasStatic);
            }
            else
            {
                var columnTypes = new List<string>();
                var columnMapping = new Dictionary<string, string>();
                var entries = new List<Entry>();
                
                foreach (var item in sheetData)
                {   
                    columnTypes.Add(item[1].ToString());
                    columnMapping.Add(item[0].ToString(), item[1].ToString());

                    if (!item[0].Equals("id"))
                    {
                        var ids = new List<string>();
                        ids.Add(item[0].ToString());
                        ids.Add("id");
                        
                        var values = new List<string>();
                        values.Add(item[2].ToString());
                        values.Add(item[0].ToString());
                        
                        entries.Add(ToEntry(values, ids));
                    }
                }
                
                columnMapping["id"] = "string";

                return new GameDBSheet(title, columnMapping, entries, hasStatic);
            }

            return null;
        }

        private static void EqualizeLength(ICollection first, List<string> second)
        {
            second.AddRange(Enumerable.Repeat(string.Empty, first.Count - second.Count));
        }

        private static string NormalizeType(string type)
        {
            switch (type)
            {
                case "*": return "List<string>";
                case "": return "string";
                default: return type;
            }
        }

        private static Entry ToEntry(IList<string> row, IList<string> columns)
        {
            var entry = new Entry();
            for (var i = 0; i < row.Count; i++)
            {
                var columnName = columns[i];
                if (columnName.StartsWith("*")) continue;
                entry[columnName] = row[i];
            }

            return entry;
        }

        private static SheetsService CreateSheetsService(string credentials)
        {
            var json = File.ReadAllText(credentials);
            var serviceAccountEmail = JObject.Parse(json)["client_email"].Value<string>();
            var credential = (ServiceAccountCredential) GoogleCredential.FromJson(json).UnderlyingCredential;
            var initializer = new ServiceAccountCredential.Initializer(credential.Id)
            {
                User = serviceAccountEmail,
                Key = credential.Key,
                Scopes = new[] { SheetsService.Scope.Spreadsheets }
            };
            credential = new ServiceAccountCredential(initializer);
            return new SheetsService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
            });
        }
    }

    public class GameDBGenerator
    {
        private readonly CodeCompileUnit targetUnit = new CodeCompileUnit();
        private readonly CodeTypeDeclaration mainClass;
        private readonly CodeMemberMethod initializeMethod;
        private readonly CodeTypeDeclaration dbBackingObject;
        private const string BackingObjectTypeName = "BackingObject";
        private const string BackingObjectName = "backingObject";
        private readonly List<GameDBSheet> sheets = new List<GameDBSheet>();

        public GameDBGenerator(string ClassName)
        {
            dbBackingObject = new CodeTypeDeclaration(BackingObjectTypeName)
            {
                IsClass = true,
                TypeAttributes = TypeAttributes.Public,
                CustomAttributes = { new CodeAttributeDeclaration("Serializable") }
            };
            
            
            
            initializeMethod = new CodeMemberMethod
            {
                Name = "Initialize",
                Attributes = MemberAttributes.Public | MemberAttributes.Static,
                Parameters = { new CodeParameterDeclarationExpression(typeof(string), "json") },
                Statements =
                {
                    new CodeVariableDeclarationStatement
                    (
                        BackingObjectTypeName,
                        BackingObjectName,
                        new CodeMethodInvokeExpression
                        (
                            new CodeTypeReferenceExpression("JsonUtility"),
                            $"FromJson<{BackingObjectTypeName}>",
                            new CodeArgumentReferenceExpression("json")
                        )
                    )
                }
            };
            
            mainClass = new CodeTypeDeclaration(ClassName)
            {
                IsClass = true,
                TypeAttributes = TypeAttributes.Public | TypeAttributes.Sealed,
                Members =
                {
                    new CodeConstructor { Attributes = MemberAttributes.Private },
                    dbBackingObject,
                    initializeMethod
                }
            };

            var root = new CodeNamespace(string.Empty)
            {
                Imports =
                {
                    new CodeNamespaceImport("System"),
                    new CodeNamespaceImport("System.Linq"),
                    new CodeNamespaceImport("System.Collections.Generic"),
                    new CodeNamespaceImport("UnityEngine")
                },
                Types = { mainClass }
            };
            targetUnit.Namespaces.Add(root);
        }

        public void ParseStatic(GameDBSheet sheet)
        {
            sheets.Add(sheet);
            mainClass.Members.Add(ClassSchemaOf(sheet));

            var className = ClassNameOf(sheet);
            var dbSetName = className.Pluralize(false);
            var dbSetTypeName = $"GameDBSet<{className}>";
            var dbSetType = new CodeTypeReference(dbSetTypeName);
            
            var dbSetField = new CodeMemberField(dbSetType, dbSetName.Camelize())
            {
                Attributes = MemberAttributes.Private | MemberAttributes.Static
            };
            
            /*var dbSetProperty = new CodeMemberProperty
            {
                Name = dbSetName,
                Type = dbSetType,
                Attributes = MemberAttributes.Public | MemberAttributes.Static,
                GetStatements = { new CodeMethodReturnStatement(new CodeArgumentReferenceExpression(dbSetField.Name)) }
            };*/

            var dbStaticProperty = new CodeMemberProperty
            {
                Name = $"{dbSetName}",
                Type = new CodeTypeReference(className),
                Attributes = MemberAttributes.Public | MemberAttributes.Static,
                GetStatements = { new CodeSnippetExpression(
                    $"return {dbSetField.Name}[0]")}
            };

            mainClass.Members.Add(dbSetField);
            //mainClass.Members.Add(dbSetProperty);
            mainClass.Members.Add(dbStaticProperty);

            dbBackingObject.Members.Add(new CodeMemberField($"List<{className}.{BackingObjectTypeName}>", sheet.Name)
            {
                Attributes = MemberAttributes.Public
            });

            initializeMethod.Statements.Add(
                new CodeAssignStatement
                (
                    new CodeSnippetExpression(dbSetField.Name),
                    new CodeSnippetExpression(
                        $"new {dbSetTypeName}(null)")
                ));
            
            initializeMethod.Statements.Add(
                new CodeAssignStatement
                (
                    new CodeSnippetExpression($"var {className}BackingObject"),
                    new CodeSnippetExpression(
                        $"new {className}.{BackingObjectTypeName}()")
                ));

            foreach (var key in sheet.Columns.Keys)
            {
                if (key.Equals("id"))
                    continue;
                
                initializeMethod.Statements.Add(
                    new CodeAssignStatement
                    (
                        new CodeSnippetExpression($"{className}BackingObject.{key}"),
                        new CodeSnippetExpression(
                            $"{BackingObjectName}.{sheet.Name}.Where(o => o.id.Equals(\"{key}\")).FirstOrDefault().{key}")
                    ));
            }

            initializeMethod.Statements.Add(
                new CodeExpressionStatement(
                    new CodeSnippetExpression($"{dbSetField.Name}.Add(new {className}({className}BackingObject))"))
                );
        }
        
        public void Parse(GameDBSheet sheet)
        {
            sheets.Add(sheet);
            mainClass.Members.Add(ClassSchemaOf(sheet));

            var className = ClassNameOf(sheet);
            var dbSetName = className.Pluralize();
            var dbSetTypeName = $"GameDBSet<{className}>";
            var dbSetType = new CodeTypeReference(dbSetTypeName);
            
            var dbSetField = new CodeMemberField(dbSetType, dbSetName.Camelize())
            {
                Attributes = MemberAttributes.Private | MemberAttributes.Static
            };
            
            var dbSetProperty = new CodeMemberProperty
            {
                Name = dbSetName,
                Type = dbSetType,
                Attributes = MemberAttributes.Public | MemberAttributes.Static,
                GetStatements = { new CodeMethodReturnStatement(new CodeArgumentReferenceExpression(dbSetField.Name)) }
            };

            mainClass.Members.Add(dbSetField);
            mainClass.Members.Add(dbSetProperty);

            dbBackingObject.Members.Add(new CodeMemberField($"List<{className}.{BackingObjectTypeName}>", sheet.Name)
            {
                Attributes = MemberAttributes.Public
            });
            
            
            initializeMethod.Statements.Add(
                new CodeAssignStatement
                (
                    new CodeSnippetExpression(dbSetField.Name),
                    new CodeSnippetExpression(
                        $"new {dbSetTypeName}({BackingObjectName}.{sheet.Name}.Select(o => new {className}(o)))")
                )
            );
        }

        private static CodeTypeDeclaration ClassSchemaOf(GameDBSheet sheet)
        {
            var constructor = new CodeConstructor
            {
                Attributes = MemberAttributes.Public,
                Parameters = { new CodeParameterDeclarationExpression(BackingObjectTypeName, BackingObjectName) },
                Statements =
                {
                    new CodeAssignStatement
                    (
                        new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), BackingObjectName),
                        new CodeArgumentReferenceExpression(BackingObjectName)
                    )
                }
            };
            var sheetClass = new CodeTypeDeclaration(ClassNameOf(sheet))
            {
                IsClass = true,
                TypeAttributes = TypeAttributes.Public | TypeAttributes.Sealed,
                BaseTypes = { new CodeTypeReference("IGameDBEntry") },
                Members =
                {
                    BackingClassDeclarationOf(sheet),
                    new CodeMemberField(BackingObjectTypeName, BackingObjectName),
                    constructor
                }
            };

            var properties = sheet.Columns
                .Select(kvp => AccessorProperty(kvp.Key, kvp.Value))
                .Cast<CodeTypeMember>()
                .ToArray();
            sheetClass.Members.AddRange(properties);

            return sheetClass;
        }

        private static CodeMemberProperty AccessorProperty(string name, string type)
        {
            return new CodeMemberProperty
            {
                Name = name.Pascalize(),
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                Type = TypeReferenceOf(type),
                GetStatements =
                {
                    new CodeMethodReturnStatement(
                        new CodeFieldReferenceExpression(
                            new CodeArgumentReferenceExpression(BackingObjectName),
                            name
                        )
                    )
                }
            };
        }

        private static string ClassNameOf(GameDBSheet sheet) => sheet.Name.Singularize(false).Pascalize();

        private static CodeTypeDeclaration BackingClassDeclarationOf(GameDBSheet sheet)
        {
            var backingClass = new CodeTypeDeclaration(BackingObjectTypeName)
            {
                IsClass = true,
                TypeAttributes = TypeAttributes.Public | TypeAttributes.Sealed,
                CustomAttributes = { new CodeAttributeDeclaration("Serializable") }
            };
            var backingFields = sheet.Columns
                .Select(kvp => BackingFieldDeclaration(kvp.Key, kvp.Value))
                .Cast<CodeTypeMember>()
                .ToArray();
            backingClass.Members.AddRange(backingFields);
            return backingClass;
        }

        private static CodeMemberField BackingFieldDeclaration(string name, string type) =>
            new CodeMemberField(TypeReferenceOf(type), name) { Attributes = MemberAttributes.Public };

        private static CodeTypeReference TypeReferenceOf(string type)
        {
            switch (type)
            {
                case "int":
                    return new CodeTypeReference(typeof(int));
                case "float":
                    return new CodeTypeReference(typeof(float));
                case "string":
                    return new CodeTypeReference(typeof(string));
                case "bool":
                    return new CodeTypeReference(typeof(bool));
                case "double":
                    return new CodeTypeReference(typeof(double));
                case "int[]":
                    return new CodeTypeReference(typeof(List<int>));
                case "float[]":
                    return new CodeTypeReference(typeof(List<float>));
                case "double[]":
                    return new CodeTypeReference(typeof(List<double>));
                case "bool[]":
                    return new CodeTypeReference(typeof(List<bool>));
                case "*":
                case "string[]":
                    return new CodeTypeReference(typeof(List<string>));

                default:
                {
                    Debug.LogError("Unknown type: " + type);
                    return new CodeTypeReference(type);
                }
            }
        }

        public string Schema
        {
            get
            {
                var provider = CodeDomProvider.CreateProvider("CSharp");
                var options = new CodeGeneratorOptions
                {
                    BracingStyle = "C",
                    BlankLinesBetweenMembers = true,
                    VerbatimOrder = true
                };
                var builder = new StringBuilder();
                var writer = new StringWriter(builder, CultureInfo.InvariantCulture);
                provider.GenerateCodeFromCompileUnit(targetUnit, writer, options);
                return writer.ToString();
            }
        }

        public string Data
        {
            get
            {
                var jData = new JObject();
                foreach (var sheet in sheets)
                {
                    var jSheet = new JArray();
                    jData[sheet.Name] = jSheet;
                    foreach (var entry in sheet.Entries)
                    {
                        var jEntry = new JObject();
                        foreach (var kvp in entry)
                        {
                            var columnName = kvp.Key;
                            var columnType = sheet.Columns[columnName];
                            
                            if (columnType.Equals("type"))
                                continue;

                            var value = ParseValue(columnType, kvp.Value);
                            jEntry[columnName] = JToken.FromObject(value);
                        }

                        jSheet.Add(jEntry);
                    }
                }

                return jData.ToString(Formatting.None);
            }
        }

        private object ParseValue(string type, string value)
        {
            switch (type)
            {
                case "int":
                    return string.IsNullOrWhiteSpace(value) ? 0 : int.Parse(value, CultureInfo.InvariantCulture);
                case "float":
                    return string.IsNullOrWhiteSpace(value) ? 0f : float.Parse(value, CultureInfo.InvariantCulture);
                case "double":
                    return string.IsNullOrWhiteSpace(value) ? 0f : double.Parse(value, CultureInfo.InvariantCulture);
                case "string": return value;
                case "bool": return value.Equals("1");
                case "string[]":
                case "List<string>":
                    return value.Split(',');
                case "int[]": 
                    return value.Split(',').Select(s => int.Parse(s, CultureInfo.InvariantCulture)).ToList();
                case "float[]":
                    return value.Split(',').Select(s => float.Parse(s, CultureInfo.InvariantCulture)).ToList();
                case "double[]":
                    return value.Split(',').Select(s => double.Parse(s, CultureInfo.InvariantCulture)).ToList();
                case "bool[]":
                    return value.Split(',').Select(s => s.Equals("1")).ToList();
                default:
                {
                    Debug.LogError("Type not supported: " + type);
                    throw new ArgumentOutOfRangeException(nameof(type));
                }
                    
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///  LOCALIZATION
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public const string LocalizationFile = "localization.xls";



        class LocalItem
        {
            public string id;
            public string value;
            public int index = 0;
            public bool renamed = false;
            public string state = null;
        }

        private const string DeletedState = "DELETED";
        private static System.Drawing.Color DeletedStateColor = Color.Brown;

        private const string ChangedState = "CHANGED";
        private static System.Drawing.Color ChangedStateColor = Color.Gold;

        private const string AddedState = "ADDED";
        private static System.Drawing.Color AddedStateColor = Color.GreenYellow;

        private const string RenamedState = "RENAMED";
        private static System.Drawing.Color RenamedStateColor = Color.SeaGreen;
        
        
        private const string StateCellID = "A1";
        private const string OldValueCellID = "B1";
        private const string IDCellID = "C1";
        private const string MainLanguageValueCellID = "D1";

        private const string SheetID = "Localization";
        
        public static void PrepareLocalization(string credentials, string spreadsheedID)
        {
            int deletedCount = 0;
            int changedCount = 0;
            int addedCount = 0;
            int renamedCount = 0;
            
            
            
            if (!File.Exists(LocalizationFile))
            {
                const string template = "Assets/Fabros/Config/Editor/localization";
                File.Copy(template, LocalizationFile);
            }
            
            
            
            Dictionary<string, LocalItem> googleDocItems = new Dictionary<string, LocalItem>();
            
            
            foreach (GameDBSheet sheet in GameDBSheet.LoadFromGoogleDrive(spreadsheedID, credentials))
            {
                if (sheet == null)
                {
                    EditorUtility.ClearProgressBar();
                    return;
                }

                var s = sheet;

                List<string> columns = sheet.Columns.Select(x => x.Key).Where(x => !x.Equals("id")).ToList();

                foreach (var entry in sheet.Entries)
                {
                    string itemId = entry["id"];

                    foreach (var column in columns)
                    {
                        LocalItem item = new LocalItem();
                        item.id = $"{sheet.Name}.{itemId}.{column}";

                        if (!entry.ContainsKey(column))
                        {
                            //Cell is empty, it is ok.
                            continue;
                        }
                        
                        item.value = entry[column];

                        if (googleDocItems.ContainsKey(item.id))
                        {
                            Debug.LogError("Dublicate item with ID: " + item.id);
                        }
                        else
                        {
                            googleDocItems.Add(item.id, item);
                        }
                        
                    }
                    
                }
            }//foreach

            Dictionary<string, LocalItem> xlsItems = new Dictionary<string, LocalItem>();
            
            
            var xlFile = new System.IO.FileInfo(LocalizationFile);
            var package = new ExcelPackage(xlFile);
            var worksheet = package.Workbook.Worksheets[SheetID];
            
            
            int index = 2;
            while (true)
            {
                var id = worksheet.Cells[index, 3].Value;

                if (id == null)
                {
                    break;
                }

                if (string.IsNullOrEmpty(id.ToString()))
                {
                    break;
                }

                LocalItem item = new LocalItem();
                item.id = id.ToString();
                item.value = worksheet.Cells[index, 4].Value?.ToString();
                item.index = index;
                item.state = worksheet.Cells[index, 1].Value?.ToString();
                
                if (xlsItems.ContainsKey(item.id))
                {
                    Debug.LogError("XML Contains Dublicates: " + item.id);
                }
                else
                {
                    xlsItems.Add(item.id, item);
                }

                index++;
            }

            int lastIndex = index;
            
            List<LocalItem> renamed = new List<LocalItem>();
            
            foreach (var key in xlsItems.Keys)
            {
                var xmlItem = xlsItems[key];
                
                if (!googleDocItems.ContainsKey(key))
                {
                    bool isRenamed = false;
                    string newID = "";
                    
                    foreach (var gdKey in googleDocItems.Keys)
                    {
                        var gdItem = googleDocItems[gdKey];

                        if (gdItem.value.Equals(xmlItem.value))
                        {
                            isRenamed = true;
                            newID = gdItem.id;
                            break;
                        }
                    }

                    if (isRenamed)
                    {
                        //RENAMED
                        xmlItem.renamed = true;
                        xmlItem.id = newID;
                        renamed.Add(xmlItem);
                        
                        worksheet.Cells[xmlItem.index, 1].Value = RenamedState;
                        worksheet.Cells[xmlItem.index, 3].Value = newID;
                        worksheet.Cells[xmlItem.index, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[xmlItem.index, 1].Style.Fill.BackgroundColor.SetColor(RenamedStateColor);
                        worksheet.Cells[xmlItem.index, 2].Value = "";
                        renamedCount++;
                    }
                    else
                    {
                        //DELETED
                        worksheet.Cells[xmlItem.index, 1].Value = DeletedState;
                        worksheet.Cells[xmlItem.index, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[xmlItem.index, 1].Style.Fill.BackgroundColor.SetColor(DeletedStateColor);
                        worksheet.Cells[xmlItem.index, 2].Value = "";
                        deletedCount++;
                    }
                }
            }

            foreach (var item in renamed)
            {
                if (xlsItems.ContainsKey(item.id))
                {
                    Debug.LogError("Dublicate while adding renamed item: " + item.id);
                }
                else
                {
                    xlsItems.Add(item.id, item);
                }
            }

            foreach (var key in googleDocItems.Keys)
            {
                var gdItem = googleDocItems[key];

                if (xlsItems.ContainsKey(key))
                {
                    var xmlItem = xlsItems[key];

                    if (xmlItem.value.Equals(gdItem.value))
                    {
                        if (!xmlItem.renamed)
                        {
                            if (xmlItem.state != null && xmlItem.state.Equals(ChangedState))
                            {
                                changedCount++;
                            }
                            else//DID NOT CHANGE
                            {
                                if (xmlItem.state == null)
                                {
                                    worksheet.Cells[xmlItem.index, 1].Value = "";
                                    worksheet.Cells[xmlItem.index, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[xmlItem.index, 1].Style.Fill.BackgroundColor.SetColor(Color.White);
                                    worksheet.Cells[xmlItem.index, 2].Value = "";    
                                }
                            }
                        }
                    }
                    else
                    {
                        //CHANGED
                        worksheet.Cells[xmlItem.index, 4].Value = gdItem.value;
                        worksheet.Cells[xmlItem.index, 2].Value = xmlItem.value;
                        worksheet.Cells[xmlItem.index, 1].Value = ChangedState;
                        worksheet.Cells[xmlItem.index, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[xmlItem.index, 1].Style.Fill.BackgroundColor.SetColor(ChangedStateColor);
                        changedCount++;
                    }
                }
                else
                {
                    //ADDED
                    worksheet.Cells[lastIndex, 4].Value = gdItem.value;
                    worksheet.Cells[lastIndex, 3].Value = gdItem.id;
                    worksheet.Cells[lastIndex, 1].Value = AddedState;
                    worksheet.Cells[lastIndex, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[lastIndex, 1].Style.Fill.BackgroundColor.SetColor(AddedStateColor);
                    lastIndex++;
                    addedCount++;
                }
            }
            
            package.SaveAs(xlFile);
            
            EditorUtility.DisplayDialog("Localization Update Done", $"Added: {addedCount}\n" +
                                                                    $"Changed: {changedCount}\n" +
                                                                    $"Renamed: {renamedCount}\n" +
                                                                    $"Removed: {deletedCount}\n" +
                                                                    $"Total items: {googleDocItems.Count}", "OK");
            
            System.Diagnostics.Process.Start("explorer.exe", "/select," + xlFile);
        }


        class LocalizationTranslate
        {
            public string value;
            public string state;
            public string field;
        }

        public static void LocalizationConvert()
        {
            int emptyStringsCount = 0;
            
            var xlFile = new System.IO.FileInfo(LocalizationFile);
            var package = new ExcelPackage(xlFile);
            var worksheet = package.Workbook.Worksheets[SheetID];

            Dictionary<int, string> languages = new Dictionary<int, string>();
            int langColumnIndex = 5;//Skip main language

            //EN, General, Id, value
            var translates = new Dictionary<string,
                Dictionary<string, 
                    Dictionary<string, List<LocalizationTranslate> > > >();
            
            
            while (true)
            {
                var cell = worksheet.Cells[1, langColumnIndex].Value;

                if (cell == null)
                {
                    break;
                }

                if (translates.ContainsKey(cell.ToString()))
                {
                    Debug.LogError("Language Dublicate: " + cell.ToString());
                    continue;
                }
                
                translates.Add(cell.ToString(), new Dictionary<string, Dictionary<string, List<LocalizationTranslate>>>());
                
                languages.Add(langColumnIndex, cell.ToString());
                langColumnIndex++;
            }



            int rowIndex = 2;
            while (true)
            {
                var idValue = worksheet.Cells[rowIndex, 3].Value;

                if (idValue == null)
                {
                    break;
                }

                string id = idValue.ToString();
                
                foreach (var langCellIndex in languages.Keys)
                {
                    string lang = languages[langCellIndex];

                    var value = worksheet.Cells[rowIndex, langCellIndex].Value;

                    if (value == null)
                    {
                        emptyStringsCount++;
                        continue;
                    }
                    
                    var state = worksheet.Cells[rowIndex, 1].Value;

                    if (state != null && state.ToString().Equals(DeletedState))
                    {
                        continue;
                    }

                    string strValue = value.ToString();
                    
                    LocalizationTranslate translate = new LocalizationTranslate();

                    int firstDotIndex = id.IndexOf(".", StringComparison.Ordinal);
                    int lastDotIndex = id.LastIndexOf(".", StringComparison.Ordinal);
                    
                    string sheet = id.Substring(0, firstDotIndex);
                    translate.field = id.Substring(lastDotIndex + 1, id.Length - lastDotIndex - 1);
                    string itemId = id.Substring(firstDotIndex + 1, lastDotIndex - firstDotIndex - 1);
                    translate.value = strValue.Replace("\n", "\\n").Replace("\"", "\\\"");
                    
                    
                    if (worksheet.Cells[rowIndex, 1].Value != null)
                    {
                        translate.state = worksheet.Cells[rowIndex, 1].Value.ToString();
                    }

                    if (!translates[lang].ContainsKey(sheet))
                    {
                        translates[lang].Add(sheet, new Dictionary<string, List<LocalizationTranslate>>());
                    }

                    if (!translates[lang][sheet].ContainsKey(itemId))
                    {
                        translates[lang][sheet].Add(itemId, new List<LocalizationTranslate>());
                    }
                    
                    translates[lang][sheet][itemId].Add(translate);
                }


                rowIndex++;

            }
            
            //игнорить DELETED
            //Выводить диалог

            
            foreach (var lang in translates.Keys)
            {
                string fileContent = "{\n";

                int processed = 0;
                foreach (var sheetName in translates[lang].Keys)
                {
                    EditorUtility.DisplayProgressBar(
                        $"Localization Convert",
                        $"Processing {lang}",
                        processed++ / (float) translates[lang].Count);
                    
                    fileContent += $"    \"{sheetName}\": [\n";

                    foreach (var id in translates[lang][sheetName].Keys)
                    {
                        fileContent += "        {\n";
                        
                        fileContent += $"            \"id\": \"{id}\",\n";
                        
                        foreach (var translate in translates[lang][sheetName][id])
                        {
                            fileContent += $"            \"{translate.field}\": \"{translate.value}\",\n";
                        }

                        fileContent = fileContent.Remove(fileContent.Length - 2, 1);
                        fileContent += "        },\n";
                    }
                    
                    fileContent = fileContent.Remove(fileContent.Length - 2, 1);
                    fileContent += "    ],\n";
                }
                
                fileContent = fileContent.Remove(fileContent.Length - 2, 1);
                fileContent += "}\n";
                
                StreamWriter writer = new StreamWriter("Assets/Resources/Locale" + lang + ".json", false);
                writer.WriteLine(fileContent);
                writer.Close();
            }
            
            AssetDatabase.Refresh();
            EditorUtility.ClearProgressBar();
        }
    }
}