using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

public interface IGameDBEntry
{
    string Id { get; }
}

public class GameDBSet<T>: Collection<T> where T : IGameDBEntry
{
    private readonly Dictionary<string, T> set;

    public GameDBSet(IEnumerable<T> items)
    {
        if (items == null)
        {
            return;
        }
        
        foreach (var item in items)
        {
            Add(item);
        }
        
        set = Items.ToDictionary(i => i.Id, i => i);
    }

    public T Find(string id, bool showError = true)
    {
        T item;
        if (!set.TryGetValue(id, out item))
        {

            if (showError)
            {
                Debug.LogError($"{typeof(T).Name} with ID {id} is not found in GameDB");    
            }
            
            return default(T);
        }

        return item;
    }

    public T FindOrDefault(string id)
    {
        T item;
        set.TryGetValue(id, out item);
        return item;
    }
}