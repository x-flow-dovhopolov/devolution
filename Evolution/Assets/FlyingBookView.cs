﻿using System;
using System.Collections.Generic;
using Core;
using DG.Tweening;
using echo17.EndlessBook.Demo01;
using UI.LoadingScreen;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class FlyingBookView : MonoBehaviour, IPointerClickHandler
{
    private Sequence seq;
    private Sequence rotateSequence;
    private Sequence XYZequence;
    [SerializeField] private GameObject BookScript;
    [SerializeField] private  Button btn12;
    [SerializeField] private  Button play;
    [SerializeField] private GameObject sceneLoader;
    [SerializeField] private PagesList pageList;
    [SerializeField] private RandomPointVector3 list;
    
    private bool isOpened;
    private int TextCounter = 0;
    public float time;
    public float time2 = 2.5f;
    public float angle;

    [SerializeField] private Vector3 seq1Rotate;
    [SerializeField] private Vector3 seq1XYZ;
    [Space]
    [SerializeField] private Vector3 seq2Rotate;
    [SerializeField] private Vector3 seq2XYZ;
    [Space]
    [SerializeField] private Vector3 seq3Rotate;
    [SerializeField] private Vector3 seq3XYZ;
    [Space]
    [SerializeField] private Vector3 seq4Rotate;
    [SerializeField] private Vector3 seq4XYZ;
    [Space]
    [SerializeField] private Vector3 seq5Rotate;
    [SerializeField] private Vector3 seq5XYZ;

    [Space]
    
    [SerializeField] private List<GameObject> points;
    [SerializeField] private Vector3 point1;
    [SerializeField] private Vector3 point2;
    [SerializeField] private Vector3 point3;
    [SerializeField] private Vector3 point4;
    private void Start()
    {
        btn12.OnClickAsObservable().Subscribe(_ =>
        {
            try
            {
                var singleComixImage = pageList.ShowNextSingleComixByStack();
                ShowSingleComix(singleComixImage);
            }
            catch (Exception)
            {
                btn12.gameObject.SetActive(false);
                play.gameObject.SetActive(true);
            }
        });
        
        play.OnClickAsObservable().Subscribe(_ =>
        {
            //sceneLoader.GetComponent<LoadingScreen>().LoadSceneAsync("Adventure").Forget();
            GameManager.Instance.OnDataLoaded();
        });

        //text();
        NewFlySequence();
        //MoveXYZ();
        //MoveRotate();
    }
    
    private void text()
    {
        var Sequence = DOTween.Sequence();//.SetLoops(-1, LoopType.Yoyo);
        Sequence
            .Append(transform.DORotate(list.GetRandomRotateVector3(), time))
            .Join(transform.DOMove(list.GetRandomPoint(), time))
            .OnComplete(() =>
            {
                var Sequence2 = DOTween.Sequence()
                    .Append(transform.DORotate(list.GetRandomRotateVector3(), time))
                    .Join(transform.DOMove(list.GetRandomPoint(), time)).OnComplete(() =>
                    {
                        var Sequence3 = DOTween.Sequence()
                            .Append(transform.DORotate(list.GetRandomRotateVector3(), time))
                            .Join(transform.DOMove(list.GetRandomPoint(), time)).OnComplete(() =>
                            {
                                var Sequence4 = DOTween.Sequence()
                                    .Append(transform.DORotate(list.GetRandomRotateVector3(), time))
                                    .Join(transform.DOMove(list.GetRandomPoint(), time)).OnComplete(() =>
                                    {
                                        var Sequence5 = DOTween.Sequence()
                                            .Append(transform.DORotate(list.GetRandomRotateVector3(), time))
                                            .Join(transform.DOMove(list.GetRandomPoint(), time));
                                    });;
                            });;
                    });;
            });
        
        /*var Sequence = DOTween.Sequence();//.SetLoops(0, LoopType.Yoyo);
        Sequence.Append(transform.DORotate(list.GetRandomRotateVector3(), time))
            .Join(transform.DOMove(list.GetRandomPoint(), time))
            
            .Insert(time - 1f, transform.DORotate(list.GetRandomRotateVector3(), time))
            .Insert(time - 1f, transform.DOMove(list.GetRandomPoint(), time))

            .Insert(time + 1f, transform.DORotate(list.GetRandomRotateVector3(), time))
            .Insert(time + 1f, transform.DOMove(list.GetRandomPoint(), time))

            .Insert(time + 3f, transform.DORotate(list.GetRandomRotateVector3(), time))
            .Insert(time + 1f, transform.DOMove(list.GetRandomPoint(), time))

            .Insert(time + 5f, transform.DORotate(list.GetRandomRotateVector3(), time))
            .Insert(time + 5f, transform.DOMove(list.GetRandomPoint(), time))
            ;*/
    }

    private void NewFlySequence()
    {
        var vv = list.GetRandomPoint();
        
        seq = DOTween.Sequence()
            .Append(transform.DORotate(list.GetRandomRotateVector3(), time))
            .Insert(0,transform.DOMove(vv, time))
            
            .Insert(time - 2f, transform.DORotate(list.GetRandomRotateVector3Correction(), time2))
            .Insert(time - 1f, transform.DORotate(list.GetRandomRotateVector3Correction(), time2))
            .Insert(time, transform.DORotate(list.GetRandomFinishCorrection(), time2))
            
            //.Insert(time - 0.25f, transform.DORotate(list.GetRandomFinishCorrection(), 0.5f))
            //.Insert(time - 0.25f, transform.DOMove(list.GetMiddlePoint(), time2))
            
            .OnComplete(NewFlySequence);
    }
    private void ShowSingleComix(GameObject comix)
    {
        if (comix != null)
        {
            comix.SetActive(true);
        }
        else
        {
            BookScript.GetComponent<Demo01>().OnTurnButtonClicked(1);
        }
    }
    private void NewSequence()
    {
        Sequence s = DOTween.Sequence();

        s
            .Append(transform.DORotate(new Vector3(-10, 0, 0), 2f))
            .Append(transform.DORotate(new Vector3(-10, 0, 0), 2f))
            .Insert(3.5f, transform.DORotate(new Vector3(10, 0, -20), 2f));
    }
    private void MoveRotate()
    {
        rotateSequence = DOTween.Sequence();
        //var K = Random.Range(0, 3);
        var K = 2;
        
        if (K == 0)
        {
            rotateSequence
                .Append(transform.DORotate(new Vector3(Random.Range(-angle, angle), 0f, 0f), time));
        }
        else if (K == 1)
        {
            rotateSequence
                .Append(transform.DORotate(new Vector3(0f,  0f, Random.Range(-angle, angle)), time));
        }
        else if (K == 2)
        {
            rotateSequence
                .Append(transform.DORotate(CalculateAngle(), time / 2));
        }

        rotateSequence.Insert(time - 0.1f, transform.DORotate(new Vector3( 1f, 0f, 1f), 
            time / 2));
        
        rotateSequence
            .OnComplete(MoveRotate);
    }

    private Vector3 CalculateAngle()
    {
        //float angle = 30f;
        float x = Random.Range(-angle * 2, angle * 2);
        float y = Random.Range(-5, 5);
        float z = Random.Range(-angle, angle );
        
        return new Vector3(x, y, z);
    }
    
    private void MoveXYZ()
    {
        float distance = Random.Range(0.02f, 0.04f);
        float moveX = Random.Range(-distance, distance);
        float moveZ = Random.Range(-distance, distance);

        float time = 2f;
        
        XYZequence = DOTween.Sequence();
        XYZequence.Append(transform.DOMoveX( moveX, time))
            .Append(transform.DOMoveZ( moveZ, time))
            .OnComplete(MoveXYZ);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isOpened)
        {
            return;
        }
        
        seq.Kill();
        rotateSequence.Kill();
        XYZequence.Kill();
        ConnectSequence();
        Debug.Log("Killed");
    }

    private void ConnectSequence()
    {
        Sequence move = DOTween.Sequence();
        move
            .Join(transform.DOMoveX(0.1f, 1f))
            .Join(transform.DOMoveY(1.5f, 1f))
            .Join(transform.DOMoveZ(-0.1f, 1f))
            .Join(transform.DORotate(new Vector3(0,0,0), 1f))
                .OnComplete(() =>
                {
                    /*Destroy(GetComponent<BoxCollider>());
                    gameObject.AddComponent<BoxCollider>();*/

                    transform.DORotate(Vector3.zero, 0.1f);
                    move.Append(transform.DOMoveX(0.1f, 0.5f));
                    BookScript.GetComponent<Demo01>().OnStateButtonClicked(2);
                    btn12.gameObject.SetActive(true);
                    isOpened = true;
                });
    }
}
