﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class GeneralWindowView : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI HeaderText;
    [SerializeField] protected Button CloseBtn;
    [SerializeField] private GameObject DataPanel;

    private void Start()
    {
        Debug.Log($"OnStart GeneralWindowView {transform.parent.name}");
        CloseBtn.OnClickAsObservable().Subscribe(_ =>
        {
            gameObject.SetActive(false);
        });
    }
}
