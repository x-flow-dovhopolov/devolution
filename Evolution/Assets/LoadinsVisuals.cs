﻿using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;
using UnityEngine.UI;

public class LoadinsVisuals : MonoBehaviour
{
    public Image[] progressBar;
    public Sprite loadingIcon;
    public Sprite loadingDoneIcon;
    public float fadeDuration;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartVisuals());
    }

    public IEnumerator StartVisuals()
    {
        int i = 0;

      
            //show loading process visuals till response from authentication received
            while (true)
            {
                progressBar[i].sprite = loadingDoneIcon;

                if (i == 0)
                {
                    progressBar[progressBar.Length - 1].sprite = loadingIcon;
                }
                else
                {
                    progressBar[i - 1].sprite = loadingIcon;
                }

                i++;

                if (i == progressBar.Length)
                {
                    i = 0;
                }

                yield return new WaitForSeconds(fadeDuration);
            }
        
    }
}
