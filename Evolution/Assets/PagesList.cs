﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;

public class PagesList : MonoBehaviour
{
    [SerializeField] private List<GameObject> AllImages;
    private int i = 0;
    private int j = 0;
    
    [SerializeField] private Queue<GameObject> numbers = new Queue<GameObject>();
    
    public Subject<Unit> onSequenceEnd = new Subject<Unit>();

    private void Start()
    {
        foreach (var image in AllImages)
        {
            numbers.Enqueue(image);
        }
    }

    public GameObject ShowNextSingleComixByStack()
    {
        try
        {
            return numbers.Dequeue();
        }
        catch (Exception)
        {
            throw;
        }
    }
}
