﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPointVector3 : MonoBehaviour
{
    [SerializeField] private List<Vector3> leftPoints;
    [SerializeField] private List<Vector3> rightPoints;
    
    [SerializeField] private List<Vector3> vectors;

    private bool leftSide;
    public Vector3 GetRandomPoint()
    {
        var vector = Vector3.zero;
        var i = Random.Range(0, 2);
       // Debug.Log($"i {i}");
        if (leftSide)
        {
            vector = leftPoints[i];
            leftSide = false;
        }
        else
        {
            vector = rightPoints[i];
            leftSide = true;
        }
        //Debug.Log($"Vector = {vector}");
        return vector;
    }

    public Vector3 GetMiddlePoint()
    {
        return new Vector3(0,0,0);
    }
    public Vector3 GetRandomRotateVector3()
    {
        var x = Random.Range(-15f, 15f);
        x = GetOtherDirectionX(x);
        var y = Random.Range(-15f, 15f);
        y = GetOtherDirectionY(y);
        var z = Random.Range(-10f, 10f);
        z = GetOtherDirectionZ(z);
        return new Vector3(x,y,z);
    }
    
    public Vector3 GetRandomRotateVector3Correction()
    {
        var x = Random.Range(-45f, 0f);
        var y = Random.Range(-15f, 5f);
        var z = Random.Range(-15f, 5f);
        return new Vector3(x,y,z);
    }
    
    
    public Vector3 GetRandomFinishCorrection()
    {
        var x = Random.Range(-15f, 15f);
        var y = Random.Range(-15f, 15f);
        var z = Random.Range(-15f, 15f);
        return new Vector3(x,y,z);
    }
    
    private float GetOtherDirectionX(float direction)
    {
        
//        Debug.Log($"{transform.rotation.x} = {direction} ");
        if ((transform.rotation.x < 0 && direction < 0) || 
            transform.rotation.x > 0 && direction > 0)
        {
            var newDirection = direction * -1;
            Debug.Log($"вв {transform.rotation.x} = {direction} = {newDirection}");
            return direction * -1;
        }

        return direction;
    }

    private float GetOtherDirectionY(float direction)
    {
        //Debug.Log($"{transform.rotation.x} = {direction} ");
        if ((transform.rotation.y < 0 && direction < 0) || 
            transform.rotation.y > 0 && direction > 0)
        {
            var newDirection = direction * -1;
            Debug.Log($"вв {transform.rotation.x} = {direction} = {newDirection}");
            return direction * -1;
        }

        return direction;
    }

    private float GetOtherDirectionZ(float direction)
    {
        //Debug.Log($"{transform.rotation.x} = {direction} ");
        if ((transform.rotation.z < 0 && direction < 0) || 
            transform.rotation.z > 0 && direction > 0)
        {
            var newDirection = direction * -1;
            Debug.Log($"вв {transform.rotation.x} = {direction} = {newDirection}");
            return direction * -1;
        }

        return direction;
    }
}
