﻿using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

// need UniRx.Triggers namespace for extend gameObejct

public class RxTest : MonoBehaviour
{    
    void Start()
    {
        var source = Observable.Interval(TimeSpan.FromMilliseconds(1000));
        var hot = source.Publish();

        var subscription1 = source.Subscribe(x => Debug.Log($"'Observer 1: onNext: {x}); "),
            e => Debug.Log($"Observer 1: onError: {e}"),
            () => Debug.Log($"Observer 1: onCompleted"));
        
        
        Debug.Log("Current Time after 1st subscription: " + Time.time);
        
        hot.Connect();
        /*Observable. 
            Defer<>(() => { Debug.Log("1"); });
        var clickStream = Observable.EveryUpdate()
            .Where(_ => Input.GetMouseButtonDown(0));

        clickStream.Buffer(clickStream.Throttle(TimeSpan.FromMilliseconds(250)))
            .Where(xs => xs.Count >= 2)
            .Subscribe(xs => Debug.Log("DoubleClick Detected! Count:" + xs.Count));*/
    }
}
