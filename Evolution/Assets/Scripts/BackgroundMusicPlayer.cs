﻿using UnityEngine;

public class BackgroundMusicPlayer : MonoBehaviour
{
    public static BackgroundMusicPlayer Instance;
    private bool musicIsMute = false;

    public bool MusicIsMute
    {
        get => musicIsMute;
        set
        {
            musicIsMute = value;
        }
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this) 
            Destroy(gameObject);

        DontDestroyOnLoad(this);
    }
}
