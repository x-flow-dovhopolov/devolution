﻿using System.Collections.Generic;

namespace Core.Conversation
{
    public static class ConversationDataFromJson
    {
        public static ConversationModel WelcomeAndBuildMill = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("WelcomeAndBuildMill.text1").Text
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("WelcomeAndBuildMill.text2").Text
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("WelcomeAndBuildMill.text3").Text
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("WelcomeAndBuildMill.text4").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("WelcomeAndBuildMill.text5").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("WelcomeAndBuildMill.text6").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("WelcomeAndBuildMill.text7").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("WelcomeAndBuildMill.text8").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("WelcomeAndBuildMill.text9").Text
                }
            }
        };
        
        public static ConversationModel GoToMill = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("GoToMill.text1").Text
                }
            }
        };

        public static ConversationModel MillBuildWorkers = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("MillBuildWorkers.text1").Text
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("MillBuildWorkers.text2").Text
                }
            }
        };
        
        public static ConversationModel MillMergeWorkers = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("MillMergeWorkers.text1").Text
                }
            }
        };

        public static ConversationModel MillMergeFinish = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("MillMergeFinish.text1").Text
                }
            }
        };

        public static ConversationModel MillUpgradeBuildingPart2 = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("MillUpgradeBuildingPart2.text1").Text
                }
            }
        };

        public static ConversationModel MillBuyManager = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("MillBuyManager.text1").Text
                }
            }
        };
        
        public static ConversationModel MillUseSkill = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("MillUseSkill.text1").Text
                }
            }
        };
        
        public static ConversationModel MillExit = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("MillExit.text1").Text
                }
            }
        };
        
        public static ConversationModel CastleUpgrade = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CastleUpgrade.text1").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CastleUpgrade.text2").Text,
                    Side = ConversationSide.Right,
                    PrimaryCharacter = ConversationCharacter.Loras
                }
            }
        };
        
        public static ConversationModel BuildFarm = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("BuildFarm.text1").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("BuildFarm.text2").Text
                }
            }
        };
        
        public static ConversationModel GoToFarm = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("GoToFarm.text1").Text
                }
            }
        };
        
        public static ConversationModel GoToStock = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("GoToStock.text1").Text
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("GoToStock.text2").Text
                }
            }
        };
        
        public static ConversationModel StockOverview = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("StockOverview.text1").Text
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("StockOverview.text2").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("StockOverview.text3").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                }
            }
        };
        
        public static ConversationModel DiscoverLand = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("DiscoverLand.text1").Text
                }
            }
        };
        
        public static ConversationModel CamillaAndBuildBarracks = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CamillaAndBuildBarracks.text1").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Camilla,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CamillaAndBuildBarracks.text2").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Camilla,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CamillaAndBuildBarracks.text3").Text,
                    SecondaryCharacter = ConversationCharacter.Camilla
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CamillaAndBuildBarracks.text4").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Camilla,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CamillaAndBuildBarracks.text5").Text,
                    SecondaryCharacter = ConversationCharacter.Camilla
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CamillaAndBuildBarracks.text6").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Camilla,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CamillaAndBuildBarracks.text7").Text,
                    SecondaryCharacter = ConversationCharacter.Camilla
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CamillaAndBuildBarracks.text8").Text
                }
            }
        };
        
        public static ConversationModel ShopBarracks = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("ShopBarracks.text2").Text
                }
            }
        };
        
        public static ConversationModel BuildTavern = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("BuildTavern.text1").Text
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("BuildTavern.text2").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("BuildTavern.text3").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("BuildTavern.text4").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("BuildTavern.text5").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("BuildTavern.text6").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                }
            }
        };
        
        public static ConversationModel GoToTavern = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("GoToTavern.text1").Text
                }
            }
        };
        
        public static ConversationModel TavernPlayGame = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("TavernPlayGame.text1").Text
                }
            }
        };
        
        public static ConversationModel CaptureTomb = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CaptureTomb.text1").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CaptureTomb.text2").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("CaptureTomb.text3").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                }
            }
        };
        
        public static ConversationModel AboutMapPieces = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text1").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text2").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text3").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text4").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text5").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text6").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text7").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text8").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text9").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text10").Text,
                    Side = ConversationSide.Left,
                    PrimaryCharacter = ConversationCharacter.Jafar,
                    SecondaryCharacter = ConversationCharacter.Loras
                },
                new ConversationLine()
                {
                    Message = LocaleRU.Tutorials.Find("AboutMapPieces.text11").Text,
                    SecondaryCharacter = ConversationCharacter.Jafar
                }
            }
        };
        
        public static ConversationModel Empty = new ConversationModel
        {
            Lines = new List<ConversationLine>()
            {
            }
        };
    }
}