using Core.Tutorial.TutorialStateMachine.HintsView;
using UI;
using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Conversation
{
    public class ConversationManager
    {
        private GameObject hint;
        
        public async void ExecuteConversationWithEvent(ConversationModel conversation)
        {
            HideHintDuringDialog();
            
            var blocker = GameManager.Instance.CreateConversationBlocker();
            
            foreach (var conversationLine in conversation.Lines)
            {
                var conversationView = GameManager.Instance.CreateConversation();
                conversationView.SetData(conversationLine);
                
                await conversationView.ClickStream.First();
                
                GameObject.Destroy(conversationView.gameObject);
            }
            
            GameObject.Destroy(blocker.gameObject);

            ShowHint();
            
            GameManager.Instance.Tutor.Context._state.DialogFinished.OnNext(Unit.Default);
        }

        private GameObject FindHint()
        {
            //TODO explore another way to find Hint. Dont destroy on load in start scene ? 
            switch (SceneManager.GetActiveScene().name)
            {
                case "Adventure" : 
                    hint = AdventureMainUI.Instance.Hint;
                    break;
                case "CurrencyScene" : 
                    hint = InsideBuildingUiHelper.Instance.Hint;
                    break;
                case "Tavern" :
                    hint = TavernUI.Instance.Hint;
                    break;
            }

            return hint;
        }
        
        private void HideHintDuringDialog()
        {
            //if (hint != null) 
                FindHint().SetActive(false);
        }

        private void ShowHint()
        {
            //if (hint != null)  
                hint.SetActive(true);
        }
    }
}