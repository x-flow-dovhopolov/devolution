using System.Collections.Generic;

namespace Core.Conversation
{
    public class ConversationModel
    {
        public List<ConversationLine> Lines;
    }

    public class HintModel
    {
        public string Message;
    }
    
    public class ConversationLine
    {
        public ConversationSide Side = ConversationSide.Right;
        public ConversationCharacter PrimaryCharacter = ConversationCharacter.Loras;
        public ConversationCharacter SecondaryCharacter = ConversationCharacter.None;
        public string Message;
    }
    
    public enum ConversationSide
    {
        Left,
        Right
    }

    public enum ConversationCharacter
    {
        None,
        Loras,
        Jafar,
        Camilla
    }
}