﻿using System.Collections.Generic;

namespace Core.Conversation
{
    public static class HintData
    {
        public static HintModel BuildMill => new HintModel
        {
            Message = LocaleRU.Hints.Find("BuildMill").Dialog
        };
        public static HintModel GoToMill => new HintModel
        {
            Message = LocaleRU.Hints.Find("GoToMill").Dialog
        };
        public static HintModel CreateNpc => new HintModel
        {
            Message = LocaleRU.Hints.Find("CreateNpc").Dialog
        };
        public static HintModel Wait => new HintModel
        {
            Message = LocaleRU.Hints.Find("Wait").Dialog
        };
        public static HintModel CreateOneMoreNpc => new HintModel
        {
            Message = LocaleRU.Hints.Find("CreateOneMoreNpc").Dialog
        };
        public static HintModel NpcReward => new HintModel
        {
            Message = LocaleRU.Hints.Find("NpcReward").Dialog
        };
        public static HintModel Create2LevelNpc => new HintModel
        {
            Message = LocaleRU.Hints.Find("Create2LevelNpc").Dialog
        };
        public static HintModel CollectUpgradeBuildingMoney => new HintModel
        {
            Message = LocaleRU.Hints.Find("CollectUpgradeBuildingMoney").Dialog
        };
        public static HintModel UpgradeBuilding => new HintModel
        {
            Message = LocaleRU.Hints.Find("UpgradeBuilding").Dialog
        };
        public static HintModel CollectManagerMoney => new HintModel
        {
            Message = LocaleRU.Hints.Find("CollectManagerMoney").Dialog
        };
        public static HintModel HireManager => new HintModel
        {
            Message = LocaleRU.Hints.Find("HireManager").Dialog
        };
        public static HintModel UseManager => new HintModel
        {
            Message = LocaleRU.Hints.Find("UseManager").Dialog
        };
        public static HintModel CollectCastleMoney => new HintModel
        {
            Message = LocaleRU.Hints.Find("CollectCastleMoney").Dialog
        };
        public static HintModel CollectFarmMoney => new HintModel
        {
            Message = LocaleRU.Hints.Find("CollectFarmMoney").Dialog
        };
        public static HintModel GoToCity => new HintModel
        {
            Message = LocaleRU.Hints.Find("GoToCity").Dialog
        };
        
        /*------------------------------------------------------------------------------------------------------------*/
        
        
        public static HintModel UpgradeCastle => new HintModel
        {
            Message = LocaleRU.Hints.Find("UpgradeCastle").Dialog
        };
        public static HintModel BuildFarm => new HintModel
        {
            Message = LocaleRU.Hints.Find("BuildFarm").Dialog
        };
        public static HintModel GoToFarm => new HintModel
        {
            Message = LocaleRU.Hints.Find("GoToFarm").Dialog
        };
        public static HintModel PlantTree => new HintModel
        {
            Message = LocaleRU.Hints.Find("PlantTree").Dialog
        };
        public static HintModel CollectHarvest => new HintModel
        {
            Message = LocaleRU.Hints.Find("CollectHarvest").Dialog
        };
        public static HintModel PlantOneMoreTree => new HintModel
        {
            Message = LocaleRU.Hints.Find("PlantOneMoreTree").Dialog
        };
        public static HintModel Create2LevelTree => new HintModel
        {
            Message = LocaleRU.Hints.Find("Create2LevelTree").Dialog
        };
        public static HintModel CollectFarmManagerMoney => new HintModel
        {
            Message = LocaleRU.Hints.Find("CollectFarmManagerMoney").Dialog
        };
        public static HintModel HireFarmManager => new HintModel
        {
            Message = LocaleRU.Hints.Find("HireFarmManager").Dialog
        };
        
        /*------------------------------------------------------------------------------------------------------------*/
        
        public static HintModel GoToStock => new HintModel
        {
            Message = LocaleRU.Hints.Find("GoToStock").Dialog
        };
        public static HintModel CloseStock => new HintModel
        {
            Message = LocaleRU.Hints.Find("CloseStock").Dialog
        };
        public static HintModel CollectMapDiscoverMoney => new HintModel
        {
            Message = LocaleRU.Hints.Find("CollectMapDiscoverMoney").Dialog
        };
        public static HintModel MapDiscover => new HintModel
        {
            Message = LocaleRU.Hints.Find("MapDiscover").Dialog
        };
        public static HintModel RepairBridge => new HintModel
        {
            Message = LocaleRU.Hints.Find("RepairBridge").Dialog
        };
        public static HintModel HelpTraveler => new HintModel
        {
            Message = LocaleRU.Hints.Find("HelpTraveler").Dialog
        };
        public static HintModel CollectBarrackMoney => new HintModel
        {
            Message = LocaleRU.Hints.Find("CollectBarrackMoney").Dialog
        };
        public static HintModel BuildBarrack => new HintModel
        {
            Message = LocaleRU.Hints.Find("BuildBarrack").Dialog
        };
        
        /*------------------------------------------------------------------------------------------------------------*/
        
        public static HintModel EnterBarrack => new HintModel
        {
            Message = LocaleRU.Hints.Find("EnterBarrack").Dialog
        };
        public static HintModel CreateWarrior => new HintModel
        {
            Message = LocaleRU.Hints.Find("CreateWarrior").Dialog
        };
        public static HintModel UseShop => new HintModel
        {
            Message = LocaleRU.Hints.Find("UseShop").Dialog
        };
        public static HintModel Create3LevelNpc => new HintModel
        {
            Message = LocaleRU.Hints.Find("Create3LevelNpc").Dialog
        };
        public static HintModel TavernCollectMoney => new HintModel
        {
            Message = LocaleRU.Hints.Find("TavernCollectMoney").Dialog
        };
        public static HintModel BuildTavern => new HintModel
        {
            Message = LocaleRU.Hints.Find("BuildTavern").Dialog
        };
        public static HintModel GoToTavern => new HintModel
        {
            Message = LocaleRU.Hints.Find("GoToTavern").Dialog
        };
        public static HintModel PlayGame => new HintModel
        {
            Message = LocaleRU.Hints.Find("PlayGame").Dialog
        };
        public static HintModel CollectTombDiscoverMoney => new HintModel
        {
            Message = LocaleRU.Hints.Find("CollectTombDiscoverMoney").Dialog
        };
        public static HintModel TombDiscover => new HintModel
        {
            Message = LocaleRU.Hints.Find("TombDiscover").Dialog
        };
    }
}