﻿using System;
using System.Collections;
using System.Linq;
using Core.Conversation;
using Core.ModelData.ExceptionHierarchy;
using Core.Tutorial;
using Core.Tutorial.TutorialStateMachine.HintsView;
using GameObjects;
using UI;
using UI.Conversation;
using UI.LoadingScreen;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] public GameObject tutorialMaskPrefab = default;
        [SerializeField] public GameObject conversationUIPrefab = default;
        [SerializeField] public GameObject conversationUIPrefabBrocker = default;
        [SerializeField] public GameObject hintUIPrefab = default;
        [SerializeField] public GameObject SceneLoader = default;
        //[SerializeField] public bool tutorFromStart = default;
        
        public static GameManager Instance;
        public Model Model;
        public GameSetting GameSetting;
        public GameObject Reward;
        public GameObject BuildMenuPanel;
        public TutorialManager Tutor;
        public ConversationManager Conversation;

        private GameObject tutorialMask;

        public ImageDatabase ImageDb;
        public bool TutorialIsReady;
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) 
                Destroy(gameObject);

            DontDestroyOnLoad(this);
            
            LocaleRU.Initialize(Resources.Load("LocaleRU").ToString());
            Conversation = new ConversationManager();
            
            var targetFile = Resources.Load<TextAsset>("GameSetting");
            if (targetFile != null)
            {
                GameSetting.Initialize(targetFile.text);
            }

            Tutor = new TutorialManager();
            Model = new Model();
            Model.Load();
            Application.targetFrameRate = 60;
//            Debug.Log("Parallax");
        }

        private void Start() => Screen.orientation = ScreenOrientation.Portrait;

        private bool firstOpenShowBook = true;
        public void OnDataLoaded()
        {
            if (Tutor.CurrentStep == ETutorialStep.BuyMillConversation && firstOpenShowBook)
            {
                SceneManager.LoadSceneAsync("BookComix 1");
                firstOpenShowBook = false;
                return;
            }
            
            LoadSceneAsyncAdventureScene().Forget();
            StartCoroutine(SpawnCurrencyMainThread());
            
            //Tutor.StartTutorial();
            
            /*var asyncOperation =  SceneManager.LoadSceneAsync("Adventure", 
                new LoadSceneParameters(LoadSceneMode.Single));*/
        }
        
        private async UniTask LoadSceneAsyncAdventureScene()
        {
            string sceneName = "";
            Tutor.StartTutorial();
            Tutor.OnStepFromSaveDefined.Subscribe(str => sceneName = str);
            //await Tutor.OnStepFromSaveDefined.First(str => sceneName = str);
            
            await UniTask.WaitUntil(() =>
            {
                Debug.Log($"Tutor.TutorialIsLoad = {Tutor.TutorialIsLoad}");
                return Tutor.TutorialIsLoad ;
            });
            Debug.Log("tutorial TutorialIsLoad");
            
            Debug.Log($"SceneManager.LoadSceneAsync({sceneName});");
            var asyncOperation = SceneManager.LoadSceneAsync(sceneName);
            Debug.Log("loadingOperation started");
            await UniTask.WaitUntil(() => asyncOperation.isDone);
            Debug.Log("loadingOperation.IsLoaded finished");
            asyncOperation.allowSceneActivation = true;;
            Debug.Log("Scene activated");
            
            TutorialIsReady = true;
        }
        
        public void GoToAnotherScene(BuildingType buildingType)
        {
            Debug.Log($"buildingType {buildingType}");
            /*if (SceneLoader == null)
            {
                SceneLoader = GameObject.Find("Loading Screen");
            }
            
            SceneLoader.SetActive(true);
            SceneLoader.GetComponent<LoadingScreen>().SetSceneLoader(new SceneLoader());*/
            DestroyTutorialMask();
            switch (buildingType)
            {
                case BuildingType.Default:
                    break;
                case BuildingType.Farm:
                case BuildingType.Mill:
                case BuildingType.Barracks:
                    AdventureMainUI.Instance.ScreenLoader.GetComponent<LoadingScreen>().
                        LoadSceneAsync("CurrencyScene").Forget();
                    break;
                case BuildingType.Tavern:
                    AdventureMainUI.Instance.ScreenLoader.GetComponent<LoadingScreen>().
                        LoadSceneAsync("Tavern").Forget();
                    break;
                default:
                    Debug.LogError("It must not happen");
                    break;
            }

            TutorialIsReady = true;
            Tutor.Context._state.OnSceneChanged.OnNext(Unit.Default);
        }

        private IEnumerator SpawnCurrencyMainThread()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.01f);

                Model.Tick();
            }
        }

        public bool CheckForEnoughMoney(int gold, int food = 0, int power = 0)
        {
            var enoughGold = Model.Gold.Value >= gold;
            var enoughFood = Model.Food.Value >= food;
            var enoughPower = Model.Power.Value >= power;
            
            if (enoughGold && enoughFood && enoughPower)
            {
                return true;
            }

            throw new BalanceException();
        }

        public void DeductPurchasePrice(int gold, int food = 0, int power = 0)
        {
            try
            {
                CheckForEnoughMoney(gold, food, power);
                Debug.Log("DeductPurchasePrice success");
                Model.Gold.Value -= gold;
                Model.Food.Value -= food;
                Model.Power.Value -= power;
            }
            catch (Exception e)
            {
                Debug.Log(e);
                throw;
            }
        }

        public void CreateTutorialMask(GameObject target, float scale = 3f)
        {
            if (tutorialMask == null)
            {
                var canvas = FindObjectsOfType<Canvas>().First(c => c.gameObject.name == "Canvas");
                tutorialMask = Instantiate(tutorialMaskPrefab, canvas.transform);
                tutorialMask.GetComponent<TutorialMask>().SetTarget(target, scale);
            }
        }
        
        public void CreateTutorialMaskOnWorldCanvas(GameObject target, float scale)
        {
            if (tutorialMask == null)
            {
                var canvas = FindObjectsOfType<Canvas>().First(c => c.gameObject.name == "World Canvas");
                tutorialMask = Instantiate(tutorialMaskPrefab, canvas.transform);
                tutorialMask.GetComponent<TutorialMask>().SetTarget(target, scale);
                tutorialMask.transform.localScale = new Vector3(0.025f, 0.025f, 1f);
            }
        }
        
        public void DestroyTutorialMask()
        {
            if (tutorialMask != null)
            {
                Destroy(tutorialMask);
                tutorialMask = null;
            }
        }

        public ConversationUIView CreateConversation()
        {
            var canvas = FindObjectsOfType<Canvas>().First(c => c.gameObject.name == "Canvas");
            return Instantiate(conversationUIPrefab, canvas.transform).GetComponent<ConversationUIView>();
        }

        public GameObject CreateConversationBlocker()
        {
            
            var canvas = FindObjectsOfType<Canvas>().First(c => c.gameObject.name == "Canvas");
            return Instantiate(conversationUIPrefabBrocker, canvas.transform);
        }

        public void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                Model.Save();
            }
        }
        public void OnApplicationQuit()
        {
            Debug.Log("Model.Save();");
            Model.Save();
        }
    }
}