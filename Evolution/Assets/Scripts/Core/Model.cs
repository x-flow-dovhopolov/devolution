﻿using System.Collections.Generic;
using System.Linq;
using Core.ModelData;
using Core.ModelData.Buildings;
using Core.ModelData.Buildings.NpcModel;
using Core.ModelData.Buildings.SupportBuildings;
using Core.ModelData.Environment;
using Core.ModelData.NpcDiscover;
using Core.ModelData.Shop;
using Core.ModelData.StockItems;
using Core.SaveLoad;
using Core.Tutorial;
using GameObjects.EnvironmentView;
using UniRx;
using UnityEngine;

namespace Core
{    
    public enum BuildingType { Default, Mill, Farm, Barracks, Castle, Bank, Stock, Tavern}
    
    public class Model : ITick
    {
        private PlayerData PlayerData = new PlayerData(); 
        public PlayerData PlayerDataProperty
        {
            get => PlayerData;
            set => PlayerData = value;
        }

        private SaveManager SaveManager;
        public ReactiveProperty<int> Gold  = new ReactiveProperty<int>(0);
        public ReactiveProperty<int> Gems = new ReactiveProperty<int>(0);
        public ReactiveProperty<int> Food  = new ReactiveProperty<int>(0);
        public ReactiveProperty<int> Power = new ReactiveProperty<int>(0);
        public ReactiveProperty<int> DailyGame = new ReactiveProperty<int>(1);
        
        public int OpenedBuildingIndex = 0;
        public static int BuildingCounter = 0;
        
        public FogSlot FogSlots;
        public List<Slot> SlotsList;
        public FruitCollection FruitCollection;
        
        public Bank Bank = new Bank();
        public Stock Stock = new Stock();
        public Castle Castle = new Castle();
        
        public ShopManager ShopManager = new ShopManager();
        public NpcDiscoverManager NpcDiscoverManager = new NpcDiscoverManager();
        
        public Building CurrentBuilding
        {
            get => SlotsList[OpenedBuildingIndex].Building.Value;
            set => SlotsList[OpenedBuildingIndex].Building.Value = value ;
        }
        
        public Subject<Unit> DataLoaded { get; } = new Subject<Unit>();
        
        public Model()
        {
            SlotsList = new List<Slot>()
            {
                new Slot(1),
                new Slot(2), 
                new Slot(3), 
                new Slot(4)
            };
            
            FruitCollection = new FruitCollection();
            FogSlots = new FogSlot(0);
            
            DataLoaded.Subscribe(_ => GameManager.Instance.OnDataLoaded()).AddTo(GameManager.Instance);
            
            SaveManager = new SaveManager(this);
        }

        public void Tick()
        {
            foreach (var slot in SlotsList)
            {
                slot.Building.Value.Tick();
            }
        }
        
        public void OnCollectRareItem(StockItem stockItem) => FruitCollection.SetItem(stockItem);

        public void Save() => SaveManager.SavePlayerProgress();

        public void Load() => SaveManager.LoadPlayerProgress();

        public void SetLoadedData(PlayerData playerData)
        {
            FillUpBuildings(playerData.SlotsList);

            Gold.Value = playerData.Gold;
            Food.Value = playerData.Food;
            Power.Value = playerData.Power;
            Bank.IsActive = playerData.IsBankActive;
            Stock.IsActive = playerData.IsStockActive;
            Castle.CastleDataProperty = playerData.Castle;
            NpcDiscoverManager.SetNpcLibrary(playerData.NpcLibraries);
            FogSlots.GameEnvironmentProperty  = playerData.GameEnvironment;
            FruitCollection.SetFruitCollectionFromSave(playerData.FruitCollection);
            GameManager.Instance.Tutor.TutorialManagerDataProperty = playerData.TutorialManagerData;
            
            DataLoaded.OnNext(Unit.Default);
            Debug.Log("SetLoadedData succeeded");
        }
        
        private void FillUpBuildings(IReadOnlyList<BuildingModel> buildings)
        {
            for (var i = 0; i <  buildings.Count; i++)
            {
                var savedBuilding = buildings[i];
                
                var building = BuildingFactory.CreateBuilding(savedBuilding.BuildingType);
                
                building.Id = savedBuilding.Index;
                building.IsSpawning = savedBuilding.IsSpawning;
                building.CreateTime = savedBuilding.CreateTimeLeft;
                building.BuildingType = savedBuilding.BuildingType;
                building.UnitSpawnTimeProperty = savedBuilding.UnitSpawnTime;
                building.BuildingLevelProperty = savedBuilding.BuildingLevel;
                building.NpcSpawnedLevelProperty = savedBuilding.NpcSpawnedLevel;
                building.MaxUnitsOnSceneProperty = savedBuilding.CurrentMaxUnitsOnScene;

                building.SetManagerFromSave(savedBuilding.Manager);
                
                foreach (var npc in savedBuilding.UnitsList)
                {
                    var injectNpc = building.NpcFactory(npc.Level);
                    injectNpc.Level = npc.Level;
                    injectNpc.SpawnRateProperty = npc.SpawnRate;
                    injectNpc.GoldSpawnCountProperty = npc.GoldSpawnCount;
                    injectNpc.СanSpawnRareItemProperty = npc.СanSpawnRareItem;
                    injectNpc.СanSpawnPreperty = npc.СanSpawn;
                    for (var j = 0; j < npc.RareItemCount; j++)
                    {
                        var farmNpc = injectNpc as FarmNpc;
                        injectNpc.RareItemsList.Add(farmNpc?.RareItemFactory());
                    }
                    building.UnitsList.Add(injectNpc);
                }
                
                SlotsList[i].Building.Value = building;
            }
        }

        public SaveManager GetSaveManager() => SaveManager;
        
        public List<BuildingModel> SaveSlotData()
        {
            return (from buildingModel in SlotsList
                where buildingModel.Building.Value.BuildingType != BuildingType.Default
                select new BuildingModel()
                {
                    Index = buildingModel.Building.Value.Id,
                    CreateTime = buildingModel.Building.Value.CreateTime,
                    CreateTimeLeft = buildingModel.Building.Value.CreateTime - Time.time,
                    IsSpawning = buildingModel.Building.Value.IsSpawning,
                    BuildingType = buildingModel.Building.Value.BuildingType,
                    UnitsOnScene = buildingModel.Building.Value.UnitsOnScene,
                    BuildingLevel = buildingModel.Building.Value.BuildingLevelProperty,
                    UnitSpawnTime = buildingModel.Building.Value.UnitSpawnTimeProperty,
                    NpcSpawnedLevel = buildingModel.Building.Value.NpcSpawnedLevelProperty,
                    CurrentMaxUnitsOnScene = buildingModel.Building.Value.MaxUnitsOnSceneProperty,

                    UnitsList = buildingModel.Building.Value.SaveNpcList(),
                    Manager = buildingModel.Building.Value.Manager.SaveManagerData()
                }).ToList();
        }
    }
}