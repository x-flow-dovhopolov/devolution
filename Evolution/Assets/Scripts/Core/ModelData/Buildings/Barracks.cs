﻿using System.Diagnostics;
using Core.ModelData.Shop;
using Core.SaveLoad;
using UnityEngine;

namespace Core.ModelData.Buildings
{
    public class Barracks : Building
    {
        public override string AdventureSceneImage { get; set; } = "Buildings/Adventure/Academy";
        public override string CurrencySceneImage { get; set; } = "Buildings/BuildingFilling/CurrencyAcademy";
        public override string UniqueItem1 { get; set; } = "Buildings/BuildingFilling/Sword";
        public override string UniqueItem2 { get; set; } = default;
        public override string FillBarImage { get; set; } = "Buildings/BuildingFilling/AcademyShootingGallery";
        public override string RoadImage { get; set; }= "Buildings/BuildingFilling/NewCurrency/AcademyRoad";
        
        public override string CurrencyBuildingBarrel { get; } = "Buildings/BuildingFilling/AcademyBowlWithMap";

        public Barracks()
        {
            BuildingType = BuildingType.Barracks;

            var build = GameSetting.Builds.Find("Barrack");

            Name = LocaleRU.Buildings.Find("Barracks").Name;
            Description = LocaleRU.Buildings.Find("Barracks").Description;
            
            Manager.ManagerFoodProperty = 400000;
            Manager.ManagerCostProperty = 400000;
            CurrentMaxUnitsOnScene = build.NpcMax;
            BuildingPrice = build.BasePrice;
            //UnitSpawnTime = build.UnitSpawnTime;
            //UnitSpawnTime = 20f;
            BuildingFoodPrice = build.BasePriceFood;
            NpcSpawnTimeDecrease = build.NpcSpawnTimeDecrease;
        }
        
        public override void StaticPassiveTick(int offlineTime)
        {
            base.StaticPassiveTick(offlineTime);
            OfflineIncome.powerOfflineAmount += buildingOfflineCurrencyAmount;
        }

        public override void ActivateSupportBuilding()
        {
            return;
        }

        public override GameObject FarmNpcPrefabFactory(int level)
        {
            GameObject farmNpcPrefab = null;
            switch (level)
            {
                case 1 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Academy/Academy_npc_1", typeof(GameObject)) as GameObject;
                    break;
                case 2 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Academy/Academy_npc_2", typeof(GameObject)) as GameObject;
                    break;
                case 3 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Academy/Academy_npc_3", typeof(GameObject)) as GameObject;
                    break;
                case 4 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Academy/Academy_npc_4", typeof(GameObject)) as GameObject;
                    break;
                case 5 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Academy/Academy_npc_5", typeof(GameObject)) as GameObject;
                    break;
                case 6 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Academy/Academy_npc_6", typeof(GameObject)) as GameObject;
                    break;
            }
            
            return farmNpcPrefab;
        }

        public override int GetLevelUpPrice()
        {
            var newVal = (int) Mathf.Pow((10), BuildingLevel.Value) * 400000;
            return newVal;
        }

        public override int GetFoodLevelUpPrice()
        {
            var food = (int) Mathf.Pow((10), BuildingLevel.Value) * 400000;
            return food;
        }
    }
}
