﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.ModelData.Buildings.Managers;
using Core.ModelData.Buildings.Managers.VariousManagers;
using Core.ModelData.Buildings.Managers.VariousManagers.ManagerSupportData;
using Core.ModelData.Buildings.NpcModel;
using Core.ModelData.ExceptionHierarchy;
using Core.SaveLoad;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings
{
    public abstract class Building : ITick
    {
        public int BuildingFoodPriceProperty
        {
            get => BuildingFoodPrice;
            set => BuildingFoodPrice = value;
        }

        public int BuildingPriceProperty
        {
            get => BuildingPrice;
            set => BuildingPrice = value;
        }

        public abstract string UniqueItem1 { get; set; }
        public abstract string UniqueItem2 { get; set; }
        public abstract string FillBarImage { get; set; }
        public abstract string RoadImage { get; set; }
        public abstract string CurrencySceneImage { get; set; }
        public abstract string AdventureSceneImage { get; set; }
        public virtual string CurrencyBuildingGround { get; } = "Buildings/BuildingFilling/GroundField";
        public virtual string CurrencyBuildingBarrel { get; } = "Buildings/BuildingFilling/Barrel";

        public const int MaxLevel = 6;
        public const int MinSpawnTime = 1;
        public const int MaxAmountNpcInBuilding = 16;

        private int unitsOnScene;
        private int NpcSpawnedLevel;
        private ReactiveCollection<NPC> ReactiveUnitsList; // TODO do i need this collection ? 
        
        public int UnitsOnScene
        {
            get => unitsOnScene;
            set => unitsOnScene = value;
        }

        protected int BuildingPrice;
        protected string Description;
        protected float UnitSpawnTime;
        protected int CurrentMaxUnitsOnScene;  
        protected int BuildingFoodPrice;
        protected int NpcSpawnTimeDecrease;
        protected ReactiveProperty<int> BuildingLevel;

        public int Id;
        public string Name;
        public bool IsSpawning;
        public float CreateTime;
        public List<NPC> UnitsList;
        public Manager Manager;
        public BuildingType BuildingType;
        
        public int BuildingLevelProperty
        {
            get => BuildingLevel.Value;
            set
            {
                BuildingLevel = new ReactiveProperty<int>(value);
                LevelIncreased.OnNext(BuildingLevel.Value);
            }
        }
        public float UnitSpawnTimeProperty
        {
            get => UnitSpawnTime;
            set => UnitSpawnTime = value;
        }

        public int MaxUnitsOnSceneProperty
        {
            get => CurrentMaxUnitsOnScene;
            set => CurrentMaxUnitsOnScene = value;
        }

        public int NpcSpawnedLevelProperty
        {
            get => NpcSpawnedLevel;
            set
            {
                if (value == 0)
                {
                    NpcSpawnedLevel = 1;
                }

                NpcSpawnedLevel = value;
            }
        }

        private bool IsFreePlaceOnSceneProperty
        {
            get
            {
                var npcCount = unitsOnScene;
                if (IsSpawning) npcCount += 1;
                return npcCount >= CurrentMaxUnitsOnScene;
            }
        }

        public Subject<Unit> OnNpcMerged = new Subject<Unit>();
        
        public Subject<Manager> OnManagerSetToBuilding = new Subject<Manager>();
        public Subject<float> OnStartSpawn { get; } = new Subject<float>();
        public Subject<NPC> NpcBought { get; } = new Subject<NPC>();
        public Subject<NPC> NpcDestroyed { get; } = new Subject<NPC>();
        public Subject<int> LevelIncreased { get; } = new Subject<int>();
        public Subject<(NPC, Vector3?)> OnNpcSpawned { get; } = new Subject<(NPC, Vector3?)>();

        protected Building()
        {
            NpcSpawnedLevelProperty = 1;
            UnitsList = new List<NPC>();
            BuildingLevel = new ReactiveProperty<int>(0);
            
            Manager = new NullManager(this, GameManager.Instance.Tutor, new NullManagerItemData());
            
            ReactiveUnitsList = new ReactiveCollection<NPC>();
            ReactiveUnitsList.ObserveAdd().Subscribe(newValue =>
                {
                    GameManager.Instance.Model.NpcDiscoverManager.FindByTypeAndAddIfNotExists(newValue.Value);
                });
            
            UnitSpawnTime = 4f;
        }

        public void SetManager(ManagerType managerType)
        {
            ManagerFactory(managerType);
            OnManagerSetToBuilding.OnNext(Manager);
        }

        private Manager ManagerFactory(ManagerType managerType)
        {
            switch (managerType)
            {
                case ManagerType.Gold:
                    Manager = new GoldManager(this, GameManager.Instance.Tutor, new GoldManagerItemData());
                    break;
                case ManagerType.Speed:
                    Manager = new SpeedManager(this, GameManager.Instance.Tutor, new SpeedManagerItemData());
                    break;
                case ManagerType.Time:
                    Manager = new TimeManager(this, GameManager.Instance.Tutor, new TimeManagerItemData());
                    break;
                case ManagerType.Default:
//                    Debug.Log("ManagerType.Default");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return Manager;
        }
        
        public void SetNewSpawnTime(int val) => UnitSpawnTime = val;
        public virtual int GetFoodLevelUpPrice() => BuildingFoodPrice;
        public virtual int GetLevelUpPrice() => (int) (BuildingPrice);
        private bool CheckForMaxUnitsOnScene() => unitsOnScene >= CurrentMaxUnitsOnScene;
        public int GetBuildingMaxNpcOnSceneOnNextLevel() => MaxUnitsOnSceneProperty + 1;

        public void SetSpawnFinishTime()
        {
            if (IsSpawning || CheckForMaxUnitsOnScene()) return;

            OnStartSpawn.OnNext(GetBuildingSpawnTime());
            CreateTime = Time.time + GetBuildingSpawnTime();
            IsSpawning = true;
            
            /*if (!GameManager.Instance.Tutor.IsBarrackBlockFinished)
            {
                if (GameManager.Instance.Tutor.Context._state.StartSpawnNpc.HasObservers)
                {
                    GameManager.Instance.Tutor.Context._state.StartSpawnNpc.OnNext(Unit.Default);
                    GameManager.Instance.Tutor.Context._state.StartSpawnNpc.Dispose();
                }
            }*/
            if (GameManager.Instance.Tutor.Context._state.StartSpawnNpc.HasObservers)
            {
                GameManager.Instance.Tutor.Context._state.StartSpawnNpc.OnNext(Unit.Default);
                GameManager.Instance.Tutor.Context._state.StartSpawnNpc.Dispose();
            }
        }

        protected int buildingOfflineCurrencyAmount;
        public virtual void StaticPassiveTick(int offlineTime)
        {
            buildingOfflineCurrencyAmount = 0;
            
            foreach (var npc in UnitsList)
            {
                buildingOfflineCurrencyAmount += npc.OfflineCurrencyCombine(offlineTime);
            }
        }
        
        public void Tick()
        {
            if (BuildingType == BuildingType.Default) return;

            if (Manager.ManagerDataProperty.managerType != ManagerType.Default)
            {
                Manager.Tick();
                SetSpawnFinishTime();
            }
            
            //Debug.Log($"Building CreateTime = {CreateTime} // Time.time {Time.time}");
            if (IsSpawning && Time.time >= CreateTime)
            {
                IsSpawning = false;
                var newNpc = NpcFactory(NpcSpawnedLevelProperty);
                AddNpcToCollectionAndDiscover(newNpc);
                Debug.Log($"OnNpcSpawned");
                OnNpcSpawned.OnNext((newNpc, null));
            }

            foreach (var npc in UnitsList)
            {
                npc.Tick();
            }
        }

        public void Merge(NPC npc1, NPC npc2, Vector3 position)
        {
            unitsOnScene -= 2;
            UnitsList.Remove(npc1);
            UnitsList.Remove(npc2);
            NpcDestroyed.OnNext(npc1);
            NpcDestroyed.OnNext(npc2);

            var newNpc = NpcFactory(npc1.Level + 1);
            
            AddNpcToCollectionAndDiscover(newNpc);
            
            OnNpcSpawned.OnNext((newNpc, position));
            OnNpcMerged.OnNext(Unit.Default);
        }

        public NPC NpcFactory(int level)
        {
            NPC npc = null;

            switch (BuildingType)
            {
                case BuildingType.Farm:
                    npc = new FarmNpc(this, level);
                    break;
                case BuildingType.Mill:
                    npc = new MillNpc(this, level);
                    break;
                case BuildingType.Barracks:
                    npc = new BarrackNpc(this,level);
                    break;
                case BuildingType.Default:
                    break;
                default:
                    Debug.LogError("It might not happen");
                    break;
            }

            unitsOnScene++;
            return npc;
        }

        public float GetBuildingSpawnTime()
        {
            var newSpawnTme = 0f;
            newSpawnTme = UnitSpawnTime;
            return newSpawnTme < 0 ? 0 : newSpawnTme;
        }

        public float GetBuildingSpawnTimeOnNextLevel()
        {
            var newSpawnTme = 0f;
            newSpawnTme = UnitSpawnTime - NpcSpawnTimeDecrease;
            return newSpawnTme < 0 ? 0 : newSpawnTme;
        }

        public void BuyNpc(int npcBoughtLevel, int gold, int food)
        {
            if (GameManager.Instance.Model.CurrentBuilding.IsFreePlaceOnSceneProperty)
            {
                throw new MaxNpcInBuildingException(CurrentMaxUnitsOnScene);
                return;
            }
            
            var newNpc = NpcFactory(npcBoughtLevel);
            UnitsList.Add(newNpc);
            NpcBought.OnNext(newNpc);
            GameManager.Instance.DeductPurchasePrice(gold, food);
        }

        private void AddNpcToCollectionAndDiscover(NPC newNpc)
        {
            UnitsList.Add(newNpc);
            ReactiveUnitsList.Add(newNpc);
        }

        public abstract void ActivateSupportBuilding();
        public abstract GameObject FarmNpcPrefabFactory(int level);

        public void SetManagerFromSave(ManagerModel savedBuildingManager)
        {
            var manager = ManagerFactory(savedBuildingManager.ManagerType);

            manager.ManagerDataProperty.managerType = savedBuildingManager.ManagerType;
            manager.AbilityIsOnProperty = savedBuildingManager.AbilityIsOn;
            manager.IsAbilityEnableProperty = savedBuildingManager.IsAbilityEnable;
            manager.CooldownFinishTimeProperty = savedBuildingManager.CooldownFinishTIme;
            manager.AbilityFinishTimeProperty = savedBuildingManager.AbilityFinishTIme;

            if (manager.AbilityIsOnProperty)
            {
                //BUG on Owl. Will cause immediately reward 
                manager.EnableManagerAbility();
            }
        }
        
        public List<SaveLoad.NpcModel> SaveNpcList()
        {
            return (from npcModel in UnitsList
                select new SaveLoad.NpcModel()
                {
                    Level = npcModel.Level,
                    SpawnRate = npcModel.SpawnRateProperty,
                    GoldSpawnCount = npcModel.GoldSpawnCountProperty,
                    СanSpawnRareItem = npcModel.СanSpawnRareItemProperty,
                    RareItemCount = npcModel.RareItemsList.Count,
                    СanSpawn = npcModel.СanSpawnPreperty - Time.time
                }).ToList();
        }
    }
}