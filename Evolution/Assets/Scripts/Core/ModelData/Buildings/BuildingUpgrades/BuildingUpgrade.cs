﻿using Core.ModelData.ExceptionHierarchy;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.BuildingUpgrades
{
    public abstract class BuildingUpgrade : Item, IUpgrade
    {
        protected Building Building;

        protected BuildingUpgrade(Building building)
        {
            Building = building;
        }

        protected void IsUpgradeEnable()
        {
            try
            {
                CheckForCastleLevel();
                Debug.Log("After success CheckForCastleLevel");
                
                CheckIsEnoughCurrency();
                Debug.Log("After success CheckIsEnoughCurrency");
                DeductTheCost();
            }
            catch (AdventureException e)
            {
                Debug.Log(e);
                throw;
            }
        }
        private void CheckIsEnoughCurrency()
        {
            var isEnoughMoney = GameManager.Instance.Model.Gold.Value >= GoldCosts &&
                                GameManager.Instance.Model.Food.Value >= FoodCosts;
                
            if (!isEnoughMoney) throw new BalanceException();
        }
        
        private void CheckForCastleLevel()
        {
            //GameManager.Instance.Model.Castle.LevelProperty = 0;
            Debug.Log($"Building level {Building.BuildingLevelProperty} --- Castle level {GameManager.Instance.Model.Castle.LevelProperty}");
            if (Building.BuildingLevelProperty >= GameManager.Instance.Model.Castle.LevelProperty)
                throw new CastleException();
        }

        private void DeductTheCost()
        {
            GameManager.Instance.DeductPurchasePrice(GoldCosts, FoodCosts);
            Building.BuildingLevelProperty += 1;
        }

        protected bool CheckForMaxSpawnedNpcLevel() => Building.NpcSpawnedLevelProperty == Building.MaxLevel;
        //protected bool CheckForMaxSpawnTime() => (int) Building.UnitSpawnTimeProperty == Building.MinSpawnTime;
        protected bool CheckForMaxSpawnTime() => (int) Building.GetBuildingSpawnTimeOnNextLevel() <= Building.MinSpawnTime;

        protected bool CheckForMaxNpcInBuilding() =>
            Building.MaxUnitsOnSceneProperty == Building.MaxAmountNpcInBuilding;

        public abstract void Upgrade();
        
        public override int CalculateGoldCosts()
        {
            GoldCosts = Building.GetLevelUpPrice();
            return GoldCosts;
        }

        public override int CalculateFoodCosts()
        {
            FoodCosts = Building.GetFoodLevelUpPrice();
            return FoodCosts;
        }

        protected void FinishTutorialUpgradeStep()
        {
            GameManager.Instance.Tutor.Context._state.OnBuildingUpgrade.OnNext(Unit.Default);
            /*if (!GameManager.Instance.Tutor.IsTutorialBuildingLevelUpPlayed)
            {
                GameManager.Instance.Tutor.IsTutorialBuildingLevelUpPlayed = true;
                GameManager.Instance.Tutor.Context._state.OnBuildingUpgrade.OnNext(Unit.Default);
            }*/
        }
    }
}