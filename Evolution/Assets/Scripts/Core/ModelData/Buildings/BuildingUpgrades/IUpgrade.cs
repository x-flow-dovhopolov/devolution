﻿namespace Core.ModelData.Buildings.BuildingUpgrades
{
    public interface IUpgrade
    {
        void Upgrade();
    }
}