﻿using Core.ModelData.ExceptionHierarchy;
using UnityEngine;

namespace Core.ModelData.Buildings.BuildingUpgrades
{
    public class IncreaseMaxNpcOnScene : BuildingUpgrade
    {
        public IncreaseMaxNpcOnScene(Building building) : base(building)
        {
            Name = LocaleRU.BuildingUprgades.Find("npc_max_count").Name;
            Description = LocaleRU.BuildingUprgades.Find("npc_max_count").Description;
            /*Name = "Увеличение максимального количества существ на сцене";
            Description = "Увеличение максимального количества существ на сцене";*/
        }

        public override void Upgrade()
        {
            try
            {
                if (CheckForMaxNpcInBuilding())
                {
                    throw new MaxNpcInBuildingException(Building.MaxUnitsOnSceneProperty);
                }
                Debug.Log($"After success MaxNpcInBuildingException");

                IsUpgradeEnable();
                Debug.Log($"After success IsUpgradeEnable();");

                GameManager.Instance.Model.CurrentBuilding.MaxUnitsOnSceneProperty += 1;

                FinishTutorialUpgradeStep();
            }
            catch (AdventureException e)
            {
                Debug.Log(e);
                throw;
            }
        }

        public override string CalculateUpgrade()
        {
            if (CheckForMaxNpcInBuilding()) return "Max";

            ImproveValue = $"{Building.MaxUnitsOnSceneProperty} => {Building.GetBuildingMaxNpcOnSceneOnNextLevel()}";
            return ImproveValue;
        }
    }
}