﻿using Core.ModelData.ExceptionHierarchy;
using UnityEngine;

namespace Core.ModelData.Buildings.BuildingUpgrades
{
    public class IncreaseSpawnedNpcLevel : BuildingUpgrade
    {
        public IncreaseSpawnedNpcLevel(Building building) : base(building)
        {
            Name = LocaleRU.BuildingUprgades.Find("npc_spawn_level").Name;
            Description = LocaleRU.BuildingUprgades.Find("npc_spawn_level").Description;
            /*Name = "Производится существо следующего уровня";
            Description = "Производится существо следующего уровня";*/
        }

        public override void Upgrade()
        {
            
            Debug.Log($"Context: Transition to {GetType().Name}.");
            try
            {
                if (CheckForMaxSpawnedNpcLevel())
                {
                    throw new MaxNpcLevelException();
                }

                Debug.Log($"After success MaxNpcLevelException");

                IsUpgradeEnable();
                Debug.Log($"After success IsUpgradeEnable();");

                Debug.Log($" GameManager.Instance.Model.CurrentBuilding. { Building.BuildingType}" +
                          $" and { Building.NpcSpawnedLevelProperty}");
                Building.NpcSpawnedLevelProperty += 1;

                Debug.Log($" GameManager.Instance.Model.CurrentBuilding. {Building.BuildingType}" +
                          $" and { Building.NpcSpawnedLevelProperty}");
                FinishTutorialUpgradeStep();
            }
            catch (AdventureException e)
            {
                Debug.Log(e);
                throw;
            }
        }

        public override string CalculateUpgrade()
        {
            if (CheckForMaxSpawnedNpcLevel()) return "Max";

            var level = GameManager.Instance.Model.CurrentBuilding.NpcSpawnedLevelProperty;
            ImproveValue = $"{level} => {level + 1}";
            return ImproveValue;
        }
    }
}