﻿namespace Core.ModelData.Buildings.BuildingUpgrades
{
    public abstract class Item
    {
        public string Name;
        public string Description;
        public string ImproveValue;
        public int GoldCosts;
        public int FoodCosts;
        
        public abstract string CalculateUpgrade();
        public abstract int CalculateGoldCosts();
        public abstract int CalculateFoodCosts();
    }
}