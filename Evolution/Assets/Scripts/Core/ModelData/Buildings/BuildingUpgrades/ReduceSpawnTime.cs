﻿using Core.ModelData.ExceptionHierarchy;
using UnityEngine;

namespace Core.ModelData.Buildings.BuildingUpgrades
{
    public class ReduceSpawnTime : BuildingUpgrade
    {
        public ReduceSpawnTime(Building building) : base(building)
        {
            Name = LocaleRU.BuildingUprgades.Find("spawn_time").Name;
            Description = LocaleRU.BuildingUprgades.Find("spawn_time").Description;
            /*Name = "Ускорение обучение";
            Description = "Уменьшение времени создания";*/
        }
        
        public override void Upgrade()
        {
            try
            {
                if(CheckForMaxSpawnTime())
                {
                    throw new SpawnException();
                }
                Debug.Log($"After success SpawnException");
                
                IsUpgradeEnable();
                Debug.Log($"After success IsUpgradeEnable();");
            
                Building.SetNewSpawnTime((int) Building.GetBuildingSpawnTimeOnNextLevel());

                FinishTutorialUpgradeStep();
            }
            catch (AdventureException e)
            {
                Debug.Log(e);
                throw;
            }
        }

        public override string CalculateUpgrade()
        {
            if (CheckForMaxSpawnTime()) return "Max";
            
            ImproveValue = $"{Building.GetBuildingSpawnTime()} => {Building.GetBuildingSpawnTimeOnNextLevel()}";
            
            return ImproveValue;
        }
    }
}