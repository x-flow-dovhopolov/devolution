﻿using System;
using Core.ModelData.Shop;
using UnityEngine;

namespace Core.ModelData.Buildings
{
    [Serializable] 
    public class DefaultBuilding : Building
    {
        public override string AdventureSceneImage { get; set; } = "Buildings/CreateBuild";
        public override string RoadImage { get; set; } = default;
        public override string CurrencySceneImage { get; set; } = default;
        public override string UniqueItem1 { get; set; }
        public override string UniqueItem2 { get; set; }
        public override string FillBarImage { get; set; } = default;

        public DefaultBuilding()
        {
            BuildingType =  BuildingType.Default;
        }
        
        public override void ActivateSupportBuilding()
        {
            return;
        }

        public override GameObject FarmNpcPrefabFactory(int level)
        {
            return null;
        }
    }
}