﻿using Core.SaveLoad;
using UI;
using UnityEngine;

namespace Core.ModelData.Buildings
{
    public class Farm : Building
    {
        public override string AdventureSceneImage { get; set; } = "Buildings/Adventure/Farm";
        public override string CurrencySceneImage { get; set; } = "Buildings/BuildingFilling/CurrencyFarm";
        public override string UniqueItem1 { get; set; } = "Buildings/BuildingFilling/Spade and chest";
        public override string UniqueItem2 { get; set; } = "Buildings/BuildingFilling/Haystack";
        public override string RoadImage { get; set; }= "Buildings/BuildingFilling/NewCurrency/FarmRoad";
        public override string FillBarImage { get; set; } = "Buildings/BuildingFilling/FarmField";

        public Farm()
        {
            BuildingType = BuildingType.Farm;
            
            var build = GameSetting.Builds.Find("Farm");

            Name = LocaleRU.Buildings.Find("Farm").Name;
            Description = LocaleRU.Buildings.Find("Farm").Description;
            
            Manager.ManagerFoodProperty = 100;
            Manager.ManagerCostProperty = 5000;
            CurrentMaxUnitsOnScene = build.NpcMax;
            BuildingPrice = build.BasePrice;
            //UnitSpawnTime = 15f;
            BuildingFoodPrice = build.BasePriceFood;
            NpcSpawnTimeDecrease = build.NpcSpawnTimeDecrease;
        }
        
        public override void StaticPassiveTick(int offlineTime)
        {
            base.StaticPassiveTick(offlineTime);
            OfflineIncome.foodOfflineAmount += buildingOfflineCurrencyAmount;
        }

        public override void ActivateSupportBuilding()
        {
            GameManager.Instance.Model.Stock.SetStockActive();
        }

        public override int GetLevelUpPrice()
        {
            var newVal = (int) Mathf.Pow((10), BuildingLevel.Value) * 6000;
            return newVal;
        }

        public override int GetFoodLevelUpPrice()
        {
            var food = (int) Mathf.Pow((10), BuildingLevel.Value) * 3000;
            return food;
        }

        public override GameObject FarmNpcPrefabFactory(int level)
        {
            GameObject farmNpcPrefab = null;
            switch (level)
            {
                case 1 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Farm/Farm_npc_1", typeof(GameObject)) as GameObject;
                    break;
                case 2 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Farm/Farm_npc_2", typeof(GameObject)) as GameObject;
                    break;
                case 3 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Farm/Farm_npc_3", typeof(GameObject)) as GameObject;
                    break;
                case 4 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Farm/Farm_npc_4", typeof(GameObject)) as GameObject;
                    break;
                case 5 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Farm/Farm_npc_5", typeof(GameObject)) as GameObject;
                    break;
                case 6 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Farm/Farm_npc_6", typeof(GameObject)) as GameObject;
                    break;
            }
            
            return farmNpcPrefab;
        }
    }
}
