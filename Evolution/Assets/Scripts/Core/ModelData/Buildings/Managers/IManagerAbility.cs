﻿namespace Core.ModelData.Buildings.Managers
{
    public interface IManagerAbility
    {
        void EnableManagerAbility();
        void DisableManagerAbility();
    }
}