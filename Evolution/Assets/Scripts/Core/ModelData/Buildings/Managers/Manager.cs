﻿using Core.SaveLoad;
using Core.Tutorial;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.Managers
{
    public abstract class Manager : IManagerAbility, ITick
    {
        protected Building Building;
        private TutorialManager TutorialManager;
        private ManagerItemData ManagerItemData;
        
        public int ManagerBooster = 1;
        protected float ManagerSpeedBooster = 1;
        
        protected Manager(Building building, TutorialManager manager, ManagerItemData managerItemData)
        {
            Building = building;
            TutorialManager = manager;
            ManagerItemData = managerItemData;
        }
        
        public abstract void EnableManagerAbility();
        public abstract void DisableManagerAbility();
        
        public void CheckAbility()
        {
            if (!IsAbilityEnableProperty) return;
            
            if (TutorialManager.CurrentStep == ETutorialStep.ManagerAbility)
            {
                GameManager.Instance.DestroyTutorialMask();
                TutorialManager.Context._state.OnManagerAbilityUsed.OnNext(Unit.Default);
            }
            /*if (!GameManager.Instance.Tutor.IsTutorialManagerPlayed)
            {
                GameManager.Instance.DestroyTutorialMask();
                TutorialManager.Context._state.OnManagerAbilityUsed.OnNext(Unit.Default);
            }*/

            AbilityIsOnProperty = true;
            IsAbilityEnableProperty = false;
            ActivateAbility.OnNext(Unit.Default);
            EnableManagerAbility(); // add and need to test

            AbilityFinishTimeProperty = Time.time + ManagerItemData.PowerUpTime;
            CooldownFinishTimeProperty = Time.time + ManagerItemData.PowerUpTime + ManagerItemData.CooldownTime;
        }
        
        public void Tick()
        {
            if (Time.time > AbilityFinishTimeProperty && AbilityIsOnProperty)
            {
                AbilityIsOnProperty = false;
                IsAbilityDiActiveEvent.OnNext(Unit.Default);
                DisableManagerAbility();
            }

            if (Time.time > CooldownFinishTimeProperty && !AbilityIsOnProperty && !IsAbilityEnableProperty)
            {
                IsAbilityEnableProperty = true;
                IsAbilityActiveEvent.OnNext(Unit.Default);
            }
        }

        public ManagerModel SaveManagerData()
        {
            return new ManagerModel()
            {
                AbilityIsOn = AbilityIsOnProperty,
                IsAbilityEnable = IsAbilityEnableProperty,
                ManagerType = ManagerItemData.managerType,
                AbilityFinishTIme = AbilityFinishTimeProperty - Time.time,
                CooldownFinishTIme = CooldownFinishTimeProperty - Time.time
            };
        }
        
        public Subject<Unit> ActivateAbility { get; } = new Subject<Unit>();
        public Subject<Unit> IsAbilityActiveEvent { get; } = new Subject<Unit>();
        public Subject<Unit> IsAbilityDiActiveEvent { get; } = new Subject<Unit>();

        #region Property
        
        public ManagerItemData ManagerDataProperty => ManagerItemData;
        
        public string ManagerSprite => ManagerItemData.ManagerImage;

        public bool IsAbilityEnableProperty { get; set; }

        public bool AbilityIsOnProperty { get; set; }

        public float AbilityFinishTimeProperty { get; set; }

        public float CooldownFinishTimeProperty { get; set; }

        public int ManagerCostProperty
        {
            get => ManagerItemData.GoldCosts;
            set => ManagerItemData.GoldCosts = value;
        }

        public int ManagerFoodProperty
        {
            get => ManagerItemData.FoodCosts;
            set => ManagerItemData.FoodCosts = value;
        }

        #endregion
    }
}