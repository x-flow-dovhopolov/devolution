﻿using Core.ModelData.Buildings.BuildingUpgrades;

namespace Core.ModelData.Buildings.Managers
{
    public class ManagerItem : ManagerItemData
    {
        public override string ManagerImage { get;} = "Buildings/Managers/BearManager";

        public ManagerItem()
        {
            var item = LocaleRU.Managers.Find("golden_lord1");
            Name = item.Name;
            ImproveValue = item.Job;
            Description = item.Description;
            GoldCosts = 2000;
            FoodCosts = 0;
        }
    }
}