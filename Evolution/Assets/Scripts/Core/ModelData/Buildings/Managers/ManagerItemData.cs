﻿using Core.ModelData.Buildings.BuildingUpgrades;

namespace Core.ModelData.Buildings.Managers
{
    public enum ManagerType { Default, Gold, Speed, Time}
    public abstract class ManagerItemData : Item
    {
        public abstract string ManagerImage { get;}

        public ManagerType managerType;
        public int PowerUpTime { get; set; }
        public int CooldownTime { get; set; }

        public sealed override string CalculateUpgrade()
        {
            return ImproveValue;
        }

        public sealed override int CalculateGoldCosts()
        {
            return GoldCosts;
        }

        public sealed override int CalculateFoodCosts()
        {
            return FoodCosts;
        }
    }
}