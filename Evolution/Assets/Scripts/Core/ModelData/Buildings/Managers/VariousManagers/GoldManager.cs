﻿using Core.Tutorial;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.Managers.VariousManagers
{
    public class GoldManager : Manager
    {
        public GoldManager(Building building, TutorialManager manager, ManagerItemData managerItemData) 
            : base(building, manager, managerItemData)
        {
            //IsBoughtProperty = new ReactiveProperty<bool>(true);
        }
        
        public override void EnableManagerAbility()
        {
            Debug.Log("GoldManager EnableManagerAbility");
            ManagerBooster = 2;
        }

        public override void DisableManagerAbility()
        {
            Debug.Log("GoldManager DisableManagerAbility");
            ManagerBooster = 1;
        }
    }
}