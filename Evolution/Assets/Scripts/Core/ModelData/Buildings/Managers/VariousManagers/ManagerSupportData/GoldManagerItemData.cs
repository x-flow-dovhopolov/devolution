﻿using System;

namespace Core.ModelData.Buildings.Managers.VariousManagers.ManagerSupportData
{
    public class GoldManagerItemData : ManagerItemData
    {
        public override string ManagerImage { get;} = "Buildings/Managers/BearManager";

        public GoldManagerItemData()
        {
            managerType = ManagerType.Gold;
            
            var item = LocaleRU.Managers.Find("golden_lord1");
            Name = item.Name;
            ImproveValue = item.Job;
            Description = item.Description;
            
            var manager = GameSetting.Managers.Find("Strong");
            GoldCosts = Convert.ToInt32(manager.BasePrice);
            FoodCosts = Convert.ToInt32(manager.BasePriceFood);
            PowerUpTime = Convert.ToInt32(manager.PowerUpTime);
            CooldownTime = Convert.ToInt32(manager.CooldownTime);
        }
    }
}