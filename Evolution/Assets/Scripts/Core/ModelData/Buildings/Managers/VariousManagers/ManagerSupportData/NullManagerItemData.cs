﻿namespace Core.ModelData.Buildings.Managers.VariousManagers.ManagerSupportData
{
    public class NullManagerItemData : ManagerItemData
    {
        public override string ManagerImage { get; } = default; //"Buildings/Managers/Placeholder";

        public NullManagerItemData()
        {
            managerType = ManagerType.Default;

            Name = "";
            ImproveValue = "";
            Description = "";

            GoldCosts = 0;
            FoodCosts = 0;
            PowerUpTime = 0;
            CooldownTime = 0;
        }
    }
}