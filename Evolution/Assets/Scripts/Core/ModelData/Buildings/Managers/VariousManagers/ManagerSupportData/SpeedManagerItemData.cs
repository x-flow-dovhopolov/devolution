﻿using System;

namespace Core.ModelData.Buildings.Managers.VariousManagers.ManagerSupportData
{
    public class SpeedManagerItemData : ManagerItemData
    {
        public override string ManagerImage { get;} = "Buildings/Managers/СheetahManager";

        public SpeedManagerItemData()
        {
            managerType = ManagerType.Speed;
            
            var item = LocaleRU.Managers.Find("speed_master1");
            Name = item.Name;
            ImproveValue = item.Job;
            Description = item.Description;
            
            var manager = GameSetting.Managers.Find("Speed");
            GoldCosts = Convert.ToInt32(manager.BasePrice);
            FoodCosts = Convert.ToInt32(manager.BasePriceFood);
            PowerUpTime = Convert.ToInt32(manager.PowerUpTime);
            CooldownTime = Convert.ToInt32(manager.CooldownTime);
        }
    }
}