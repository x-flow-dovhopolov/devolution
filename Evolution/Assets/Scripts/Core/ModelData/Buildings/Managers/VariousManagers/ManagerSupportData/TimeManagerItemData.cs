﻿using System;

namespace Core.ModelData.Buildings.Managers.VariousManagers.ManagerSupportData
{
    public class TimeManagerItemData : ManagerItemData
    {
        public int PowerUpTime { get; }
        public override string ManagerImage { get;} = "Buildings/Managers/OwlManager";

        public TimeManagerItemData()
        {
            managerType = ManagerType.Time;
            
            var item = LocaleRU.Managers.Find("time_mistress1");
            Name = item.Name;
            ImproveValue = item.Job;
            Description = item.Description;
            
            var manager = GameSetting.Managers.Find("Time");
            GoldCosts = Convert.ToInt32(manager.BasePrice);
            FoodCosts = Convert.ToInt32(manager.BasePriceFood);
            PowerUpTime = Convert.ToInt32(manager.PowerUpTime);
            CooldownTime = Convert.ToInt32(manager.CooldownTime);
        }
    }
}