﻿using Core.Tutorial;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.Managers.VariousManagers
{
    public class NullManager : Manager
    {
        public NullManager(Building building, TutorialManager manager, ManagerItemData managerItemData) 
            : base(building, manager, managerItemData)
        {
            //IsBoughtProperty = new ReactiveProperty<bool>(false);
        }

        public override void EnableManagerAbility()
        {
            Debug.Log("NullManager EnableManagerAbility");
            return;
        }

        public override void DisableManagerAbility()
        {
            Debug.Log("NullManager DisableManagerAbility");
            return;
        }
    }
}