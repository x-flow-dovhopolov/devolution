﻿using Core.Tutorial;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.Managers.VariousManagers
{
    public class SpeedManager : Manager
    {
        public SpeedManager(Building building, TutorialManager manager, ManagerItemData managerItemData) 
            : base(building, manager, managerItemData)
        {
            //IsBoughtProperty = new ReactiveProperty<bool>(true);
        }
        
        public override void EnableManagerAbility()
        {
            Debug.Log("SpeedManager EnableManagerAbility");
            ManagerBooster = 5;
            ManagerSpeedBooster = 0.5f;
        }

        public override void DisableManagerAbility()
        {
            Debug.Log("SpeedManager DisableManagerAbility");
            ManagerBooster = 1;
            ManagerSpeedBooster = 1;
        }
    }
}