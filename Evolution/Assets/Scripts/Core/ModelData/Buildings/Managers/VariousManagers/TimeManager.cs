﻿using Core.Tutorial;
using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.Managers.VariousManagers
{
    public class TimeManager : Manager
    {
        
        public TimeManager(Building building, TutorialManager manager, ManagerItemData managerItemData) 
            : base(building, manager, managerItemData)
        {
            //IsBoughtProperty = new ReactiveProperty<bool>(true);
        }
        
        public override void EnableManagerAbility()
        {
            ManagerBooster = 10;
            
            Debug.Log("TimeManager EnableManagerAbility");
            var currency = " gold";
            switch (Building.BuildingType)
            {
              case BuildingType.Mill:
                  GameManager.Instance.Model.Gold.Value += 50000;
                  break;
              case BuildingType.Farm:
                  currency = " food";
                  GameManager.Instance.Model.Food.Value += 50000;
                  break;
              case BuildingType.Barracks:
                  currency = " power";
                  GameManager.Instance.Model.Power.Value += 50000;
                  break;
                  
            }
            //TODO Create rewand panel for this manager
            InsideBuildingUiHelper.Instance.ShowErrorPrefab($"You get 50000{currency}");
        }

        public override void DisableManagerAbility()
        {
            ManagerBooster = 1;
            Debug.Log("TimeManager DisableManagerAbility");
            return;
        }
    }
}