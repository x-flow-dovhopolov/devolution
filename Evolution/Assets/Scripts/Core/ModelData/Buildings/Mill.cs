﻿using Core.SaveLoad;
using UI;
using UnityEngine;

namespace Core.ModelData.Buildings
{
    public class Mill : Building
    {
        public override string AdventureSceneImage { get; set; } = "Buildings/Adventure/Mill";
        public override string CurrencySceneImage { get; set; } = "Buildings/BuildingFilling/CurrencyMill";
        public override string UniqueItem1 { get; set; } = "Buildings/BuildingFilling/Spade and chest";
        public override string UniqueItem2 { get; set; } = default;
        public override string FillBarImage { get; set; } = "Buildings/BuildingFilling/MillCorn";
        public override string RoadImage { get; set; }= "Buildings/BuildingFilling/NewCurrency/MillRoad";
        public override string CurrencyBuildingGround { get; } = "Buildings/BuildingFilling/GroundGarden";

        public Mill()
        {
            BuildingType = BuildingType.Mill;

            var build = GameSetting.Builds.Find("Mill");

            Name = LocaleRU.Buildings.Find("Mill").Name;
            Description = LocaleRU.Buildings.Find("Mill").Description;
            
            Manager.ManagerFoodProperty = 0;
            Manager.ManagerCostProperty = 2000;
            CurrentMaxUnitsOnScene = build.NpcMax;
            BuildingPrice = build.BasePrice;
            //UnitSpawnTime = build.UnitSpawnTime;
            //UnitSpawnTime = 10f;
            BuildingFoodPrice = build.BasePriceFood;
            NpcSpawnTimeDecrease = build.NpcSpawnTimeDecrease;
        }

        public override void StaticPassiveTick(int offlineTime)
        {
            base.StaticPassiveTick(offlineTime);
            OfflineIncome.goldOfflineAmount += buildingOfflineCurrencyAmount;
        }

        public override void ActivateSupportBuilding()
        {
            GameManager.Instance.Model.Bank.SetBankActive();
        }

        public override int GetLevelUpPrice()
        {
            var newVal = (int) Mathf.Pow(10, BuildingLevel.Value) * 70;
            return newVal;
        }
        
        public override GameObject FarmNpcPrefabFactory(int level)
        {
            GameObject farmNpcPrefab = null;
            switch (level)
            {
                case 1 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Mill/Mill_npc_1", typeof(GameObject)) as GameObject;
                    break;
                case 2 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Mill/Mill_npc_2", typeof(GameObject)) as GameObject;
                    break;
                case 3 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Mill/Mill_npc_3", typeof(GameObject)) as GameObject;
                    break;
                case 4 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Mill/Mill_npc_4", typeof(GameObject)) as GameObject;
                    break;
                case 5 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Mill/Mill_npc_5", typeof(GameObject)) as GameObject;
                    break;
                case 6 : 
                    farmNpcPrefab = Resources.Load("Buildings/Npc/Mill/Mill_npc_6", typeof(GameObject)) as GameObject;
                    break;
            }
            
            return farmNpcPrefab;
        }
    }
}
