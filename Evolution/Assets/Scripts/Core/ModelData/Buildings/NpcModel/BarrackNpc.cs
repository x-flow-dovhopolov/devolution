﻿using Core.ModelData.Buildings.NpcModel.NpcDialogs;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.NpcModel
{
    public class BarrackNpc : NPC
    {
        public BarrackNpc(Building building, int level) : base(building, level)
        {
        }

        protected override void InitByType()
        {
            var npc = GameSetting.NPCs.Find("Barrack");

            var npcNameKey = "Barrack_level_" + Level;
            Name = LocaleRU.Units.Find(npcNameKey).Name;
            
            Type = npc.Type;
            Icon = npc.Icon;
            SpawnRate = npc.SpawnTime;
            SpawnRateByClick = npc.SpawnRateByClick;
            GoldSpawnCount = int.Parse(npc.GoldSpawnCount);
        }

        public override void CurrencyCombine()
        {
            base.CurrencyCombine();
            GameManager.Instance.Model.Power.Value += CurrencyAmount;
        }

        public override void ShowNpcDialog()
        {
            var chance = Random.Range(0, 50);
            if (chance >= 2) return;

            var dialogText = BarrackNpcDialog.DialogList[Level - 1].npcDialogTexts[DialogCounter++];
            NpcDialog.OnNext(dialogText);

            if (DialogCounter > 4) DialogCounter = 0;
        }
    }
}