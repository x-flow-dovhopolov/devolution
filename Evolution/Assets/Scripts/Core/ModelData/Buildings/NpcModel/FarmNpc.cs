﻿using Core.ModelData.StockItems;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.NpcModel
{
    public class FarmNpc : NPC
    {
        private float SpawnRareRate = 30f;
        
        public FarmNpc(Building building, int level) : base(building, level)
        {
            СanSpawnRareItem = Time.time + 1f;
        }

        protected override void InitByType()
        {
            var npc = GameSetting.NPCs.Find("Farm");

            var npcNameKey = "Farm_level_" + Level;
            Name = LocaleRU.Units.Find(npcNameKey).Name;
            
            Type = npc.Type;
            Icon = npc.Icon;
            SpawnRate = npc.SpawnTime;
            SpawnRateByClick = npc.SpawnRateByClick;
            GoldSpawnCount = int.Parse(npc.GoldSpawnCount);
        }

        public override void CurrencyCombine()
        {
            base.CurrencyCombine();
            GameManager.Instance.Model.Food.Value += CurrencyAmount;
        }

        public override void ShowNpcDialog()
        {
            return;
        }

        protected override void GoldCombineByTime()
        {
            SpawnRareItem();
            if (!(Time.time > СanSpawn)) return;

            CurrencyCombine();
            СanSpawn = Time.time + SpawnRate;
        }

        private void SpawnRareItem()
        {
            if (!(Time.time > СanSpawnRareItem)) return;

            var stockItem = RareItemFactory();
            RareItemsList.Add(stockItem);
            RareItemSpawned.OnNext(stockItem);

            СanSpawnRareItem = Time.time + SpawnRareRate;
        }

        private void OnOwnRareItemCollected(StockItem stockItem)
        {
            RareItemsList.Remove(stockItem);
        }

        public StockItem RareItemFactory()
        {
            StockItem rareItem = null;

            switch (Level)
            {
                case 1:
                    rareItem = new Wheel();
                    break;
                case 2:
                    rareItem = new Corn();
                    break;
                case 3:
                    rareItem = new Sunflower();
                    break;
                case 4:
                    rareItem = new Cucumber();
                    break;
                case 5:
                    rareItem = new Strawberry();
                    break;
                case 6:
                    rareItem = new Watermelon();
                    break;
                default:
                    Debug.LogError("It might not happen");
                    break;
            }
            
            rareItem?.RareItemCollected.Subscribe(OnOwnRareItemCollected);
            
            return rareItem;
        }
    }
}