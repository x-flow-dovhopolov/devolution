﻿using Core.ModelData.Buildings.NpcModel.NpcDialogs;
using Core.Tutorial;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.NpcModel
{
    public class MillNpc : NPC
    {
        public MillNpc(Building building, int level) : base(building, level)
        {
        }

        protected override void InitByType()
        {
            var npc = GameSetting.NPCs.Find("Mill");

            var npcNameKey = "Mill_level_" + Level;
            Name = LocaleRU.Units.Find(npcNameKey).Name;
            
            Type = npc.Type;
            Icon = npc.Icon;
            SpawnRate = npc.SpawnTime;
            SpawnRateByClick = npc.SpawnRateByClick;
            GoldSpawnCount = int.Parse(npc.GoldSpawnCount);
        }

        public override void CurrencyCombine()
        {
            base.CurrencyCombine();
            GameManager.Instance.Model.Gold.Value += CurrencyAmount;
        }

        public override void ShowNpcDialog()
        {
            var chance = Random.Range(0, 50);
            if (chance >= 2) return;

            var dialogText = MillNpcDialog.DialogList[Level - 1].npcDialogTexts[DialogCounter++];
            NpcDialog.OnNext(dialogText);

            if (DialogCounter > 4) DialogCounter = 0;
        }
    }
}