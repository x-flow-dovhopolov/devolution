﻿using System.Collections.Generic;
using Core.ModelData.StockItems;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.NpcModel
{
    public abstract class NPC : ITick
    {
        protected float SpawnRate;
        protected float СanSpawnRareItem;
        
        public float СanSpawnRareItemProperty
        {
            get => СanSpawnRareItem;
            set => СanSpawnRareItem = value;
        }
        
        public float SpawnRateProperty
        {
            get => SpawnRate;
            set => SpawnRate = value;
        }

        protected string Icon = "";
        protected int DialogCounter = 0;
        protected int GoldSpawnCount;

        public int GoldSpawnCountProperty
        {
            get => GoldSpawnCount;
            set => GoldSpawnCount = value;
        }

        protected int CurrencyAmount;
        protected float СanSpawn = -1f;

        public float СanSpawnPreperty
        {
            get => СanSpawn;
            set => СanSpawn = value;
        }

        public int Level;
        public string Name;
        public string Type = "";
        public float SpawnRateByClick;
        public Building Building;
        public List<StockItem> RareItemsList = new List<StockItem>();
        
        public Subject<int> CoinSpawnedWithValue { get; } = new Subject<int>();
        public Subject<string> NpcDialog { get; } = new Subject<string>();
        public Subject<StockItem> RareItemSpawned { get; } = new Subject<StockItem>();

        protected NPC(Building building, int level)
        {
            Building = building;
            Level = level;
            InitByType(); // https://www.jetbrains.com/help/rider/VirtualMemberCallInConstructor.html
            
            СanSpawn = Time.time + SpawnRate;
        }
        
        public int OfflineCurrencyCombine(int offlineTime)
        {
            var offlineAmount = (offlineTime / (int) SpawnRate) * CurrencySpawned();
            return offlineAmount;
        }
        
        protected virtual void GoldCombineByTime()
        {
            if (!(Time.time > СanSpawn)) return;

            CurrencyCombine();
            СanSpawn = Time.time + SpawnRate;
        }

        public void Tick() => GoldCombineByTime();
        
        public void CollectRareItemsOnMergeParent()
        {
            foreach (var rareItem in RareItemsList)
            {
                rareItem.ParentMerged.OnNext(Unit.Default);
            }
        }

        private int CurrencySpawned()
        {
            var currencySpawned = 0;
            var booster = Building.Manager.ManagerBooster;
            currencySpawned = (int) Mathf.Pow(GoldSpawnCount, Level) * booster;
            
            return currencySpawned;
        }
        
        public virtual void CurrencyCombine()
        {
            CurrencyAmount = CurrencySpawned();
            
            CoinSpawnedWithValue.OnNext(CurrencyAmount);
        }
        
        public abstract void ShowNpcDialog();
        protected abstract void InitByType();
    }
}