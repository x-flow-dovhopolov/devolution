﻿using System.Collections.Generic;

namespace Core.ModelData.Buildings.NpcModel.NpcDialogs
{
    public static class BarrackNpcDialog
    {
        public static List<SingleDialog> DialogList = new List<SingleDialog>();

        static BarrackNpcDialog()
        {
            DialogList.Add(new SingleDialog(1,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Recruit.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Recruit.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Recruit.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Recruit.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Recruit.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(2,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Caretaker.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Caretaker.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Caretaker.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Caretaker.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Caretaker.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(3,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Novice.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Novice.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Novice.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Novice.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Novice.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(4,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("GraduateStudent.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("GraduateStudent.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("GraduateStudent.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("GraduateStudent.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("GraduateStudent.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(5,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Illusionist.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Illusionist.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Illusionist.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Illusionist.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Illusionist.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(6,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Mechanic.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Mechanic.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Mechanic.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Mechanic.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Mechanic.dialog5").Dialog
                }));
        }
    }
}