﻿using System.Collections.Generic;

namespace Core.ModelData.Buildings.NpcModel.NpcDialogs
{
    public static class MillNpcDialog
    {
        public static List<SingleDialog> DialogList = new List<SingleDialog>();

        static MillNpcDialog()
        {
            DialogList.Add(new SingleDialog(1,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Peasant.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Peasant.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Peasant.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Peasant.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Peasant.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(2,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Lumberjack.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Lumberjack.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Lumberjack.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Lumberjack.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Lumberjack.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(3,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("CityDweller.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("CityDweller.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("CityDweller.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("CityDweller.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("CityDweller.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(4,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Innkeeper.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Innkeeper.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Innkeeper.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Innkeeper.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Innkeeper.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(5,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Merchant.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Merchant.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Merchant.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Merchant.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Merchant.dialog5").Dialog
                }));

            DialogList.Add(new SingleDialog(6,
                new List<string>()
                {
                    LocaleRU.NpcDialogs.Find("Banker.dialog1").Dialog,
                    LocaleRU.NpcDialogs.Find("Banker.dialog2").Dialog,
                    LocaleRU.NpcDialogs.Find("Banker.dialog3").Dialog,
                    LocaleRU.NpcDialogs.Find("Banker.dialog4").Dialog,
                    LocaleRU.NpcDialogs.Find("Banker.dialog5").Dialog
                }));
        }
    }
}