﻿using System.Collections.Generic;

namespace Core.ModelData.Buildings.NpcModel.NpcDialogs
{
    public class SingleDialog
    {
        private int level;
        public List<string> npcDialogTexts;

        public SingleDialog(int level, List<string> npcDialogTexts)
        {
            this.level = level;
            this.npcDialogTexts = npcDialogTexts;
        }
    }
}