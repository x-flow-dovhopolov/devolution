﻿using UniRx;

namespace Core.ModelData.Buildings.SupportBuildings
{
    public class Bank
    {
        public bool IsActive;
        
        public Subject<Unit> FarmBuilt { get; } = new Subject<Unit>();
        
        public void SetBankActive()
        {
            IsActive = true;
            FarmBuilt.OnNext(Unit.Default);
        }
    }
}