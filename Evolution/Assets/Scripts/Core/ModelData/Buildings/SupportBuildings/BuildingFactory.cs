﻿namespace Core.ModelData.Buildings.SupportBuildings
{
    public static class BuildingFactory
    {
        private static Building Building;
        
        public static Building CreateBuilding(BuildingType buildingType)
        {
            if (buildingType == BuildingType.Mill)
            {
                Building = new Mill(){Id = ++Model.BuildingCounter};
            }
            else if (buildingType == BuildingType.Farm)
            {
                Building = new Farm(){Id = ++Model.BuildingCounter};
            }
            else if(buildingType == BuildingType.Barracks)
            {
                Building = new Barracks(){Id = ++Model.BuildingCounter};
            }
            else if(buildingType == BuildingType.Tavern)
            {
                Building = new Tavern(){Id = ++Model.BuildingCounter};
            }
            else
            {
            }

            return Building;
        }
    }
}