﻿using System;
using Core.ModelData.ExceptionHierarchy;
using Core.SaveLoad;
using Core.Tutorial;
using Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Buildings.SupportBuildings
{
    [Serializable]
    public class Castle
    {
        private CastleData CastleData;

        public CastleData CastleDataProperty
        {
            get => CastleData;
            set
            {
                if (value.Level != 0)
                {
                    CastleData = value;
                }
                else
                {
                    InitCastleData();
                }
            }
        }

        public int Level
        {
            get => CastleData.Level;
            set => CastleData.Level = value;
        }

        public string Name
        {
            get => CastleData.Name;
            set => CastleData.Name = value;
        }

        public string Description
        {
            get => CastleData.Description;
            set => CastleData.Description = value;
        }

        public string NextLevelDescription
        {
            get => CastleData.NextLevelDescription;
            set => CastleData.NextLevelDescription = value;
        }

        public int GoldAmount
        {
            get => CastleData.GoldAmount;
            set => CastleData.GoldAmount = value;
        }

        public string Description1
        {
            get => CastleData.Description1;
            set => CastleData.Description1 = value;
        }

        public string Description2
        {
            get => CastleData.Description2;
            set => CastleData.Description2 = value;
        }

        public Subject<int> LevelIncreased { get; } = new Subject<int>();

        public Castle()
        {
            InitCastleData();
        }

        private void InitCastleData()
        {
            CastleData = new CastleData
            {
                Level = 1,
                Name = LocaleRU.Buildings.Find("Castle").Name,
                Description = "Чем выше уровень Вашего замка, тем больше улучшений вы можете делать внутри ваших зданий",//LocaleRU.Buildings.Find("Castle").Description,
                NextLevelDescription = "Чем выше уровень Вашего замка, тем больше улучшений вы можете делать внутри ваших зданий",//LocaleRU.Buildings.Find("Castle").NextLevelDescription,
                GoldAmount = 12000,
                Description1 = "Чем выше уровень Вашего замка, тем больше улучшений вы можете делать внутри ваших зданий",
                Description2 = "Чем выше уровень Вашего замка, тем больше улучшений вы можете делать внутри ваших зданий"
            }; 
        }
        
        public int LevelProperty
        {
            get => Level;
            set
            {
                Level = value;
                LevelIncreased.OnNext(Level);
                
                if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.CastleUpgrade)
                {
                    var castleState = GameManager.Instance.Tutor.Context._state as AdventureStateHierarchyParent;
                    castleState?.OnCastleUpgrade.OnNext(Unit.Default);
                }
            }
        }
    
        public void Upgrade()
        {
            if (GameManager.Instance.Model.Gold.Value >= GoldAmount)
            {
                LevelProperty += 1;
                GameManager.Instance.DeductPurchasePrice(GoldAmount);
            }
            else
            {
                throw new BalanceException();
            }
        }
    }
}