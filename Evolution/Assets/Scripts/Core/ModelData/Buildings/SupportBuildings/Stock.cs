﻿using UniRx;

namespace Core.ModelData.Buildings.SupportBuildings
{
    public class Stock
    {
        public bool IsActive;
        public string Name;
        
        public Subject<Unit> MillBuilt { get; } = new Subject<Unit>();

        public Stock()
        {
            Name = LocaleRU.Buildings.Find("Tavern").Name;
        }

        public void SetStockActive()
        {
            IsActive = true;
            
            MillBuilt.OnNext(Unit.Default);
        }
    }
}