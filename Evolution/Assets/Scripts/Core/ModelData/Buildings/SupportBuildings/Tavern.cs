﻿using UnityEngine;

namespace Core.ModelData.Buildings.SupportBuildings
{
    public class Tavern : Building
    {
        public override string AdventureSceneImage { get; set; }  = "Buildings/Adventure/Tavern";
        public override string CurrencySceneImage { get; set; } = "Buildings/Adventure/Tavern";
        public override string UniqueItem1 { get; set; }
        public override string UniqueItem2 { get; set; }
        public override string RoadImage { get; set; } = default;
        public override string FillBarImage { get; set; } = default;

        public Tavern()
        {
            base.BuildingType = BuildingType.Tavern;
            
            var tavern = GameSetting.Builds.Find("Tavern");

            Name = LocaleRU.Buildings.Find("Tavern").Name;
            Description = LocaleRU.Buildings.Find("Tavern").Description;
            
            BuildingPrice = tavern.BasePrice;
            BuildingFoodPrice = tavern.BasePriceFood;
        }
        
        public override void ActivateSupportBuilding()
        {
            return;
        }

        public override GameObject FarmNpcPrefabFactory(int level)
        {
            return null;
        }
    }
}