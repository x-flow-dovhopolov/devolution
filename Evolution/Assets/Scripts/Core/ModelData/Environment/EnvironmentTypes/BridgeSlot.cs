﻿using UnityEngine;

namespace Core.ModelData.Environment.EnvironmentTypes
{
    public class BridgeSlot : InsideFogSlot
    {
        public override bool IsPassedProperty { 
            get => IsPassed.Value;
            set
            {
                GameManager.Instance.Model.FogSlots.IsBridgeRepaired = true;
                IsPassed.Value = value;
            }}
        
        public BridgeSlot(bool gameEnvironmentIsBridgeRepaired) : base(gameEnvironmentIsBridgeRepaired)
        {
            var bridge = GameSetting.Environments.Find("Bridge");
            GoldDiscoverCount = bridge.BasePrice;
            FoodDiscoverCount = bridge.BasePriceFood;
            PowerDiscoverCount = bridge.BasePricePower;
        }
    }
}