﻿using UnityEngine;

namespace Core.ModelData.Environment.EnvironmentTypes
{
    public class EnemySlot: InsideFogSlot
    {
        public override bool IsPassedProperty { 
            get => IsPassed.Value;
            set
            {
                GameManager.Instance.Model.FogSlots.IsTombDiscovered = true;
                IsPassed.Value = value;
            }}
        
        public EnemySlot(bool gameEnvironmentIsTombDiscovered) : base(gameEnvironmentIsTombDiscovered)
        {
            var enemy = GameSetting.Environments.Find("Enemy");
            GoldDiscoverCount = enemy.BasePrice;
            FoodDiscoverCount = enemy.BasePriceFood;
            PowerDiscoverCount = enemy.BasePricePower;
        }
    }
}