﻿using System;
using System.Collections.Generic;
using Core.ModelData.Buildings.SupportBuildings;
using Core.ModelData.StockItems;
using UnityEngine;

namespace Core.ModelData.Environment.EnvironmentTypes
{
    public class TravelerSlot: InsideFogSlot
    {
        private readonly string PATH = "Core.ModelData.StockItems.";

        private string firstItemClassName;
        private int firstItemNeededCount;
        private string secondItemClassName;
        private int secondItemNeededCount;

        public override bool IsPassedProperty { 
            get => IsPassed.Value;
            set
            {
                GameManager.Instance.Model.FogSlots.IsTravelerSaved = true;
                IsPassed.Value = value;
            }}
        
        public List<RequiredItem> requiredItemslist;
        
        public TravelerSlot(bool gameEnvironmentIsTravelerSaved) : base(gameEnvironmentIsTravelerSaved)
        {
            var traveler = GameSetting.Environments.Find("Traveler");
            GoldDiscoverCount = traveler.BasePrice;
            FoodDiscoverCount = traveler.BasePriceFood;
            PowerDiscoverCount = traveler.BasePricePower;

            firstItemNeededCount = traveler.FirstItemCount;
            firstItemClassName = traveler.FirstItemClassName;
            secondItemNeededCount = traveler.SecondItemCount;
            secondItemClassName = traveler.SecondItemClassName;

            SwitchInit();
        }
        
        private void ByClassNameInit()
        {
            requiredItemslist = new List<RequiredItem>()
            {
                new RequiredItem(firstItemNeededCount, Activator.CreateInstance(Type.GetType(PATH + firstItemClassName)) as StockItem),
                new RequiredItem(secondItemNeededCount, Activator.CreateInstance(Type.GetType(PATH + secondItemClassName)) as StockItem)
            };
        }
        
        private void SwitchInit()
        {
            requiredItemslist = new List<RequiredItem>()
            {
                new RequiredItem(firstItemNeededCount, InitRequiredItemClass(firstItemClassName)),
                new RequiredItem(secondItemNeededCount, InitRequiredItemClass(secondItemClassName))
            };
        }

        private StockItem InitRequiredItemClass(string className)
        {
            StockItem item;
            switch (className)
            {
                case "Wheel" : 
                    item = new Wheel();
                    break;
                case "Corn" : 
                    item = new Corn();
                    break;
                case "Sunflower" : 
                    item = new Sunflower();
                    break;
                case "Cucumber" : 
                    item =  new Cucumber();
                    break;
                case "Strawberry" : 
                    item = new Strawberry();
                    break;
                case "Watermelon" : 
                    item = new Watermelon();
                    break;
                default: 
                    /* it might not happen*/
                    item = new Corn(); 
                    break;
            }

            return item;
        }
        
        public override void PointedPassed()
        {
            base.PointedPassed();
        }
    }

    public class RequiredItem
    {
        public int count;
        public StockItem item;

        public RequiredItem(int count, StockItem item)
        {
            this.count = count;
            this.item = item;
        }
    }
    
    
}