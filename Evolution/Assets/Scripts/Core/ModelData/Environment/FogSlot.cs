﻿using System.Collections.Generic;
using Core.ModelData.Environment.EnvironmentTypes;
using Core.SaveLoad;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Environment
{
    public class FogSlot
    {
        private GameEnvironment GameEnvironment;
        
        public GameEnvironment GameEnvironmentProperty
        {
            get => GameEnvironment;
            set
            {
                GameEnvironment = value;
                InitAddData();
            }
        }
        
        private ReactiveProperty<bool> IsDiscovered;

        public bool IsBridgeRepaired
        {
            private get => GameEnvironment.IsBridgeRepaired;
            set => GameEnvironment.IsBridgeRepaired = value;
        }
        
        public bool IsTravelerSaved
        {
            private get => GameEnvironment.IsTravelerSaved;
            set => GameEnvironment.IsTravelerSaved = value;
        }
        
        public bool IsTombDiscovered
        {
            private get => GameEnvironment.IsTombDiscovered;
            set => GameEnvironment.IsTombDiscovered = value;
        }
        public ReactiveProperty<bool> IsDiscoveredProperty
        {
            get => IsDiscovered;
            set
            {
                if (IsDiscovered == null)
                {
                    IsDiscovered = new ReactiveProperty<bool>(value.Value);
                }
                else
                {
                    IsDiscovered.Value = value.Value;
                }
                
                GameEnvironment.IsFogPassed = value.Value;
            }
        }

        private int index;
        public List<InsideFogSlot> fogSlot;
        public int FogGoldDiscoverCount;
        public int FogFoodDiscoverCount;
        public int FogPowerDiscoverCount;
        
        public FogSlot(int index)
        {
            this.index = index;

            var fog = GameSetting.Environments.Find("Fog");
            FogGoldDiscoverCount = fog.BasePrice;
            FogFoodDiscoverCount = fog.BasePriceFood;
            FogPowerDiscoverCount = fog.BasePricePower;
            
            IsDiscoveredProperty = new ReactiveProperty<bool>(false);
            fogSlot = new List<InsideFogSlot>
            { 
                new BridgeSlot(false),  
                new TravelerSlot(false), 
                new EnemySlot(false)
            };
        }

        private void InitAddData()
        {
            IsDiscoveredProperty.Value = GameEnvironment.IsFogPassed;

            fogSlot[0].IsPassed.Value = GameEnvironment.IsBridgeRepaired;
            fogSlot[1].IsPassed.Value = GameEnvironment.IsTravelerSaved;
            fogSlot[2].IsPassed.Value = GameEnvironment.IsTombDiscovered;
        }

        public void SetFogIsDiscovered()
        {
            IsDiscoveredProperty = new ReactiveProperty<bool>(true);
            GameManager.Instance.Tutor.Context._state.OnFogDiscovered.OnNext(Unit.Default);
            //GameManager.Instance.Tutor.Context._state.TutorialActionSecondStepFinished.OnNext(Unit.Default);
        }
        
        public bool CheckEnoughCurrencyToDiscover()
        {
            return GameManager.Instance.Model.Gold.Value >= FogGoldDiscoverCount &&
                   GameManager.Instance.Model.Food.Value >= FogFoodDiscoverCount &&
                   GameManager.Instance.Model.Power.Value >= FogPowerDiscoverCount;
        }

        public GameEnvironment SaveEnvironment()
        {
            return GameEnvironment;
        }
    }
}