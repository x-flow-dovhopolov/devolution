﻿using UniRx;
using UnityEngine;

namespace Core.ModelData.Environment
{
    public class InsideFogSlot
    {
        public int Index;
        public int GoldDiscoverCount;
        public int FoodDiscoverCount;
        public int PowerDiscoverCount;
        public ReactiveProperty<bool> IsPassed;
        
        public virtual bool IsPassedProperty
        {
            get => IsPassed.Value;
            set
            {
                IsPassed.Value = value;
                
                Debug.Log($" SUPER value = {value} ");
            }
        }

        protected InsideFogSlot(bool gameEnvironmentIsBridgeRepaired)
        {
            IsPassed = new ReactiveProperty<bool>(gameEnvironmentIsBridgeRepaired);
        }

        public virtual void PointedPassed()
        {
            IsPassedProperty = true;
            GameManager.Instance.Tutor.Context._state.OnEnvironmentPassed.OnNext(Unit.Default);
        }
        
        public bool CheckEnoughCurrencyToDiscoverInsideFogSlot()
        {
            return GameManager.Instance.Model.Gold.Value >= GoldDiscoverCount &&
                   GameManager.Instance.Model.Food.Value >= FoodDiscoverCount&&
                   GameManager.Instance.Model.Power.Value >= PowerDiscoverCount;
        }
    }
}