﻿using System;

namespace Core.ModelData.ExceptionHierarchy
{
    public abstract class AdventureException : Exception
    {
        public string Description = "";
    }
    
    public class BalanceException : AdventureException
    {
        public BalanceException()
        {
            Description = LocaleRU.Generals.Find("alert_no_resource").Value;
        }
    }

    public class CastleException : AdventureException
    {
        public CastleException()
        {
            Description = LocaleRU.Generals.Find("alert_no_castlelevel").Value;
        }
    }

    public class MaxNpcLevelException : AdventureException
    {
        public MaxNpcLevelException()
        {
            Description = LocaleRU.Generals.Find("alert_no_resource").Value;
        }
    }

    public class SpawnException : AdventureException
    {
        public SpawnException()
        {
            Description = LocaleRU.Generals.Find("Min_spawn_time").Value;
        }
    }

    public class MaxNpcInBuildingException : AdventureException
    {
        public MaxNpcInBuildingException(int npcCount)
        {
            Description = LocaleRU.Generals.Find("alert_no_space").Value.Replace("{value}", npcCount + "");
        }
    }
    
    public class NotEnoughFruitException : AdventureException
    {
        public NotEnoughFruitException()
        {
            Description = LocaleRU.Generals.Find("Not_enough_fruits").Value;
        }
    }
}