﻿namespace Core.ModelData
{
    public interface ITick
    {
        void Tick();
    }
}