﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.ModelData.Buildings.NpcModel;
using Core.SaveLoad;
using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;

namespace Core.ModelData.NpcDiscover
{
    public class NpcDiscoverManager
    {
        private List<NpcLibrary> NpcLibraries;
        public ReactiveDictionary<int, SingleNpcDiscover<NPC>> MillCreatures; 
        public ReactiveDictionary<int, SingleNpcDiscover<NPC>> FarmCreatures;
        public ReactiveDictionary<int, SingleNpcDiscover<NPC>> BarrackCreatures;
        
        public readonly Dictionary<int, string> mill = new Dictionary<int, string>{
            {1, "Крестьянин"}, {2, "Лесоруб"}, {3, "Горожанин"}, {4, "Трактирщик"}, {5, "Торговец"}, {6, "Банкир"}
        };
        public readonly Dictionary<int, string> farm = new Dictionary<int, string>{
            {1, "Пшеница"}, {2, "Кукуруза"}, {3, "Подсолнух"}, {4, "Огурец"}, {5, "Клубника"}, {6, "Арбуз"}
        };
        public readonly Dictionary<int, string> barrack= new Dictionary<int, string>{
            {1, "Новобранец"}, {2, "Знаменосец"}, {3, "Послушник"}, {4, "Аспирант"}, {5, "Ученик мага"}, {6, "Механик"}
        };
        
        public readonly Dictionary<int, int> millNpcPrice = new Dictionary<int, int>{
            {1, 560}, {2, 1200}, {3, 17000}, {4, 54000}, {5, 0}, {6, 0}
        };
        public readonly Dictionary<int, int> farmNpcPrice = new Dictionary<int, int>{
            {1, 1120}, {2, 2400}, {3, 34000}, {4, 108000}, {5, 0}, {6, 0}
        };
        public readonly Dictionary<int, int> barrackNpcPrice= new Dictionary<int, int>{
            {1, 2240}, {2, 4800}, {3, 68000}, {4, 216000}, {5, 0}, {6, 0}
        };
        
        public Subject<(int, Sprite, string)> NewNpcJustDiscovered { get; } = new Subject<(int, Sprite, string)>();
        public Subject<int> TotalItemCount { get; } = new Subject<int>();

        public NpcDiscoverManager()
        {
            NpcLibraries = new List<NpcLibrary>();
            Action<int, SingleNpcDiscover<NPC>> op = SetNewItemInShopLambda;

            MillCreatures = new ReactiveDictionary<int, SingleNpcDiscover<NPC>>();
            FarmCreatures = new ReactiveDictionary<int, SingleNpcDiscover<NPC>>();
            BarrackCreatures = new ReactiveDictionary<int, SingleNpcDiscover<NPC>>();
            
            MillCreatures.ObserveAdd()
                .Where(x => MillCreatures.Count > 2)
                .Subscribe(e =>
                {
                    op(e.Key, MillCreatures[MillCreatures.Count - 2]);
                });

            FarmCreatures.ObserveAdd()
                .Where(x => FarmCreatures.Count > 2)
                .Subscribe(e =>
                {
                    var key = e.Key;
                    var npc = FarmCreatures[FarmCreatures.Count - 2];
                    op(key, npc);
                });

            BarrackCreatures.ObserveAdd()
                .Where(x => BarrackCreatures.Count > 2)
                .Subscribe(e =>
                {
                    op(e.Key, BarrackCreatures[BarrackCreatures.Count - 2]);
                });
        }
        
        void SetNewItemInShopLambda(int level, SingleNpcDiscover<NPC> npc)
        {
            GameManager.Instance.Model.ShopManager.SetItem(npc);
        }

        public void FindByTypeAndAddIfNotExists(NPC npc)
        {
            var containsKey = false;
            //TODO этот свитч еще в двух местах
            switch (npc.Type)
            {
                case "Mill":
                    containsKey = MillCreatures.ContainsKey(npc.Level);
                    break;
                case "Farm":
                    containsKey = FarmCreatures.ContainsKey(npc.Level);
                    break;
                case "Barrack":
                    containsKey = BarrackCreatures.ContainsKey(npc.Level);
                    break;
                default:
                    break;
            }

            if (!containsKey) AddToCollection(npc);
        }

        private void AddToCollection(NPC npc)
        {
            var sp = GameManager.Instance.ImageDb.Find(npc.Type, npc.Level);
            
            //TODO этот свитч еще в двух местах
            string name = "";
            var price = 0;
            var listItemCount = 0;
            switch (npc.Type)
            {
                case "Mill":
                    price = millNpcPrice[npc.Level];
                    name = mill[npc.Level];
                    MillCreatures.Add(npc.Level, new SingleNpcDiscover<NPC>(npc, null, name, price));
                    listItemCount = MillCreatures.Count;
                    break;
                case "Farm":
                    price = farmNpcPrice[npc.Level];
                    name = farm[npc.Level];
                    FarmCreatures.Add(npc.Level, new SingleNpcDiscover<NPC>(npc, null, name, price));
                    listItemCount = FarmCreatures.Count;
                    break;
                case "Barrack":
                    price = barrackNpcPrice[npc.Level];
                    name = barrack[npc.Level];
                    BarrackCreatures.Add(npc.Level, new SingleNpcDiscover<NPC>(npc, null, name, price));
                    listItemCount = BarrackCreatures.Count;
                    break;
                default:
                    break;
            }

            NpcLibraries.Add(CreateSingleLibraryItem(npc.Level, name, npc.Type));
            
            NewNpcJustDiscovered.OnNext((npc.Level, sp, npc.Name));
            TotalItemCount.OnNext(listItemCount);
        }

        public void SetNpcLibrary(List<NpcLibrary> npcLibraries)
        {
            NpcLibraries = npcLibraries;
            
            foreach (var npc in npcLibraries)
            {
                switch (npc.Type)
                {
                    case "Mill":
                        MillCreatures.Add(npc.Level, 
                            new SingleNpcDiscover<NPC>(npc.Level, npc.Type, npc.Name, millNpcPrice[npc.Level]));
                        break;
                    case "Farm":
                        FarmCreatures.Add(npc.Level, 
                            new SingleNpcDiscover<NPC>(npc.Level, npc.Type, npc.Name, farmNpcPrice[npc.Level]));
                        break;
                    case "Barrack":
                        BarrackCreatures.Add(npc.Level, 
                            new SingleNpcDiscover<NPC>(npc.Level, npc.Type, npc.Name, barrackNpcPrice[npc.Level]));
                        break;
                    default:
                        break;
                }
            }
        }

        private NpcLibrary CreateSingleLibraryItem(int level, string name, string type)
        {
            return new NpcLibrary()
            {
                Level = level,
                Gold = 1,
                Food = 0,
                Name = name,
                Type = type
            };
        }
        public List<NpcLibrary> SaveNpcLibrary()
        {
            return NpcLibraries;
        }
    }
}