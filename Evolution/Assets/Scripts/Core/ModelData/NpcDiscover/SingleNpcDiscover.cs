﻿using Core.ModelData.Buildings.NpcModel;
using UniRx;
using UnityEngine;

namespace Core.ModelData.NpcDiscover
{
    public class SingleNpcDiscover<T>
    {
        public int Level;
        public int Gold;
        public int Food;
        public string Name;
        public string Type;
        public Sprite Sprite;

        public ReactiveProperty<int> BoughtCount = new ReactiveProperty<int>(0);
        public SingleNpcDiscover(NPC npc, Sprite sprite, string name, int price)
        {
            Type = npc.Type;
            Level = npc.Level;
            Sprite = GameManager.Instance.ImageDb.Find(npc.Type, npc.Level);
            Gold = price;
            Food = 0;
            Name = name;
        }
        
        public SingleNpcDiscover(int npcLevel, string Type, string name, int price)
        {
            this.Type = Type;
            Level = npcLevel;
            Sprite = GameManager.Instance.ImageDb.Find(Type, Level);
            Gold = price;
            Food = 0;
            Name = name;
        }
    }
}