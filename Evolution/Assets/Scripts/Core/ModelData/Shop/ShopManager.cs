﻿using System;
using Core.ModelData.Buildings.NpcModel;
using Core.ModelData.ExceptionHierarchy;
using Core.ModelData.NpcDiscover;
using Core.Tutorial;
using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;

namespace Core.ModelData.Shop
{
    public class ShopManager
    {
        private ReactiveDictionary<int, SingleNpcDiscover<NPC>> CurrentBuildingShopCollection;
        public Subject<SingleNpcDiscover<NPC>> SingleItemDataEvent { get; } = new Subject<SingleNpcDiscover<NPC>>();
        
        public void SetAvailableShopItems()
        {
            var buildingType = GameManager.Instance.Model.CurrentBuilding.BuildingType;

            switch (buildingType)
            {
                case BuildingType.Mill:
                    CurrentBuildingShopCollection = GameManager.Instance.Model.NpcDiscoverManager.MillCreatures;
                    break;
                case BuildingType.Farm:
                    CurrentBuildingShopCollection = GameManager.Instance.Model.NpcDiscoverManager.FarmCreatures;
                    break;
                case BuildingType.Barracks:
                    CurrentBuildingShopCollection = GameManager.Instance.Model.NpcDiscoverManager.BarrackCreatures;
                    break;
            }
            
            for (var i = 0; i < CurrentBuildingShopCollection.Count - 2; i++)
            {
                SetItem(CurrentBuildingShopCollection[i + 1]);
            }
        }

        public void SetItem(SingleNpcDiscover<NPC> npc) => SingleItemDataEvent.OnNext(npc);

        public void Buy(int gold, int food, int level)
        {
            try
            {
                GameManager.Instance.CheckForEnoughMoney(gold, food);// can throw @Not enough money
            }
            catch (AdventureException e)
            {
                InsideBuildingUiHelper.Instance.ShowErrorPrefab(e.Description);
                return;
            }

            try
            {
                GameManager.Instance.Model.CurrentBuilding.BuyNpc(level, gold, food);
            }
            catch (AdventureException e)
            {
                InsideBuildingUiHelper.Instance.ShowErrorPrefab(e.Description);
                return;
            } 
            
            CurrentBuildingShopCollection[level].BoughtCount.Value++;
            
            if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.UseShop)
            {
                SendTutorialEventOnNpcBought();
            }
        }

        private void SendTutorialEventOnNpcBought()
        {
            Debug.Log($"GameManager.Instance.Model.CurrentBuilding.BuildingType = {GameManager.Instance.Model.CurrentBuilding.BuildingType}");
            if (GameManager.Instance.Model.CurrentBuilding.BuildingType != BuildingType.Mill) return;
            
            //Debug.Log($"after if {GameManager.Instance.Tutor.Context._state.TutorialActionFinished.HasObservers}");
            GameManager.Instance.Tutor.Context._state.OnNpcBoughtInShop.OnNext(Unit.Default);
        }
    }
}