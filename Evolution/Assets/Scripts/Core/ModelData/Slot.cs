﻿using Core.ModelData.Buildings;
using UniRx;
using IOPath = System.IO.Path;

namespace Core.ModelData
{
    public class Slot 
    {
        private int index;
        public ReactiveProperty<Building> Building;
        
        public Slot(int index)
        {
            this.index = index;
            Building = new ReactiveProperty<Building>(new DefaultBuilding() {Id = 0});
        }
    }
}
