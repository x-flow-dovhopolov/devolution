﻿namespace Core.ModelData.StockItems
{
    public class Corn : StockItem
    {
        public const string Img = "Fruits/Corn";
        public override string Image { get; set; } = "Fruits/Corn";
    }
}