﻿namespace Core.ModelData.StockItems
{
    public class Cucumber : StockItem
    {
        public const string Img = "Fruits/Cucumber";
        public override string Image { get; set; } = "Fruits/Cucumber";
    }
}