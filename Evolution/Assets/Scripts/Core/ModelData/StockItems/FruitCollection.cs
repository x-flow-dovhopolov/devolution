﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.ModelData.Environment.EnvironmentTypes;
using Core.SaveLoad;
using UniRx;
using UniRx.Async;
using UnityEngine;

namespace Core.ModelData.StockItems
{
    [Serializable]
    public class FruitCollection
    {
        public Dictionary<string, Fruit> allCollectionDictionary;
        private Subject<Unit> ClickStream = new Subject<Unit>();
        
        private Fruit WheelFruit;
        private Fruit CornFruit;
        private Fruit SunflowerFruit;
        private Fruit CucumberFruit;
        private Fruit StrawberryFruit;
        private Fruit WatermelonFruit;

        public FruitCollection()
        {
            allCollectionDictionary = new Dictionary<string, Fruit>();

            WheelFruit = new Fruit("Wheel", Wheel.Img);
            allCollectionDictionary.Add("Wheel", WheelFruit);

            CornFruit = new Fruit("Corn", Corn.Img);
            allCollectionDictionary.Add("Corn", CornFruit);

            SunflowerFruit = new Fruit("Sunflower", Sunflower.Img);
            allCollectionDictionary.Add("Sunflower", SunflowerFruit);

            CucumberFruit = new Fruit("Cucumber", Cucumber.Img);
            allCollectionDictionary.Add("Cucumber", CucumberFruit);

            StrawberryFruit = new Fruit("Strawberry", Strawberry.Img);
            allCollectionDictionary.Add("Strawberry", StrawberryFruit);

            WatermelonFruit = new Fruit("Watermelon", Watermelon.Img);
            allCollectionDictionary.Add("Watermelon", WatermelonFruit);
        }

        public void SetItem(StockItem stockItem)
        {
            Debug.Log($"stockItem = {stockItem.Image}");
            
            switch (stockItem.Image)
            {
                case "Fruits/Wheel":
                    WheelFruit.Count++;
                    break;
                case "Fruits/Corn":
                    CornFruit.Count++;
                    break;
                case "Fruits/Sunflower":
                    SunflowerFruit.Count++;
                    break;
                case "Fruits/Cucumber":
                    CucumberFruit.Count++;
                    break;
                case "Fruits/Strawberry":
                    StrawberryFruit.Count++;
                    break;
                case "Fruits/Watermelon":
                    WatermelonFruit.Count++;
                    break;
                default:
                    Debug.LogError("It might not happen");
                    break;
            }
        }

        public bool CheckIsEnoughFruits(List<RequiredItem> items)
        {
            var v1 = CheckIsEnoughFruit(items[0]);
            var v2 = CheckIsEnoughFruit(items[1]);
            var isEnoughFruits = v1 && v2;
//Debug.Log($"CheckIsEnoughFruit(items[0]) = {CheckIsEnoughFruit(items[0])}");
//Debug.Log($"CheckIsEnoughFruit(items[1]) = {CheckIsEnoughFruit(items[1])}");
            if (!isEnoughFruits) return false;
            
            DeductFruit(items[0]);
            DeductFruit(items[1]);
            return true;
        }

        public async UniTask<bool> BuyUsingRareFruitsList(List<RequiredItem> items)
        {
            var isEnough = false;
            
            var observablesListOfCheckMethods = new List<IObservable<bool>>();
            
            foreach (var variable in items)
            {
                observablesListOfCheckMethods.Add(IsEnoughIObservable(variable));
            }
            
            observablesListOfCheckMethods.WhenAll()
                .ObserveOnMainThread()
                .Subscribe(xs =>
                {
                    var result = xs.All(x => true);
                    Debug.Log($"{xs.Length} = result => {result}");
                    
                    if (result)
                    {
                        foreach (var item in items)
                        {
                            DeductFruit(item);
                        }
                        
                        isEnough  = true;
                    }
                    ClickStream.OnNext(Unit.Default);
                });
            
            await ClickStream.First();
            return isEnough;
        }

        private IObservable<bool> IsEnoughIObservable(RequiredItem item) => Observable.Start(() => CheckIsEnoughFruit(item));

        private bool CheckIsEnoughFruit(RequiredItem item)
        {
            var itemClassName = item.item.GetType().Name;
            Debug.Log($"itemClassName = {itemClassName}");
            foreach (var VARIABLE in allCollectionDictionary)
            {
                Debug.Log($"{VARIABLE.Key} = {VARIABLE.Value.Count}");
            }
            var itemCollectionExists = allCollectionDictionary.ContainsKey(itemClassName);

            Debug.Log($"itemCollectionExists = {itemCollectionExists}");
            if (!itemCollectionExists) return false;
            var isEnough = allCollectionDictionary[itemClassName].Count >= item.count;

            return isEnough;
        }

        private void DeductFruit(RequiredItem item)
        {
            var singleItem = item;
            allCollectionDictionary[singleItem.item.GetType().Name].Count -= singleItem.count;
        }

        public void SetFruitCollectionFromSave(List<FruitsCollectionData> playerDataFruitCollection)
        {
            foreach (var savedFruit in playerDataFruitCollection.
                Where(savedFruit => allCollectionDictionary.ContainsKey(savedFruit.Key)))
            {
                allCollectionDictionary[savedFruit.Key].Count = savedFruit.Count;
            }
        }

        public List<FruitsCollectionData> SaveFruitCollection()
        {
            return (from fruit in allCollectionDictionary.Values 
                select new FruitsCollectionData()
                {
                    Key = fruit.Name,
                    Count = fruit.Count
                }).ToList();
        }
    }
    
    public class Fruit
    {
        public int Count;
        public string Image;
        public string Name;

        public Fruit(string name, string image)
        {
            Name = name;
            Image = image;
        }
    }
}