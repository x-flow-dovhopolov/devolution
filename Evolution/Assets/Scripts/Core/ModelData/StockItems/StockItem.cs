﻿using UniRx;
using UnityEngine;

namespace Core.ModelData.StockItems
{
    public abstract class StockItem
    {
        public int index;
        public abstract string Image { get; set; }
        
        public Subject<StockItem> RareItemCollected{ get; } = new Subject<StockItem>();
        public Subject<Unit> ParentMerged{ get; } = new Subject<Unit>();
        
        public void OnCollectRareItem()
        {
            RareItemCollected.OnNext(this);
            GameManager.Instance.Model.OnCollectRareItem(this);
        }
    }
}