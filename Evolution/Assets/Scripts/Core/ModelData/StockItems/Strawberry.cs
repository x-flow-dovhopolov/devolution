﻿namespace Core.ModelData.StockItems
{
    public class Strawberry : StockItem
    {
        public const string Img = "Fruits/Strawberry";
        public override string Image { get; set; } = "Fruits/Strawberry";
    }
}