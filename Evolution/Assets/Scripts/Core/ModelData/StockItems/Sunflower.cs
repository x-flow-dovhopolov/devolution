﻿namespace Core.ModelData.StockItems
{
    public class Sunflower : StockItem
    { 
        public const string Img = "Fruits/Sunflower";
        public override string Image { get; set; } = "Fruits/Sunflower";
    }
}