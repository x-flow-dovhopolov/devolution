﻿namespace Core.ModelData.StockItems
{
    public class Watermelon: StockItem
    {
        public const string Img = "Fruits/Watermelon";
        public override string Image { get; set; } = "Fruits/Watermelon";
    }
}