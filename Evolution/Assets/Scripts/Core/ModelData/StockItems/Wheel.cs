﻿namespace Core.ModelData.StockItems
{
    public class Wheel: StockItem
    {
        public const string Img = "Fruits/Wheel";
        public override string Image { get; set; } = "Fruits/Wheel";
    }
}