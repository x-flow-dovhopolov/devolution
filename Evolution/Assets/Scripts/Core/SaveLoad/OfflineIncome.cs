﻿using System;
using UnityEngine;

namespace Core.SaveLoad
{
    public class OfflineIncome
    {
        public static int goldOfflineAmount = 0;
        public static int foodOfflineAmount = 0;
        public static int powerOfflineAmount = 0;
        
        public void GetTime(long saveManagerTimeOnApplicationExitProperty)
        {
            if(saveManagerTimeOnApplicationExitProperty == 0) return;
            
            var timeUserWasOffline = (int) TimeUserWasOffline(saveManagerTimeOnApplicationExitProperty);
            CheckOfflineBonus(timeUserWasOffline);
        }
        
        private void CheckOfflineBonus(int offlineTime)
        {
            if (offlineTime > 10)
            {
                PassiveTick(offlineTime, out var gold, out var food, out var power);
            }
        }

        private void PassiveTick(int offlineTime, out int gold, out int food, out int power)
        { 
           gold = 0;
           food = 0;
           power = 0;
           
           if (GameManager.Instance.Model == null) return;
            
           foreach (var slot in GameManager.Instance.Model.SlotsList)
           {
               //Debug.Log($"loop type = {slot.Building.Value.BuildingType}");
               
               slot.Building.Value.StaticPassiveTick(offlineTime);
           }
        }
        
        private static double DateTimeToUnixTimestamp(DateTime dateTime) => 
            (dateTime - new DateTime(1970, 1, 1)).TotalSeconds;

        private static double TimeUserWasOffline(long dateTime) => DateTimeToUnixTimestamp(DateTime.UtcNow) - dateTime;
    }
}