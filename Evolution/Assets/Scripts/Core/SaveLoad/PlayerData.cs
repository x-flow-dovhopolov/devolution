﻿using System;
using System.Collections.Generic;
using Core.ModelData.Buildings.Managers;
using Core.Tutorial;

namespace Core.SaveLoad
{
    [Serializable]
    public class PlayerData
    {
        public TutorialManagerData TutorialManagerData;
        
        public string ProjectVersion;
        public long TimeOnApplicationExit;
        public int Gold;
        public int Food;
        public int Power;
        public bool IsStockActive;
        public bool IsBankActive;
        public CastleData Castle;
        
        public GameEnvironment GameEnvironment;
        public List<BuildingModel> SlotsList;
        public List<NpcLibrary> NpcLibraries;
        public List<FruitsCollectionData> FruitCollection;
    }
    
    [Serializable]
    public struct BuildingModel
    {
        public int Index;
        public int BuildingLevel;
        public int UnitsOnScene;
        public int CurrentMaxUnitsOnScene;
        public float UnitSpawnTime;
        public int NpcSpawnedLevel;
        public bool IsSpawning;
        public float CreateTime;
        public float CreateTimeLeft;
        
        public List<NpcModel> UnitsList;
        public BuildingType BuildingType;

        public ManagerModel Manager;
    }
    [Serializable]
    public struct NpcModel
    {
        public int Level;
        public float SpawnRate;
        public int GoldSpawnCount;
        public float СanSpawnRareItem;
        public float СanSpawn;
        public int RareItemCount;
    }

    [Serializable]
    public struct CastleData
    { 
        public int Level;

        public string Name;
        public string Description;
        public string NextLevelDescription;
        
        public int GoldAmount;
        public string Description1;
        public string Description2;
        
    }
    
    [Serializable]
    public struct GameEnvironment
    {
        public bool IsFogPassed;
        public bool IsBridgeRepaired;
        public bool IsTravelerSaved;
        public bool IsTombDiscovered;
    }
    
    [Serializable]
    public struct NpcLibrary
    {
        public int Level;
        public int Gold;
        public int Food;
        public string Name;
        public string Type;
    }

    [Serializable]
    public struct ManagerModel
    {
        public ManagerType ManagerType;
        
        public bool AbilityIsOn;
        public bool IsAbilityEnable;
        public float AbilityFinishTIme;
        public float CooldownFinishTIme;
    }
    
    [Serializable]
    public struct FruitsCollectionData
    {
        public string Key;
        public int Count;
    }
    
    [Serializable]
    public class TutorialManagerData
    {
        public ETutorialStep CurrentStep;
        public EBuildingStep CurrentBuildingStep;
    }
}