﻿using System;
using System.IO;
using UniRx;
using UnityEditor;
using UnityEngine;
using IOPath = System.IO.Path;

namespace Core.SaveLoad
{
    public class SaveManager
    {
        private const string PlayerDataFileName = "PlayerData.json";
        private Model Model;
        private OfflineIncome _offlineIncome;
        
#if UNITY_EDITOR
    
        [MenuItem("SaveManager/ClearPlayerProgress")]
        public static void ClearPlayerProgress() => Save(new PlayerData());
    
        private static readonly string Path = IOPath.Combine(Application.dataPath, PlayerDataFileName);
#else
        private static readonly string Path = IOPath.Combine(Application.persistentDataPath, PlayerDataFileName);
#endif

        public SaveManager(Model model)
        {
            Model = model;
            GameManager.Instance.Tutor.OnTutorialStepChanged.Subscribe(_ => SavePlayerProgress());
        }
        
        public void LoadPlayerProgress()
        {
            var playerData = Load<PlayerData>();
            if (playerData == null)
            {
                Debug.Log("DataLoaded incoreect");
                Model.DataLoaded.OnNext(Unit.Default);
                return;
            }
            
            Debug.Log($"{playerData.TutorialManagerData.CurrentStep} {playerData.TutorialManagerData.CurrentBuildingStep}");
            Model.SetLoadedData(playerData);
            CalculateOfflineBonus(playerData.TimeOnApplicationExit);
        }
        
        private void CalculateOfflineBonus(long playerDataTimeOnApplicationExit)
        {
            _offlineIncome = new OfflineIncome();
            _offlineIncome.GetTime(playerDataTimeOnApplicationExit);
        }
    
        public void SavePlayerProgress()
        {
            Save(new PlayerData()
            {
                Gold = Model.Gold.Value,
                Food = Model.Food.Value,
                Power = Model.Power.Value,
                
                IsStockActive = Model.Stock.IsActive,
                IsBankActive = Model.Bank.IsActive,
                Castle = Model.Castle.CastleDataProperty,
                
                SlotsList = Model.SaveSlotData(),
                
                GameEnvironment = Model.FogSlots.SaveEnvironment(),
                NpcLibraries = Model.NpcDiscoverManager.SaveNpcLibrary(),
                FruitCollection = Model.FruitCollection.SaveFruitCollection(),
                TutorialManagerData = GameManager.Instance.Tutor.TutorialManagerDataProperty,
                TimeOnApplicationExit = (long) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds,
                ProjectVersion = Application.version
            });
        }
        
        private static void Save(PlayerData obj) => File.WriteAllText(Path, JsonUtility.ToJson(obj,true));
        
        private T Load<T>() => File.Exists(Path) ? JsonUtility.FromJson<T>(File.ReadAllText(Path)) : default(T);

        public PlayerData GetLoad() => Load<PlayerData>();
    }
}