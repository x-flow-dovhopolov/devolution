﻿using UnityEngine;

public class SceneMusicPlayer : MonoBehaviour
{
    private BackgroundMusicPlayer BackgroundMusicPlayer;
    [SerializeField] private AudioSource AudioSource;
    
    void Start()
    {
        BackgroundMusicPlayer = GameObject.Find("BackgroundMusicPlayer").GetComponent<BackgroundMusicPlayer>();

        AudioSource.Play();
    }
}
