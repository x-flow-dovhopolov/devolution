﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial
{
    public class AdventureSceneTutorialHelper : MonoBehaviour
    {
        public List<GameObject> List;
        public List<GameObject> LighthouseList;

        private void Start()
        {
            //SceneManager.LoadSceneAsync("UIMenus", LoadSceneMode.Additive);
        }

        public void DoByItselfLogic(bool isEnable, int index = 500)
        {
            DisableCollider(isEnable);

            if (index != 500) EnableElement(index);
        }

        private void DisableCollider(bool isEnable)
        {
            for (int i = 0; i < List.Count; i++)
            {
                if(i == 6 || i == 7 || i == 8 || i == 9) continue; // fog not change
                
                if (List[i].GetComponent<BoxCollider2D>() != null)
                {
                    List[i].GetComponent<BoxCollider2D>().enabled = isEnable;
                    continue;
                }
                
                List[i].GetComponent<PolygonCollider2D>().enabled = isEnable;
               // List[i].GetComponent<CircleCollider2D>().enabled = isEnable;
            }
        }

        private void EnableElement(int index)
        {
            
            if(index == 6 || index == 7 || index == 8 || index == 9) return;
            
            if (List[index].GetComponent<BoxCollider2D>() != null)
            {
                List[index].GetComponent<BoxCollider2D>().enabled = true;
                return;
            }

            List[index].GetComponent<PolygonCollider2D>().enabled = true;
            //List[index].GetComponent<CircleCollider2D>().enabled = true;
        }
    }
}