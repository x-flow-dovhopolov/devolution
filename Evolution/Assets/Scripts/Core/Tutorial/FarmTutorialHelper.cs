﻿using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;

namespace Core.Tutorial
{
    public class FarmTutorialHelper : MonoBehaviour
    {
        public GameObject Building;
        public GameObject Manager;
        
        public void ShowRareItemOnScene() => GameManager.Instance.CreateTutorialMask(GameObject.Find("Rare item(Clone)"), 2f);

        public void SetFarmUIReadyToMerge(bool isBuildingEnable, bool isCanvasEnable)
        {
            Building.GetComponent<BoxCollider2D>().enabled = isBuildingEnable;
            InsideBuildingUiHelper.Instance.TurnInteractableCanvas(isCanvasEnable);
        }

        public void BlockManagerUntilBuyManagerStep(bool isEnable) =>
            Manager.GetComponent<BoxCollider2D>().enabled = isEnable;
    }
}