﻿using System.Collections;
using System.Linq;
using DG.Tweening;
using UI.CurrencyBuildings;
using UI.CurrencyBuildings.SceneSupport;
using UnityEngine;

namespace Core.Tutorial
{
    public class MillTutorialHelper : MonoBehaviour
    {
        private IEnumerator coroutine;
        public GameObject Building;
        public GameObject Manager;
        public GameObject BuildingLevelUpButton;
        public GameObject GoToMapButton;
        public GameObject MergeDirection;
        public GameObject ShopBtn;
        public GameObject ManagerAbilityButton;

        #region NPCcreate

        public void NpcCreateStep()
        {
            InsideBuildingUiHelper.Instance.InstantiateTutorialRedStripe(Building.transform.position, null);
            SetColliderWithBool(true, false);
            SetCanvasState(false);
        }
        public void AlternativeNpcCreateStep()
        {
            SetColliderWithBool(true, false);
            SetCanvasState(false);
        }
        
        public void ShowOnNpcToTap()
        {
            var npc = FindObjectsOfType<GameObject>().First(c => c.gameObject.name == "Mill_npc_1(Clone)");
            
            //InsideBuildingUiHelper.Instance.InstantiateTutorialRedStripe(npc.transform.position);

            //coroutine = TapOnNpc(npc.transform.position);
            coroutine = TapOnNpc(npc.gameObject);
            StartCoroutine(coroutine);

            SetColliderWithBool(true, false);
            SetCanvasState(false);
        }

        public void StopCoroutine()
        {
            StopCoroutine(coroutine);
            MergeDirection.SetActive(false);
            InsideBuildingUiHelper.Instance.DestroyRedStripe();
        }
        
        private IEnumerator TapOnNpc(GameObject npc)
        {
            while (true)
            {
                if (npc == null) break;
                yield return new WaitForSeconds(0.5f);

                //MergeDirection.SetActive(true);
                MergeDirection.transform
                    .DOJump(new Vector3(npc.transform.position.x + 0.5f, 
                            npc.transform.position.y + 0.5f, 
                            npc.transform.position.z), 
                        2f, 
                        (int) Random.Range(1f, 2f), 
                        0.75f, 
                        false)
                    .OnStart(() => MergeDirection.SetActive(true))
                    .OnComplete(() => MergeDirection.transform.position = npc.transform.position);
            }

            yield return null;
        }
        
        private IEnumerator TapOnNpc(Vector3 npc)
        {
            while (true)
            {
                yield return new WaitForSeconds(0.5f);

                MergeDirection.SetActive(true);
                MergeDirection.transform
                    .DOJump(new Vector3(npc.x, npc.y + 0.15f, npc.z), 
                        2f, 
                        (int) Random.Range(1f, 2f), 
                        0.75f, 
                        false)
                    .OnComplete(() => MergeDirection.transform.position = npc);
            }

            yield return null;
        }

        public void NpcCreateStepFinish()
        {
            InsideBuildingUiHelper.Instance.DestroyRedStripe();
            SetColliderWithBool(true, false);
        }

        #endregion

        #region NPCMerge

        public void StartCoroutineMergeNpc()
        {
            coroutine = MoveMouseToDirection();
            StartCoroutine(coroutine);
        }

        private void MergeNpcStep()
        {
            //SetColliderWithBool(false, false);
            SetColliderWithBool(true, false);
            SetCanvasState(false);
        }

        private IEnumerator MoveMouseToDirection()
        {
            MergeNpcStep();
        
            while (true)
            {
                var npcView1 = Building.transform.GetChild(0).transform.GetChild(0)
                    .transform.position;
                var npcView2 = Building.transform.GetChild(1).transform.GetChild(0)
                    .transform.position;
                
                MergeDirection.SetActive(true);
                MergeDirection.transform.position = npcView1;

                MergeDirection.transform.DOMove(npcView2, 0.75f)
                    .OnComplete(() => MergeDirection.transform.position = npcView1);
                yield return new WaitForSeconds(0.75f);
            }

            yield return null;
        }

        public void NpcMergeStepFinish()
        {
            StopCoroutine(coroutine);
            MergeDirection.SetActive(false);
            SetColliderWithBool(true, false);
        }

        #endregion

        #region BuyManager

        public void BuyManagerStep()
        {
            BuildingLevelUpButton.GetComponent<CanvasGroup>().interactable = false;
            ActiveStripeForManager();
            SetColliderWithBool(true, true);
        }

        private void ActiveStripeForManager()
        {
            InsideBuildingUiHelper.Instance.InstantiateTutorialRedStripe(Manager.transform.position, null);
        }

        public void BuyManagerStepFinish()
        {
            InsideBuildingUiHelper.Instance.DestroyRedStripe();
            SetColliderWithBool(true, true);
            BuildingLevelUpButton.GetComponent<CanvasGroup>().interactable = true;
        }

        #endregion

        #region UpdateBuilding, Manager and LeaveFirstScene

        public void ManagerAbilityStep()
        {
            BuildingLevelUpButton.GetComponent<CanvasGroup>().interactable = false;
            SetColliderWithBool(false, true);
            GameManager.Instance.CreateTutorialMaskOnWorldCanvas(ManagerAbilityButton, 3f);
        }

        public void ManagerAbilityStepFinish()
        {
            BuildingLevelUpButton.GetComponent<CanvasGroup>().interactable = false;
            SetColliderWithBool(true, true);
        }

        public void UpgradeBuildingStep()
        {
            //var v = new Vector3(BuildingLevelUpButton.transform.position.x, BuildingLevelUpButton.transform.position.y, 25f);
            var position = Camera.main.ScreenToWorldPoint(BuildingLevelUpButton.transform.position); // 1 way
            //var position = GetWorldPositionOnPlane(BuildingLevelUpButton.transform.position, 0);  // 2  way 
            //Tests();
            //var position = new Vector3(-6f, 8.5f, 0);
            Debug.Log($"position = {position}");
            //GameManager.Instance.CreateTutorialMask(BuildingLevelUpButton.gameObject, 2f);
            GameManager.Instance.CreateTutorialMaskOnWorldCanvas(BuildingLevelUpButton.gameObject, 2f);
            //InsideBuildingUiHelper.Instance.InstantiateTutorialRedStripe(position, BuildingLevelUpButton);
            SetColliderWithBool(false, false);
            BuildingLevelUpButton.GetComponent<CanvasGroup>().interactable = true;
        }
        
        public void BuildingLevelUpStepFinish()
        {
            InsideBuildingUiHelper.Instance.DestroyRedStripe();
            SetColliderWithBool(true, true);
            SetCanvasState(true);
        }
        
        public void UseShopStep()
        {
            GameManager.Instance.CreateTutorialMask(ShopBtn.gameObject);
            SetColliderWithBool(false, false);
            SetCanvasState(false);
        }
        
        public void LeaveFirstSceneStep()
        {
            ShopBtn.GetComponent<ShopButtonView>().Shop.SetActive(false);
            GameManager.Instance.CreateTutorialMask(GoToMapButton.gameObject);
            
            var position = Camera.main.ScreenToWorldPoint(GoToMapButton.transform.position);
            //InsideBuildingUiHelper.Instance.InstantiateTutorialRedStripe(position, null);

            SetColliderWithBool(false, false);
            SetCanvasState(false);

            InsideBuildingUiHelper.Instance.SetBackToMainSceneButtonEnable(true);
        }

        #endregion

        public void ShowRareItemOnScene() => GameManager.Instance.CreateTutorialMask(GameObject.Find("Rare item(Clone)"), 2f);

        public void SetFarmUIReadyToMerge(bool isBuildingEnable, bool isCanvasEnable)
        {
            Building.GetComponent<BoxCollider2D>().enabled = isBuildingEnable;
            InsideBuildingUiHelper.Instance.TurnInteractableCanvas(isCanvasEnable);
        }

        public void BlockManagerUntilBuyManagerStep(bool isEnable) =>
            Manager.GetComponent<BoxCollider2D>().enabled = isEnable;
        
        #region support methods

        private void SetCanvasState(bool isEnable)
        {
            InsideBuildingUiHelper.Instance.TurnInteractableCanvas(isEnable);
            BuildingLevelUpButton.GetComponent<CanvasGroup>().interactable = isEnable;
        }

        private void SetColliderWithBool(bool isEnable1, bool isEnable2)
        {
            Building.GetComponent<BoxCollider2D>().enabled = isEnable1;
            Manager.GetComponent<BoxCollider2D>().enabled = isEnable2;
        }

        #endregion
    }
}