﻿using DG.Tweening;
using UnityEngine;

namespace Core.Tutorial.RedStripe
{
    public class RedStripeView : MonoBehaviour
    {
        public bool IsWorking;
        private bool border;

        private void Start()
        {
            IsWorking = true;
        }

        public void Update()
        {
            //return;
            if (IsWorking)
            {
                if (border)
                {
                    transform.DOMoveX(transform.position.x + 0.25f, 0.5f).OnComplete(() => border = false);
                }
                else
                {
                    transform.DOMoveX(transform.position.x - 0.25f, 0.5f).OnComplete(() => border = true);
                }
            }
        }
    }
}