﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.ModelData.Buildings;
using Core.SaveLoad;
using Core.Tutorial.TutorialStateMachine;
using Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Academy;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Farm;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Tavern;
using UI;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial
{
    public enum ETutorialStep
    {
        BuyMillConversation,
        BuyMill,
        GoToMill,
        NpcCreate,
        CreateSecondNpc,
        TapOnNpc,
        MergeNpc,
        CollectBuildingUpgradeMoney,
        UpdateBuilding,
        CollectBuyManagerMoney,
        BuyManager,
        ManagerAbility,
        UseShop,
        CollectCastleUpgradeMoney,
        LeaveFirstScene,
        CastleUpgrade,
        CollectBuyFarmMoney,
        BuyFarm,
        GoToFarm,
        CreateTree,
        RareItem,
        CreateSecondTree,
        MergeTree,
        CollectFarmManagerMoney,
        BuyFarmManager,
        GoToStock,
        ExplainStock,
        CollectMapDiscoverMoney,
        MapDiscover,
        RepairBridge,
        HelpTraveler,
        CollectBarrackMoney,
        BuyBarrack,
        CreateWarrior,
        TavernCollectMoney,
        BuyTavern,
        GoToTavern,
        PlayTavernGame,
        CollectTombDiscoverMoney,
        TombDiscover,
        Finish
    }

    public enum EBuildingStep
    {
        Mill,
        Farm,
        NotAvailable,
        Academy,
        Tavern
    }

    public class TutorialManager
    {
        public Context Context;
        public bool IsFinished;
        
        private bool IsStarted;
        private TutorialManagerData TutorialManagerData;

        public TutorialManagerData TutorialManagerDataProperty
        {
            get => TutorialManagerData;
            set
            {
                Debug.Log($"value.CurrentBuildingStep {value.CurrentBuildingStep}");
                TutorialManagerData = value;
            }
        }

        public ETutorialStep CurrentStep
        {
            get => TutorialManagerData.CurrentStep;
            set => TutorialManagerData.CurrentStep = value;
        }

        private EBuildingStep CurrentBuildingStep
        {
            get => TutorialManagerData.CurrentBuildingStep;
            set => TutorialManagerData.CurrentBuildingStep = value;
        }

        public Subject<Unit> OnTutorialStepChanged { get; } = new Subject<Unit>();
        public Subject<string> OnStepFromSaveDefined { get; } = new Subject<string>();

        public bool TutorialIsLoad;
        public TutorialManager()
        {
            TutorialManagerData = new TutorialManagerData();
            CurrentStep = ETutorialStep.BuyMillConversation;
            CurrentBuildingStep = EBuildingStep.Mill;
            Context = new Context(null); // needs to be init before init state 
            //this.tutorFromStart = tutorFromStart;
        }

        public void StartTutorial()
        {
            SetTutorialStepFromLoadedData(CurrentStep);
            //SceneManager.LoadScene("Adventure");
            /*
            if (IsStarted) return;
            IsStarted = true;

            if (IsFinished)
            {
                SceneManager.LoadScene("Adventure");
                Context = new Context(new Finish());
                return;
            }
            
            
            if (tutorFromStart)
            {
                SetTutorialStepFromLoadedData(CurrentStep);
            }
            else
            {
                Context = new Context(new Finish());
                SceneManager.LoadScene("Adventure");
            }*/

            //SceneManager.LoadSceneAsync("UIMenus", LoadSceneMode.Additive);
        }

        public void NextStep(ETutorialStep nextStep)
        {
            if (IsFinished) return;

            CurrentStep = nextStep;
            OnTutorialStepChanged.OnNext(Unit.Default);
            
        }

        public void OnTutorialUIEnable()
        {
            GameManager.Instance.DestroyTutorialMask();
            AdventureMainUI.Instance.ReenableMainCanvas();
        }

        public List<bool> SetBuilderItems()
        {
            Debug.Log($"CurrentBuildingStep {CurrentBuildingStep}");
            switch (CurrentBuildingStep)
            {
                case EBuildingStep.NotAvailable:
                    return new List<bool> {false, false, false, false};
                case EBuildingStep.Mill:
                    return new List<bool> {true, false, false, false};
                case EBuildingStep.Farm:
                    return new List<bool> {false, true, false, false};
                case EBuildingStep.Academy:
                    return new List<bool> {false, false, true, false};
                case EBuildingStep.Tavern:
                    return new List<bool> {false, false, false, true};
                default:
                    return new List<bool> {true, true, true, true};
            }
        }

        public void SetBuildingStep() => CurrentBuildingStep += 1;

        private async void SetTutorialStepFromLoadedData(ETutorialStep step)
        {
            await UniTask.DelayFrame(1);
            State tutorialStateStep;

            switch (step)
            {
                case ETutorialStep.BuyMillConversation:
                    tutorialStateStep = new BuyMillConversation();
                    break;
                case ETutorialStep.BuyMill:
                    tutorialStateStep = new BuyMill();
                    break;
                case ETutorialStep.GoToMill:
                    tutorialStateStep = new GoToMill();
                    break;
                case ETutorialStep.NpcCreate:
                    tutorialStateStep = new NpcCreate();
                    break;
                case ETutorialStep.CreateSecondNpc:
                    tutorialStateStep = new CreateSecondNpc();
                    break;
                case ETutorialStep.TapOnNpc:
                    tutorialStateStep = new TapOnNpc();
                    break;
                case ETutorialStep.MergeNpc:
                    tutorialStateStep = new MergeNpc();
                    break;
                case ETutorialStep.CollectBuildingUpgradeMoney:
                    tutorialStateStep = new CollectBuildingUpgradeMoney();
                    break;
                case ETutorialStep.UpdateBuilding:
                    tutorialStateStep = new UpdateBuilding();
                    break;
                case ETutorialStep.CollectBuyManagerMoney:
                    tutorialStateStep = new CollectBuyManagerMoney();
                    break;
                case ETutorialStep.BuyManager:
                    tutorialStateStep = new BuyManager();
                    break;
                case ETutorialStep.ManagerAbility:
                    tutorialStateStep = new ManagerAbility();
                    break;
                case ETutorialStep.UseShop:
                    tutorialStateStep = new MillUseShop();
                    break;
                case ETutorialStep.CollectCastleUpgradeMoney:
                    tutorialStateStep = new CollectCastleUpgradeMoney();
                    break;
                case ETutorialStep.LeaveFirstScene:
                    tutorialStateStep = new LeaveFirstScene();
                    break;
                case ETutorialStep.CastleUpgrade:
                    tutorialStateStep = new CastleUpgrade();
                    break;
                case ETutorialStep.CollectBuyFarmMoney:
                    tutorialStateStep = new CollectBuyFarmMoney();
                    break;
                case ETutorialStep.BuyFarm:
                    tutorialStateStep = new BuyFarm();
                    break;
                case ETutorialStep.GoToFarm:
                    tutorialStateStep = new GoToFarm();
                    break;
                case ETutorialStep.CreateTree:
                    tutorialStateStep = new CreateTree();
                    break;
                case ETutorialStep.RareItem:
                    tutorialStateStep = new RareItem();
                    break;
                case ETutorialStep.CreateSecondTree:
                    tutorialStateStep = new CreateSecondTree();
                    break;
                case ETutorialStep.MergeTree:
                    tutorialStateStep = new MergeTree();
                    break;
                case ETutorialStep.CollectFarmManagerMoney:
                    tutorialStateStep = new CollectFarmManagerMoney();
                    break;
                case ETutorialStep.BuyFarmManager:
                    tutorialStateStep = new BuyFarmManager();
                    break;
                case ETutorialStep.GoToStock:
                    tutorialStateStep = new GoToStock();
                    break;
                case ETutorialStep.ExplainStock:
                    tutorialStateStep = new ExplainStock();
                    break;
                case ETutorialStep.CollectMapDiscoverMoney:
                    tutorialStateStep = new CollectMapDiscoverMoney();
                    break;
                case ETutorialStep.MapDiscover:
                    tutorialStateStep = new MapDiscover();
                    break;
                case ETutorialStep.RepairBridge:
                    tutorialStateStep = new RepairBridge();
                    break;
                case ETutorialStep.HelpTraveler:
                    tutorialStateStep = new HelpTraveler();
                    break;
                case ETutorialStep.CollectBarrackMoney:
                    tutorialStateStep = new CollectBarrackMoney();
                    break;
                case ETutorialStep.BuyBarrack:
                    tutorialStateStep = new BuyBarrack();
                    break;
                case ETutorialStep.CreateWarrior:
                    tutorialStateStep = new CreateWarrior();
                    break;
                case ETutorialStep.TavernCollectMoney:
                    tutorialStateStep = new TavernCollectMoney();
                    break;
                case ETutorialStep.BuyTavern:
                    tutorialStateStep = new BuyTavern();
                    break;
                case ETutorialStep.GoToTavern:
                    tutorialStateStep = new GoToTavern();
                    break;
                case ETutorialStep.PlayTavernGame:
                    tutorialStateStep = new PlayTavernGame();
                    break;
                case ETutorialStep.CollectTombDiscoverMoney:
                    tutorialStateStep = new CollectTombDiscoverMoney();
                    break;
                case ETutorialStep.TombDiscover:
                    tutorialStateStep = new TombDiscover();
                    break;
                case ETutorialStep.Finish:
                    tutorialStateStep = new Finish();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(step), step, null);
            }

            /*if use in tutorialStateStep constructor @GameManager.Instance.Model.CurrentBuilding.@
             CurrentBuilding is always 0*/
            tutorialStateStep.PreparingToLoadScene(); 
            Context.SetStateFromSave(tutorialStateStep);
            TutorialSceneManagerHelper.Instance.SubscribeContextEvent();
            TutorialIsLoad = true;
            Debug.Log($"TutorialIsLoad = true;");
        }
    }
}