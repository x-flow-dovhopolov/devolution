﻿using System.Collections;
using System.Collections.Generic;
using Core.Tutorial.TutorialStateMachine;
using UniRx;
using UnityEngine;
using UniRx.Async;
using UnityEngine.SceneManagement;

namespace Core.Tutorial
{
    public class TutorialSceneManagerHelper : MonoBehaviour
    {
        public static TutorialSceneManagerHelper Instance;
        
        //public List<GameObject> AdventureScenePlayableObjects;
        public TutorialManager TutorialManager;
        public bool SceneIsReady;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) 
                Destroy(gameObject);

            DontDestroyOnLoad(this);
        }

        private void Start()
        {
            TutorialManager = GameManager.Instance.Tutor;
            TutorialManager.OnStepFromSaveDefined.Subscribe(str => WaitForTwoFrames(str)).AddTo(this);
        }
        
        private void Update()
        {
            var sceneName = SceneManager.GetActiveScene().name == "Start scene";
            
            /*Debug.Log($"GameManager.Instance = {GameManager.Instance} ");
            Debug.Log($"and Tutor = {GameManager.Instance.Tutor}");*/
            //if (GameManager.Instance.Tutor.Context == null || !SceneIsReady || sceneName) return;
            if (GameManager.Instance.Tutor.Context == null  || !GameManager.Instance.TutorialIsReady || sceneName) return;
            //Debug.Log("Send Event");
            GameManager.Instance.Tutor.Context.Request1();
        }
        
        public void SubscribeContextEvent()
        {
            TutorialManager.Context.StopLoopCoroutineEvent.Subscribe(_ => StopCoroutineImm());
        }
        
        private async UniTask WaitForTwoFrames(string str)
        {
            //TODO check is a bug ? 
            //SceneManager.LoadSceneAsync(str).AsObservable();
            await UniTask.DelayFrame(3);
            SceneIsReady = true;
        }
        
        #region IEnumerators

        private IEnumerator coroutine;
        public delegate void AdventureLogic();
        
        private static IEnumerator LoopCoroutine(AdventureLogic func)
        {
            while (true)
            {
                yield return new WaitForSeconds(0.1f);

                func();
            }
        }
        
        public void StartLoopCoroutine(AdventureLogic func)
        {
            coroutine = LoopCoroutine(func);
            StartCoroutine(coroutine);
        }
        
        private void StopCoroutineImm() => StopCoroutine(coroutine);
        
        #endregion
    }
}
