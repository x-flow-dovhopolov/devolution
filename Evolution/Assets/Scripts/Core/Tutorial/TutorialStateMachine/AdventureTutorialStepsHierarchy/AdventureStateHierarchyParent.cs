﻿using UniRx;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class AdventureStateHierarchyParent : State
    {
        protected int GoldCount;
        protected int FoodCount;
        protected int PowerCount;
        protected CompositeDisposable Disposable;
        
        public Subject<Unit> OnCastleUpgrade{ get; } = new Subject<Unit>();
        
        protected AdventureStateHierarchyParent()
        {
            Disposable = new CompositeDisposable();   
        }
        
        public override void Handle1()
        {
            if (IsStarted) return;
        }

        public override void Handle2()
        {
            if (IsFinished) return;
        }

        public override void PreparingToLoadScene()
        {
            //GameManager.Instance.Tutor.OnStepFromSaveDefined.OnNext("Adventure");
            GameManager.Instance.Tutor.OnStepFromSaveDefined.OnNext("Adventure");
        }
    }
}