﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Academy;
using UI;
using UniRx;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class BuyBarrack : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public BuyBarrack()
        {
            HintText = HintData.BuildBarrack.Message;
            //GameManager.Instance.Tutor.SetBuildingStep();
        }

        public override void Handle1()
        {
            if (IsStarted) return;

            OnCurrencyBuildingBought.Subscribe(_ => Handle2());
            OnCollectMoneyFinish.Subscribe(_ => DoStep());

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }
        
        public void StartStep()
        {
            IsStarted = true;

            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.Gold.Value >= GameSetting.Builds.Find("Barrack").BasePrice &&
                        GameManager.Instance.Model.Food.Value >= GameSetting.Builds.Find("Barrack").BasePriceFood)
                    {
                        GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                    }
                });
        }

        public void DoStep()
        {
            if (IsInProgress) return;

            IsInProgress = true;
            
            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); // StopCoroutine
            OnCollectMoneyFinish.Dispose();

            AdventureMainUI.Instance.TutorialMasKEnable(3);
        }

        public void FinishStep()
        {
            IsFinished = true;
            AdventureMainUI.Instance.TutorialMasKEnable(3);
            
            HintText = HintData.EnterBarrack.Message;
            
            Context.TransitionTo(new CreateWarrior());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CreateWarrior);
        }
    }
}