﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UI;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class BuyFarm : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public BuyFarm()
        {
            HintText = HintData.BuildFarm.Message;
            DialogFinished.Subscribe(_ => DoStep());
            OnCurrencyBuildingBought.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "Adventure") return;

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished) return;

            FinishStep();
            DialogFinished.Dispose();
            OnCurrencyBuildingBought.Dispose();
        }

        public void StartStep()
        {
            IsStarted = true;

            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.BuildFarm);
        }

        public void DoStep()
        {
            AdventureMainUI.Instance.TutorialMasKEnable(2);
        }

        public void FinishStep()
        {
            IsFinished = true;
            
            AdventureMainUI.Instance.ReenableMainCanvas();
            GameManager.Instance.DestroyTutorialMask();
            
            Context.TransitionTo(new GoToFarm());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.GoToFarm);
        }
    }
}