﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UI;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class BuyMill : AdventureStateHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public BuyMill()
        {
            HintText = HintData.BuildMill.Message;
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;
            
            OnCurrencyBuildingBought.Subscribe(_ => Handle2());
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished) return;

            FinishStep();
            DialogFinished.Dispose();
        }

        public void StartStep()
        {
            AdventureMainUI.Instance.TutorialMasKEnable(1);
        }

        public void DoStep()
        {
            OnCurrencyBuildingBought.Dispose();
        }

        public void FinishStep()
        {
            AdventureMainUI.Instance.ReenableMainCanvas();
            GameManager.Instance.DestroyTutorialMask();
            IsFinished = true;
            
            Context.TransitionTo(new GoToMill());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.GoToMill);
        }
    }
}