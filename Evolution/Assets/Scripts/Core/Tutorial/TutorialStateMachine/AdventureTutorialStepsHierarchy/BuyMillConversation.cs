﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class BuyMillConversation : AdventureStateHierarchyParent, IStepFinish, IStepStart
    {
        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;
            
            DialogFinished.Subscribe(_ => Handle2());
            StartStep();
        }

        public override void Handle2()
        {
           if (IsFinished) return;
            
            FinishStep();
            DialogFinished.Dispose();
        }

        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.WelcomeAndBuildMill);
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            
            Context.TransitionTo(new BuyMill());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.BuyMill);
        }
    }
}