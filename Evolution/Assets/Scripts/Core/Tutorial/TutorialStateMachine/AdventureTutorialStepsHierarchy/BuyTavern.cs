﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy;
using UI;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class BuyTavern : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public BuyTavern()
        {
            HintText = HintData.BuildTavern.Message;
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "Adventure") return;
            IsStarted = true;

            OnCurrencyBuildingBought.Subscribe(_ => Handle2());

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            OnCurrencyBuildingBought.Dispose();
            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;
            AdventureMainUI.Instance.MoveCameraOnBuyTavernStep();
            //AdventureMainUI.Instance.TutorialMasKEnable(10);
        }

        public void DoStep()
        {
            if (IsInProgress) return;

            DialogFinished.Dispose();

            AdventureMainUI.Instance.TutorialMasKEnable(10);
        }

        public void FinishStep()
        {
            IsFinished = true;
            
            Context.TransitionTo(new GoToTavern());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.GoToTavern);
        }
    }
}