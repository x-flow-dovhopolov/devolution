﻿using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Academy;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class CastleUpdate : AdventureStateHierarchyParent, IStepStart, IStepFinish
    {
        public override void Handle1()
        {
            if (IsStarted) return;

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            IsStarted = true;
            IsInProgress = true;
            Handle2();
        }

        public void FinishStep()
        {
            IsFinished = true;
            Context.TransitionTo(new CreateWarrior());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CreateWarrior);
        }
    }
}