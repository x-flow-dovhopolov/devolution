﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill;
using UI;
using UI.Environment;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class CastleUpgrade: AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public CastleUpgrade()
        {
            HintText = HintData.UpgradeCastle.Message;
            
            DialogFinished.Subscribe(_ => DoStep());
            OnCastleUpgrade.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "Adventure") return;
            IsStarted = true;
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished) return;

            DialogFinished.Dispose();
            //TutorialActionFinished.Dispose();
            
            FinishStep();
        }

        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.CastleUpgrade);
        }

        public void DoStep()
        {
            AdventureMainUI.Instance.TutorialMasKEnable(0);
            
            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () =>
                {
                    var castleUpgradePanel = GameObject.Find("Castle upgrade(Clone)")?.GetComponent<CastleUIView>();
                    Debug.Log($"looking for btn active => {castleUpgradePanel}");
                    
                    if (castleUpgradePanel != null)
                    {
                        AdventureMainUI.Instance.Canvas.GetComponent<CanvasGroup>().interactable = false;
                        castleUpgradePanel.CloseBtn.gameObject.SetActive(false);
                        
                        GameManager.Instance.DestroyTutorialMask();
                        GameManager.Instance.CreateTutorialMask(castleUpgradePanel.Upgrade.gameObject, 2f);
                        Context.StopLoopCoroutineEvent.OnNext(Unit.Default); 
                    }
                });
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            
            AdventureMainUI.Instance.ReenableMainCanvas();
            GameManager.Instance.DestroyTutorialMask();

            Context.TransitionTo(new CollectBuyFarmMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectBuyFarmMoney);
        }
    }
}