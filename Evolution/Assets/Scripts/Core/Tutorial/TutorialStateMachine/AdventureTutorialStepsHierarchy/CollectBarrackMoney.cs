﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class CollectBarrackMoney : AdventureStateHierarchyParent, IStepFinish, IStepStart
    {
        public CollectBarrackMoney()
        {
            GoldCount = GameSetting.Builds.Find("Barrack").BasePrice; 
            FoodCount = GameSetting.Builds.Find("Barrack").BasePriceFood;
            
            HintText = HintData.CollectBarrackMoney.Message.
                Replace("{gold}", NumberFormat.ConvertNumberToFormat(GoldCount)).
                Replace("{food}", NumberFormat.ConvertNumberToFormat(FoodCount));
            
            OnCollectMoneyFinish.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            IsStarted = true;
            IsInProgress = true;

            //GameManager.Instance.Conversation.ExecuteHint(HintText);
            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.Gold.Value >= GameSetting.Builds.Find("Barrack").BasePrice &&
                        GameManager.Instance.Model.Food.Value >= GameSetting.Builds.Find("Barrack").BasePriceFood)
                    {
                        HintText = HintData.GoToCity.Message; 
                        if (SceneManager.GetActiveScene().name == "Adventure")
                        {
                            GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                        }
                    }
                });
        }

        public void FinishStep()
        {
            IsFinished = true;
            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); // StopCoroutine
            OnCollectMoneyFinish.Dispose();

            GameManager.Instance.Tutor.SetBuildingStep();
            Context.TransitionTo(new BuyBarrack());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.BuyBarrack);
        }
    }
}