﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class CollectMapDiscoverMoney : AdventureStateHierarchyParent, IStepFinish, IStepStart
    {
        public CollectMapDiscoverMoney()
        {
            GoldCount = GameSetting.Environments.Find("Fog").BasePrice;
            FoodCount = GameSetting.Environments.Find("Fog").BasePriceFood;
            
            HintText = HintData.CollectMapDiscoverMoney.Message.
                Replace("{gold}", NumberFormat.ConvertNumberToFormat(GoldCount)).
                Replace("{food}", NumberFormat.ConvertNumberToFormat(FoodCount));
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            OnCollectMoneyFinish.Subscribe(_ => Handle2());

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;

            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.FogSlots.CheckEnoughCurrencyToDiscover())
                    {
                        HintText = HintData.GoToCity.Message; 
                        if (SceneManager.GetActiveScene().name == "Adventure")
                        {
                            GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                        }
                    }
                });
        }

        public void FinishStep()
        {
            IsFinished = true;

            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); 
            OnCollectMoneyFinish.Dispose();

            Context.TransitionTo(new MapDiscover());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.MapDiscover);
        }
    }
}