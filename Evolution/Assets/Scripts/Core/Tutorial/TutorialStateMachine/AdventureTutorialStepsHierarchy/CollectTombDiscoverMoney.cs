﻿using Core.Conversation;
using Core.ModelData.Environment.EnvironmentTypes;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class CollectTombDiscoverMoney : AdventureStateHierarchyParent, IStepFinish, IStepStart
    {
        public CollectTombDiscoverMoney()
        {
            FoodCount = GameSetting.Environments.Find("Enemy").BasePriceFood;
            PowerCount = GameSetting.Environments.Find("Enemy").BasePricePower;
            HintText = HintData.CollectTombDiscoverMoney.Message.
                Replace("{food}", NumberFormat.ConvertNumberToFormat(FoodCount)).
                Replace("{power}", NumberFormat.ConvertNumberToFormat(PowerCount));
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "Adventure") return;

            OnCollectMoneyFinish.Subscribe(_ => Handle2());

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            IsStarted = true;
            IsInProgress = true;

            //GameManager.Instance.Conversation.ExecuteHint(HintText);
            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.FogSlots.fogSlot[2].CheckEnoughCurrencyToDiscoverInsideFogSlot())
                    {
                        HintText = HintData.GoToCity.Message; 
                        if (SceneManager.GetActiveScene().name == "Adventure")
                        {
                            GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                        }
                    }
                });
        }

        public void FinishStep()
        {
            IsFinished = true;

            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); // StopCoroutine
            OnCollectMoneyFinish.Dispose();

            Context.TransitionTo(new TombDiscover());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.TombDiscover);
        }
    }
}