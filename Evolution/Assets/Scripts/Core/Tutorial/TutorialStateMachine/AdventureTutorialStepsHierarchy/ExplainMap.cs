﻿using Core.Tutorial.TutorialStateMachine.ITutorialHelper;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class ExplainMap : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public override void Handle1()
        {
            return;
        }

        public override void Handle2()
        {
            return;
        }

        public void StartStep()
        {
            return;
        }

        public void DoStep()
        {
            return;
        }

        public void FinishStep()
        {
            return;
        }
    }
}