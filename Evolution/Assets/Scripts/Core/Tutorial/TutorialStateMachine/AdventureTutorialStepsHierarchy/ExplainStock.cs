﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UI;
using UniRx;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class ExplainStock : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public ExplainStock()
        {
            HintText = HintData.GoToStock.Message;

            DialogFinished.Subscribe(_ =>
            {
                HintText = HintData.CloseStock.Message; 
                DialogFinished.Dispose();
            });
            OnStockOpened.Subscribe(_ => DoStep());
            OnStockCloseAfterExplain.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted ) return;
            IsStarted = true;
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;
            
            OnStockCloseAfterExplain.Dispose();
            FinishStep();
        }
        
        public void StartStep()
        {
            AdventureMainUI.Instance.TutorialMasKEnable(4);
        }
        
        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;
            
            OnStockOpened.Dispose();
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.StockOverview);
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            Context.TransitionTo(new CollectMapDiscoverMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectMapDiscoverMoney);
        }
    }
}