﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Farm;
using UI;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class GoToFarm : AdventureStateHierarchyParent, IStepStart, IStepFinish
    {
        public GoToFarm()
        {
            HintText = HintData.GoToFarm.Message;
            
            DialogFinished.Subscribe(_ => DoStep());
            OnSceneChanged.Subscribe(_ =>
            {
                Debug.Log("Step Changed from GoToFarm");
                Handle2();
            }).AddTo(Disposable);
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished) return;
            
            FinishStep();
            DialogFinished.Dispose();
        }
        
        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson
                .GoToFarm);
        }
        
        private void DoStep()
        {
            DialogFinished.Dispose();
            AdventureMainUI.Instance.TutorialMasKEnable(2);
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            Disposable.Clear();
            
            Context.TransitionTo(new CreateTree());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CreateTree);
        }
    }
}