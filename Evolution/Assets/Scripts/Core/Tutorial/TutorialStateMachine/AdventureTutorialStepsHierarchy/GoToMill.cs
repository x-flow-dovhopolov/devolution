﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill;
using UI;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class GoToMill : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public GoToMill()
        {
            HintText = HintData.GoToMill.Message;
            
            DialogFinished.Subscribe(_ => DoStep());
            OnSceneChanged.Subscribe(_ =>
            {
                Handle2();
            }).AddTo(Disposable);
        }

        public override void Handle1()
        {
            if (IsStarted ) return;
            IsStarted = true;
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
            DialogFinished.Dispose();
            //TutorialActionFinished.Dispose();
        }

        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.GoToMill);
        }

        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;

            AdventureMainUI.Instance.TutorialMasKEnable(1);
        }

        public void FinishStep()
        {
            IsFinished = true;
            Disposable.Clear();
            
            Context.TransitionTo(new NpcCreate());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.NpcCreate);
        }
    }
}