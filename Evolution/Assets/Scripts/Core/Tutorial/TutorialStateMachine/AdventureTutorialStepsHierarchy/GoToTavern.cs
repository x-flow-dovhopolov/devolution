﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Tavern;
using UI;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class GoToTavern : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public GoToTavern()
        {
            HintText = HintData.GoToTavern.Message;

            DialogFinished.Subscribe(_ => DoStep());
            OnSceneChanged.Subscribe(_ =>
                {
                    Handle2();
                })
                .AddTo(Disposable);
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.GoToTavern);
        }

        public void DoStep()
        {
            if (IsInProgress) return;

            IsInProgress = true;

            AdventureMainUI.Instance.TutorialMasKEnable(10);
        }

        public void FinishStep()
        {
            IsFinished = true;
            Disposable.Clear();

            Context.TransitionTo(new PlayTavernGame());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.PlayTavernGame);
        }
    }
}