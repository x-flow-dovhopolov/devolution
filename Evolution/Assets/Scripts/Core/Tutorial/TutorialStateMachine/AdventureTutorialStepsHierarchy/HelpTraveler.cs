﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class HelpTraveler : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public HelpTraveler()
        {
            HintText = HintData.HelpTraveler.Message;

            DialogFinished.Subscribe(_ => Handle2());
            OnEnvironmentPassed.Subscribe(_ => DoStep());
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            DialogFinished.Dispose();
            FinishStep();
        }

        public void StartStep()
        {
        }

        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;
            
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectBarrackMoney);
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson
                .CamillaAndBuildBarracks);
            OnEnvironmentPassed.Dispose();
        }

        public void FinishStep()
        {
            IsFinished = true;
            
            Context.TransitionTo(new CollectBarrackMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectBarrackMoney);
        }
    }
}