﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UI.Environment;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class MapDiscover : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public MapDiscover()
        {
            HintText = HintData.MapDiscover.Message;
        }
        
        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            SubscribeOnCurrentTutorialStepEvents();
            
            StartStep();
        }

        private void SubscribeOnCurrentTutorialStepEvents()
        {
            DialogFinished.Subscribe(_ => DoStep());
            OnFogDiscovered.Subscribe(_ => Handle2());
        }
        
        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            OnFogDiscovered.Dispose();
            FinishStep();
        }

        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson
                .DiscoverLand);
        }

        public void DoStep()
        {
            if (IsInProgress) return;

            IsInProgress = true;

            DialogFinished.Dispose();
        }

        public void FinishStep()
        {
            IsFinished = true;

            Context.TransitionTo(new RepairBridge());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.RepairBridge);
        }
    }
}