﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UI;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class RepairBridge : AdventureStateHierarchyParent, IStepStart, IStepFinish
    {
        public RepairBridge()
        {
            HintText = HintData.RepairBridge.Message;
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            OnEnvironmentPassed.Subscribe(_ => Handle2());

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            OnEnvironmentPassed.Dispose();
            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;
        }

        public void FinishStep()
        {
            IsFinished = true;

            Context.TransitionTo(new HelpTraveler());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.HelpTraveler);
        }
    }
}