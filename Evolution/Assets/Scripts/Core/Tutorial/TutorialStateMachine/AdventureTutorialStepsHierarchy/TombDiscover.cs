﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy;
using UI;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy
{
    public class TombDiscover : AdventureStateHierarchyParent, IStepStart, IStepInProgress, IStepFinish
    {
        public TombDiscover()
        {
            HintText = HintData.TombDiscover.Message;
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "Adventure") return;
            IsStarted = true;

            DialogFinished.Subscribe(_ =>
            {
                AdventureMainUI.Instance.MoveCameraToCaptureTombStep();
                DialogFinished.Dispose();
                //DoSecondPartOfStep());

            });
            OnCollectMoneyFinish.Subscribe(_ => DoStep());
            OnEnvironmentPassed.Subscribe(_ => Handle2());

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            OnEnvironmentPassed.Dispose();
            FinishStep();
        }

        public void StartStep()
        {
            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.FogSlots.fogSlot[2].CheckEnoughCurrencyToDiscoverInsideFogSlot())
                    {
                        GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                    }
                });
        }

        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;
            
            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); // StopCoroutine
            OnCollectMoneyFinish.Dispose();
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(
                ConversationDataFromJson.CaptureTomb);
        }

        /*private void DoSecondPartOfStep()
        {
            DialogFinished.Dispose();
        }*/

        public void FinishStep()
        {
            IsFinished = true;
            Context.TransitionTo(new Finish());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.Finish);
        }
    }
}