﻿using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine
{
    public class Context
    {
        public State _state;
        
        public ReactiveProperty<string> HintText;
        
        public Subject<Unit> StopLoopCoroutineEvent { get; } = new Subject<Unit>();
        
        public Context(State state)
        {
            HintText = new ReactiveProperty<string>("");
            TransitionTo(state);
        }

        public void TransitionTo(State state)
        {
            if (state == null) return;
            Debug.Log($"Context: Transition to {state.GetType().Name}.");
            _state = state;
            _state.SetContext(this);
        }

        public void Request1() => _state.Handle1();

        public void SetStateFromSave(State tutorialStateStep) => TransitionTo(tutorialStateStep);
    }
}