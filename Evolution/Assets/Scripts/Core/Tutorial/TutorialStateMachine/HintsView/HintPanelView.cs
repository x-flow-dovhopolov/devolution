﻿using TMPro;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.HintsView
{
    public class HintPanelView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI textField;
        
        public void SetData(string message) => textField.text = message;
    }
}
