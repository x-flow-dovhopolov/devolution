﻿using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.HintsView
{
    public class HintUIView : MonoBehaviour
    {
        [SerializeField] private HintPanelView HintPanel = default;

        private void Start()
        {
            HintPanel.SetData(GameManager.Instance.Tutor.Context.HintText.Value);
            
            GameManager.Instance.Tutor.Context.HintText
                .ObserveEveryValueChanged(hintText => hintText.Value)
                .Subscribe(hintText => HintPanel.SetData(hintText));
        }
    }
}