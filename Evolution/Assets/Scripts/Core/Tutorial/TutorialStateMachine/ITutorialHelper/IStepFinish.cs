﻿namespace Core.Tutorial.TutorialStateMachine.ITutorialHelper
{
    public interface IStepFinish
    {
        void FinishStep();
    }
}