﻿namespace Core.Tutorial.TutorialStateMachine.ITutorialHelper
{
    public interface IStepInProgress
    {
        void DoStep();
    }
}