﻿namespace Core.Tutorial.TutorialStateMachine.ITutorialHelper
{
    public interface IStepStart
    {
        void StartStep();
    }
}