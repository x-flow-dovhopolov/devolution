﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UI.CurrencyBuildings;
using UniRx;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Academy
{
    public class CreateWarrior : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public CreateWarrior()
        {
            HintText = HintData.CreateWarrior.Message;
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "CurrencyScene") return;
            IsStarted = true;
            
            GameManager.Instance.Model.CurrentBuilding.OnNpcSpawned.Subscribe(_ => Handle2()).AddTo(Disposable);
            
            InsideBuildingUiHelper.Instance.SetBackToMainSceneButtonEnable(false); 
            
            HelplessMethod();
            //StartSpawnNpc.Subscribe(_ => DoStep());
        }

        private void HelplessMethod()
        {
            if (GameManager.Instance.Model.CurrentBuilding.IsSpawning)
            {
                DoStep();
            }
            else
            {
                StartSpawnNpc.Subscribe(_ => DoStep());
            }
        }
        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;
            
            //TutorialActionFinished.Dispose();
            FinishStep();
        }
        
        public void StartStep()
        {
        }
        
        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;
            
            StartSpawnNpc.Dispose(); 
            
            HintText = HintData.Wait.Message;
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            
            Disposable.Clear();
            
            InsideBuildingUiHelper.Instance.SetBackToMainSceneButtonEnable(true);
            
            /*Context.TransitionTo(new UseShop());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.UseShop);*/
            Context.TransitionTo(new TavernCollectMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.TavernCollectMoney);
        }
        
        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 2;
            base.PreparingToLoadScene();
        }
    }
}