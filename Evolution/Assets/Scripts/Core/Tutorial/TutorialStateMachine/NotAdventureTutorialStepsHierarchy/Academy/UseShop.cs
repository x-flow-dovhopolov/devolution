﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Academy
{
    public class UseShop : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public UseShop()
        {
            HintText = HintData.UseShop.Message;
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "CurrencyScene") return;
            IsStarted = true;
            
            /*There is incorrent logic when subscribe in const. Incorrect index after load(index shows on Mill not Farm etc)*/
            DialogFinished.Subscribe(_ => DoStep());
            OnNpcBoughtInShop.Subscribe(_ => Handle2());
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished) return;
            
            OnNpcBoughtInShop.Dispose();
            FinishStep();
        }
        
        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson
                .ShopBarracks);
        }
        
        private void DoStep()
        {
            DialogFinished.Dispose();
            HintText = HintData.Create3LevelNpc.Message; //  "Получите воина 3 уровня"; // 
            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () =>
                {
                    Debug.Log($"cnt = {GameManager.Instance.Model.NpcDiscoverManager.BarrackCreatures.Count}");
                    //$"and TutorialActionFinished {TutorialActionFinished.HasObservers}");
                    if (GameManager.Instance.Model.NpcDiscoverManager.BarrackCreatures.Count >= 3)
                    {
                        IsInProgress = true;
                        HintText = "Купите воина";
                        Context.StopLoopCoroutineEvent.OnNext(Unit.Default);
                    }
                });
        }
        
        public void FinishStep()
        {
            IsFinished = true;

            HintText = "Покиньте здание";
            
            Context.TransitionTo(new TavernCollectMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.TavernCollectMoney);
        }
        
        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 2;
            base.PreparingToLoadScene();
        }
    }
}