﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Farm
{
    public class BuyFarmManager : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public BuyFarmManager()
        {
            HintText = HintData.HireFarmManager.Message;
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "CurrencyScene") return;
            IsStarted = true;
            
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            MillTutorialHelper.BlockManagerUntilBuyManagerStep(true);
            
            /*There is incorrent logic when subscribe in const. Incorrect index after load(index shows on Mill not Farm etc)*/
            //TutorialActionSecondStepFinished.Subscribe(_ => Handle2());
            OnTutorialManagerBought.Subscribe(_ => Handle2());
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;
            
            FinishStep();
        }
        
        public void StartStep()
        {
            IsInProgress = true;
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            
            OnTutorialManagerBought.Dispose();
            
            //GameManager.Instance.Tutor.IsTutorialManagerPlayed = true; // покупка менеджера больше не отправляет ивенты
            
            HintText = HintData.GoToCity.Message;
            
            Context.TransitionTo(new GoToStock());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.GoToStock);
        }
        
        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 1;
            base.PreparingToLoadScene();
        }
    }
}