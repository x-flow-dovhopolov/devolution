﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Farm
{
    public class CollectFarmManagerMoney : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public CollectFarmManagerMoney()
        {
            var managerCost = GameManager.Instance.Model.CurrentBuilding.Manager.ManagerCostProperty;
            var managerFoodCost = GameManager.Instance.Model.CurrentBuilding.Manager.ManagerFoodProperty;
            
            HintText = HintData.CollectFarmManagerMoney.Message.
                Replace("{gold}", NumberFormat.ConvertNumberToFormat(managerCost)).
                Replace("{food}", NumberFormat.ConvertNumberToFormat(managerFoodCost));
        }

        public override void Handle1()    
        {
            if (IsStarted) return;
            IsStarted = true;
            
            DoINeedBackBtn(true);
            /*There is incorrent logic when subscribe in const. Incorrect index after load(index shows on Mill not Farm etc)*/
            OnCollectMoneyFinish.Subscribe(_ => Handle2());
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;

            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.Gold.Value >= 5000 && GameManager.Instance.Model.Food.Value >= 100)
                    {
                        GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                    }
                });
        }

        public void FinishStep()
        {
            IsFinished = true;
            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); // StopCoroutine
            OnCollectMoneyFinish.Dispose();
            HintText = HintData.HireFarmManager.Message;
            
            Context.TransitionTo(new BuyFarmManager());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.BuyFarmManager);
        }

        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 1;
            base.PreparingToLoadScene();
        }
    }
}