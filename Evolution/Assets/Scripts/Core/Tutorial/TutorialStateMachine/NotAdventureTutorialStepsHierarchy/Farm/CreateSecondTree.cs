﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Farm
{
    public class CreateSecondTree : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public CreateSecondTree()
        {
            HintText = HintData.PlantOneMoreTree.Message;
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            /*There is incorrent logic when subscribe in const. Incorrect index after load(index shows on Mill not Farm etc)*/
            StartSpawnNpc.Subscribe(_ => Handle2());
            DoINeedBackBtn(false);

            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            MillTutorialHelper.BlockManagerUntilBuyManagerStep(false);
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished) return;

            //TutorialActionFinished.Dispose();
            FinishStep();
        }

        public void StartStep()
        {
        }

        public void FinishStep()
        {
            IsFinished = true;

            StartSpawnNpc.Dispose();

            Context.TransitionTo(new MergeTree());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.MergeTree);
        }
        
        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 1;
            base.PreparingToLoadScene();
        }
    }
}