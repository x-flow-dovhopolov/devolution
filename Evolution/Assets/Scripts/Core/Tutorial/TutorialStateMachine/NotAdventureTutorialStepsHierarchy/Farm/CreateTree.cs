﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Farm
{
    public class CreateTree : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "CurrencyScene") return;
            IsStarted = true;
            
            HintText = HintData.PlantTree.Message;
            
            GameManager.Instance.Model.CurrentBuilding.OnNpcSpawned.Subscribe(_ => Handle2()).AddTo(Disposable);
            
            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            MillTutorialHelper.BlockManagerUntilBuyManagerStep(false);
            
            HelplessMethod();
            /*StartSpawnNpc.Subscribe(_ =>
            {
                MillTutorialHelper.NpcCreateStepFinish();
                DoStep();
            });
            MillTutorialHelper.NpcCreateStep();*/
        }
        
        private void HelplessMethod()
        {
            if (GameManager.Instance.Model.CurrentBuilding.IsSpawning)
            {
                MillTutorialHelper.NpcCreateStepFinish();
                DoStep();
            }
            else
            {
                MillTutorialHelper.NpcCreateStep();
                StartSpawnNpc.Subscribe(_ =>
                {
                    MillTutorialHelper.NpcCreateStepFinish();
                    DoStep();
                });
            }
        }
        
        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;
            
            //TutorialActionFinished.Dispose();

            FinishStep();
        }
        
        public void StartStep()
        {
        }
        
        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;
            
            StartSpawnNpc.Dispose();
            HintText = HintData.Wait.Message;
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            
            Disposable.Clear();
            
            Context.TransitionTo(new RareItem());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.RareItem);
        }
        
        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 1;
            base.PreparingToLoadScene();
        }
    }
}