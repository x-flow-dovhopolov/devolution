﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Farm
{
    public class MergeTree: NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public MergeTree()
        {
            HintText = HintData.Wait.Message;
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;
            
            HintText = HintData.Create2LevelTree.Message;
            /*There is incorrent logic when subscribe in const. Incorrect index after load(index shows on Mill not Farm etc)*/
            GameManager.Instance.Model.CurrentBuilding.OnNpcMerged.Subscribe(_ =>
            {
                Handle2();
            }).AddTo(Disposable);
            
            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            MillTutorialHelper.BlockManagerUntilBuyManagerStep(false);

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished) return;

            //TutorialActionFinished.Dispose();
            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;

            MillTutorialHelper.SetFarmUIReadyToMerge(true, false);
        }

        public void FinishStep()
        {
            IsFinished = true;
            
            MillTutorialHelper.SetFarmUIReadyToMerge(true, true);
            StartSpawnNpc.Dispose();
            Disposable.Clear();
            
            InsideBuildingUiHelper.Instance.SetBackToMainSceneButtonEnable(true);
            
            Context.TransitionTo(new CollectFarmManagerMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectFarmManagerMoney);
        }
        
        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 1;
            base.PreparingToLoadScene();
        }
    }
}