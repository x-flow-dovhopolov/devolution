﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Farm
{
    public class RareItem : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public RareItem()
        {
            HintText = HintData.CollectHarvest.Message;
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            OnRareItemCollected.Subscribe(_ => Handle2());
            
            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            MillTutorialHelper.BlockManagerUntilBuyManagerStep(false);
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;
            
            FinishStep();
        }
        
        public void StartStep()
        {
            foreach (var unit in GameManager.Instance.Model.CurrentBuilding.UnitsList)
            {
                Debug.Log("In");
                IsInProgress = true;
                unit.RareItemSpawned.Subscribe(_ =>
                {
                    MillTutorialHelper.ShowRareItemOnScene();
                    //DoStep();
                }).AddTo(Disposable);


                /*if (unit.RareItemsList.Count != 0)
                {
                    MillTutorialHelper.ShowRareItemOnScene();
                }*/
            }
            
            OnRareItemCollected.Subscribe(_ => Handle2());
            /*GameManager.Instance.Model.CurrentBuilding.UnitsList[0].
                RareItemSpawned.Subscribe(_ => DoStep()).AddTo(Disposable);*/
        }
        
        public void DoStep()
        {
            /*if (IsInProgress) return;
            IsInProgress = true;
            
            MillTutorialHelper.ShowRareItemOnScene();*/
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            OnRareItemCollected.Dispose();
            Disposable.Clear();
            //GameManager.Instance.Tutor.IsRareItemCollected = true;

            InsideBuildingUiHelper.Instance.TurnInteractableCanvas(true);
            
            Context.TransitionTo(new CreateSecondTree());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CreateSecondTree);
        }
        
        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 1;
            base.PreparingToLoadScene();
        }
    }
}