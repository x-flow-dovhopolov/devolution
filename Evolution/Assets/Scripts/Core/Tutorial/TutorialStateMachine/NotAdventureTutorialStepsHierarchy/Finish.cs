﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy
{
    public class Finish : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public override void Handle1()
        {
            if (IsStarted) return;
            if(GameManager.Instance.Tutor.IsFinished) return;
            
            StartStep();
            DialogFinished.Subscribe(_ => Handle2());
        }

        public override void Handle2()
        {
            if (IsFinished ) return;

            HintText = "The end";
            DialogFinished.Dispose();
            FinishStep();
        }

        public void StartStep()
        {
            IsStarted = true;
            
            GameManager.Instance.Reward.SetActive(true);
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.AboutMapPieces);
        }

        public void FinishStep()
        {
            IsFinished = true;
            GameManager.Instance.Tutor.IsFinished = true;
        }

        public override void PreparingToLoadScene()
        {
            Debug.Log("FINISH");
            GameManager.Instance.Tutor.OnStepFromSaveDefined.OnNext("Adventure");
        }
    }
}