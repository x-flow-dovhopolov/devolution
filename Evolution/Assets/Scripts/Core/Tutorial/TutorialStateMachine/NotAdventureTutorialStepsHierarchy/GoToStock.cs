﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy
{
    public class GoToStock : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public GoToStock()
        {
            HintText = HintData.GoToCity.Message;
            
            //DialogFinished.Subscribe(_ => DoStep());
            //TutorialActionFinished.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "Adventure") return;
            IsStarted = true;

            DialogFinished.Subscribe(_ => Handle2());
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            //TutorialActionFinished.Dispose();
            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.GoToStock);
        }

        public void DoStep()
        {
            if (IsInProgress) return;
            //IsInProgress = true;

            //DialogFinished.Dispose();
            //TutorialActionFinished.OnNext(Unit.Default);
        }

        public void FinishStep()
        {
            IsFinished = true;
            DialogFinished.Dispose();
            Context.TransitionTo(new ExplainStock());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.ExplainStock);
        }
        
        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 1;
            base.PreparingToLoadScene();
        }
    }
}