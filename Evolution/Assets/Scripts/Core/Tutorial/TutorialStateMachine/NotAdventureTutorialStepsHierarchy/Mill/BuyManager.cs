﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class BuyManager : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public BuyManager()
        {
            HintText = HintData.HireManager.Message;
            
            DialogFinished.Subscribe(_ => DoStep());
            //TutorialActionSecondStepFinished.Subscribe(_ => Handle2());
            OnTutorialManagerBought.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            StartStep();
            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public  void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson
                .MillBuyManager);
        }

        public void DoStep()
        {
            if (IsInProgress) return;

            IsInProgress = true;
            DialogFinished.Dispose();

            MillTutorialHelper.BuyManagerStep(); 
        }

        public  void FinishStep()
        {
            IsFinished = true;

            //TutorialActionSecondStepFinished.Dispose();
            OnTutorialManagerBought.Dispose();
            MillTutorialHelper.BuyManagerStepFinish();
            
            Context.TransitionTo(new ManagerAbility());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.ManagerAbility);
        }
    }
}