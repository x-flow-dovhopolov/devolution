﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class CollectBuildingUpgradeMoney : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public CollectBuildingUpgradeMoney()
        {
            OnCollectMoneyFinish.Subscribe(_ => Handle2());
            var buildingPrice = GameManager.Instance.Model.CurrentBuilding.GetLevelUpPrice() + "";
            HintText = HintData.CollectUpgradeBuildingMoney.Message.Replace("{gold}", buildingPrice);
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;

            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.Gold.Value >= 
                        GameManager.Instance.Model.CurrentBuilding.GetLevelUpPrice())
                    {
                        GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                        Debug.Log("StopCoroutine(coroutine);");
                    }
                });
        }

        public void FinishStep()
        {
            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); // StopCoroutine
            OnCollectMoneyFinish.Dispose();
            IsFinished = true;
            
            Context.TransitionTo(new UpdateBuilding());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.UpdateBuilding);
        }
    }
}