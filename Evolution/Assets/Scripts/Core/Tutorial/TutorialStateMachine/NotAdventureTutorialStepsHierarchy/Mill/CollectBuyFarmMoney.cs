﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class CollectBuyFarmMoney : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public CollectBuyFarmMoney()
        {
            var farmPrice = GameSetting.Builds.Find("Farm").BasePrice;
            HintText = HintData.CollectFarmMoney.Message.Replace("{gold}", farmPrice + "");

            OnCollectMoneyFinish.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;

            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.Gold.Value >= GameSetting.Builds.Find("Farm").BasePrice)
                    {
                        GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                    }
                });
        }

        public void FinishStep()
        {
            IsFinished = true;
            
            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); // StopCoroutine
            OnCollectMoneyFinish.Dispose();
            
            Context.TransitionTo(new BuyFarm());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.BuyFarm);
        }
    }
}