﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class CollectBuyManagerMoney : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public CollectBuyManagerMoney()
        {
            OnCollectMoneyFinish.Subscribe(_ => Handle2());
            
            var managerPrice = GameManager.Instance.Model.CurrentBuilding.Manager.ManagerCostProperty + "";
            HintText = HintData.CollectManagerMoney.Message.Replace("{gold}", managerPrice);
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            DoINeedBackBtn(false);
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;

            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.Gold.Value >= 2000)
                    {
                        GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                        Debug.Log("Collect money for manager");
                    }
                });
        }

        public void FinishStep()
        {
            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); // StopCoroutine
            OnCollectMoneyFinish.Dispose();
            
            IsFinished = true;
            Context.TransitionTo(new BuyManager());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.BuyManager);
        }
    }
}