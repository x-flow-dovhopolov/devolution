﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class CollectCastleUpgradeMoney : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public CollectCastleUpgradeMoney()
        {
            var castleGoldAmount = GameManager.Instance.Model.Castle.GoldAmount;
            HintText = HintData.CollectCastleMoney.Message.Replace("{gold}", castleGoldAmount+ "");
            
            OnCollectMoneyFinish.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            DoINeedBackBtn(false);
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            IsInProgress = true;

            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.Gold.Value >= GameManager.Instance.Model.Castle.GoldAmount) 
                    {
                        GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                    }
                });
        }

        public void FinishStep()
        {
            IsFinished = true;
            
            Context.StopLoopCoroutineEvent.OnNext(Unit.Default);
            OnCollectMoneyFinish.Dispose();
            
            Context.TransitionTo(new LeaveFirstScene());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.LeaveFirstScene);
        }
    }
}