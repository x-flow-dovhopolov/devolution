﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class CreateSecondNpc: NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public CreateSecondNpc()
        {
            HintText = HintData.CreateOneMoreNpc.Message;
            StartSpawnNpc.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;
            
            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished) return;
            
            FinishStep();
            //TutorialActionFinished.Dispose();
        }
        
        public void StartStep()
        {
            MillTutorialHelper.NpcCreateStep();
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            
            StartSpawnNpc.Dispose();
            MillTutorialHelper.NpcCreateStepFinish();
            
            Context.TransitionTo(new TapOnNpc());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.TapOnNpc);
        }
    }
}