﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class LeaveFirstScene : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public LeaveFirstScene()
        {
            HintText = HintData.GoToCity.Message;
            DialogFinished.Subscribe(_ => DoStep());
            OnMillSceneFinishTutorial.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            DoINeedBackBtn(true);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.MillExit);
        }

        public void DoStep()
        {
            if (IsInProgress) return;

            IsInProgress = true;
            DialogFinished.Dispose();

            MillTutorialHelper.LeaveFirstSceneStep();
            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (SceneManager.GetActiveScene().name == "Adventure")
                    {
                        GameManager.Instance.Tutor.Context._state.OnMillSceneFinishTutorial.OnNext(Unit.Default);
                    }
                });
        }

        public void FinishStep()
        {
            IsFinished = true;

            Context.StopLoopCoroutineEvent.OnNext(Unit.Default); // StopCoroutine
            OnMillSceneFinishTutorial.Dispose();
            
            Context.TransitionTo(new CastleUpgrade());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CastleUpgrade);
        }
    }
}