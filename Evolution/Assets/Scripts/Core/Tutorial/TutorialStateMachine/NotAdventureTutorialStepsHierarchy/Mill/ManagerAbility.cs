﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class ManagerAbility : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public ManagerAbility()
        {
            HintText = string.Format(HintData.UseManager.Message);
            DialogFinished.Subscribe(_ => DoStep());
            OnManagerAbilityUsed.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;
            
            DoINeedBackBtn(false);
            
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            GameManager.Instance.Conversation.
                ExecuteConversationWithEvent(ConversationDataFromJson.MillUseSkill);
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            if (!GameManager.Instance.Model.CurrentBuilding.Manager.AbilityIsOnProperty)
            {
                GameManager.Instance.Conversation.
                    ExecuteConversationWithEvent(ConversationDataFromJson.MillUseSkill);
            }
        }

        public void DoStep()
        {
            if (IsInProgress) return;

            IsInProgress = true;
            DialogFinished.Dispose();

            MillTutorialHelper.ManagerAbilityStep();
        }

        public void FinishStep()
        {
            IsFinished = true;

            OnManagerAbilityUsed.Dispose();
            MillTutorialHelper.ManagerAbilityStepFinish();

            /*Context.TransitionTo(new MillUseShop());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.UseShop);*/
            Context.TransitionTo(new CollectCastleUpgradeMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectCastleUpgradeMoney);
        }
    }
}