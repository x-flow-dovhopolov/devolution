﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class MergeNpc : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public MergeNpc()
        {
            HintText = HintData.Wait.Message;
            DialogFinished.Subscribe(_ => DoStep());
            GameManager.Instance.Model.CurrentBuilding.OnNpcMerged.Subscribe(_ => ShowSecondPartOfStepDialog()).AddTo(Disposable);
        }

        public override void Handle1()
        {
            if (IsStarted || GameManager.Instance.Model.CurrentBuilding.UnitsList.Count < 2) return;
            IsStarted = true;

            HintText = HintData.Create2LevelNpc.Message;
            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;
            FinishStep();
        }

        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson
                .MillMergeWorkers);
        }

        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;
            
            DialogFinished.Subscribe(_ => Handle2());

            MillTutorialHelper.StartCoroutineMergeNpc();
        }

        private void ShowSecondPartOfStepDialog()
        {
            Debug.Log($"MillTutorialHelper {MillTutorialHelper}");
            MillTutorialHelper.NpcMergeStepFinish();
            Disposable.Clear();
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.
                MillMergeFinish);
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            DialogFinished.Dispose();
            
            Context.TransitionTo(new CollectBuildingUpgradeMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectBuildingUpgradeMoney);
        }
    }
}