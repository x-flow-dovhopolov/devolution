﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class MillUseShop : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public MillUseShop()
        {
            HintText = HintData.UseShop.Message;
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "CurrencyScene") return;
            IsStarted = true;
            
            OnUseShopStepStarted.OnNext(Unit.Default);
            DoINeedBackBtn(false);
            
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            MillTutorialHelper.BlockManagerUntilBuyManagerStep(false);
            
            DialogFinished.Subscribe(_ => DoStep());
            OnNpcBoughtInShop.Subscribe(_ => Handle2());
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished) return;
            
            OnNpcBoughtInShop.Dispose();
            FinishStep();
        }
        
        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson
                .ShopBarracks);
        }
        
        private void DoStep()
        {
            DialogFinished.Dispose();
            HintText = "Создайте рабочего 3 уровня";
            
            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () =>
                {
                    Debug.Log($"cnt = {GameManager.Instance.Model.NpcDiscoverManager.MillCreatures.Count}");
                    if (GameManager.Instance.Model.NpcDiscoverManager.MillCreatures.Count >= 3)
                    {
                        IsInProgress = true;
                        HintText = "Купите рабочего";
                        MillTutorialHelper.UseShopStep();
                        Context.StopLoopCoroutineEvent.OnNext(Unit.Default);
                    }
                });
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            
            /*Context.TransitionTo(new CollectCastleUpgradeMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectCastleUpgradeMoney);*/
            Context.TransitionTo(new CollectBuyManagerMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectBuyManagerMoney);
        }
    }
}