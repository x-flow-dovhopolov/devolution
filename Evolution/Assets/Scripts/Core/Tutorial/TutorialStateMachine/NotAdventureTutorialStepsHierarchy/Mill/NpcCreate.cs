﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class NpcCreate : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "CurrencyScene") return;
            IsStarted = true;
            
            HintText = HintData.CreateNpc.Message;
            
            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            
            GameManager.Instance.Model.CurrentBuilding.OnNpcSpawned.Subscribe(_ => Handle2()).AddTo(Disposable);
            HelplessMethod();
        }
        
        private void HelplessMethod()
        {
            if (GameManager.Instance.Model.CurrentBuilding.IsSpawning)
            {
                MillTutorialHelper.AlternativeNpcCreateStep();
                IsInProgress = true;
                SecondStep();
            }
            else
            {
                DialogFinished.Subscribe(_ => DoStep());
                GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson
                    .MillBuildWorkers);
            }
        }
        
        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;
            FinishStep();
        }

        public void StartStep()
        {
        }

        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;
            
            DialogFinished.Dispose();
            
            MillTutorialHelper.NpcCreateStep();
            StartSpawnNpc.Subscribe(_ =>
            {
                MillTutorialHelper.NpcCreateStepFinish();
                SecondStep();
            });
        }

        private void SecondStep()
        {
            StartSpawnNpc.Dispose(); 
            HintText = HintData.Wait.Message;
        }

        public void FinishStep()
        {
            IsFinished = true;

            Disposable.Clear();
            
            Context.TransitionTo(new CreateSecondNpc());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CreateSecondNpc);
        }
    }
}