﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class TapOnNpc : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart
    {
        public TapOnNpc()
        {
            HintText = HintData.NpcReward.Message;
            OnNpcTaped.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;

            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
            OnNpcTaped.Dispose();
        }

        public void StartStep()
        {
            IsInProgress = true;

            MillTutorialHelper.ShowOnNpcToTap();
        }

        public void FinishStep()
        {
            IsFinished = true;

            MillTutorialHelper.StopCoroutine();

            Context.TransitionTo(new MergeNpc());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.MergeNpc);
        }
    }
}