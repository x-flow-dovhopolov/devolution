﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Mill
{
    public class UpdateBuilding : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public UpdateBuilding()
        {
            HintText = HintData.UpgradeBuilding.Message;
        }

        public override void Handle1()
        {
            if (IsStarted) return;
            IsStarted = true;
            
            DoINeedBackBtn(false);
            MillTutorialHelper = GameObject.Find("TutorialHelper").GetComponent<MillTutorialHelper>();
            DialogFinished.Subscribe(_ => DoStep());
            OnBuildingUpgrade.Subscribe(_ => Handle2());

            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            FinishStep();
        }

        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson
                .MillUpgradeBuildingPart2);
        }

        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;
            
            DialogFinished.Dispose();

            MillTutorialHelper.UpgradeBuildingStep();
        }

        public void FinishStep()
        {
            OnBuildingUpgrade.Dispose();
            MillTutorialHelper.BuildingLevelUpStepFinish();
            
            IsFinished = true;
            Context.TransitionTo(new MillUseShop());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.UseShop);
            /*Context.TransitionTo(new CollectBuyManagerMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectBuyManagerMoney);*/
        }
    }
}