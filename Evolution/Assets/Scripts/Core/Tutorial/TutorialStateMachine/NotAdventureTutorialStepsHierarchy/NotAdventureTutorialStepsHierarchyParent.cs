﻿using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy
{
    public class NotAdventureTutorialStepsHierarchyParent : State
    {
        protected MillTutorialHelper MillTutorialHelper;

        protected CompositeDisposable Disposable;
        protected NotAdventureTutorialStepsHierarchyParent()
        {
            Disposable = new CompositeDisposable();
        }
        
        public override void Handle1()
        {
        }

        public override void Handle2()
        {
        }

        public  override void PreparingToLoadScene()
        {
            //GameManager.Instance.Tutor.OnStepFromSaveDefined.OnNext("SingleCurrencyBuildingScene");
            GameManager.Instance.Tutor.OnStepFromSaveDefined.OnNext("CurrencyScene");
        }

        protected void DoINeedBackBtn(bool iNeed)
        {
            InsideBuildingUiHelper.Instance.SetBackToMainSceneButtonEnable(iNeed);
        }
    }
}