﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy.Tavern
{
    public class PlayTavernGame : NotAdventureTutorialStepsHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public PlayTavernGame()
        {
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "Tavern") return;
            IsStarted = true;
            
            /*There is incorrent logic when subscribe in const. Incorrect index after load(index shows on Mill not Farm etc)*/
            DialogFinished.Subscribe(_ => DoStep());
            OnTavernGamePlayed.Subscribe(_ => Handle2());
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;

            OnTavernGamePlayed.Dispose();
            FinishStep();
        }
        
        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(ConversationDataFromJson.TavernPlayGame);
        }
        
        public void DoStep()
        {
            DialogFinished.Dispose();
            IsInProgress = true;
            HintText = HintData.PlayGame.Message;
        }

        public void FinishStep()
        {
            IsFinished = true;
            
            HintText = HintData.GoToCity.Message;
            Context.TransitionTo(new CollectTombDiscoverMoney());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.CollectTombDiscoverMoney);
        }
        
        public override void PreparingToLoadScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 3;
            GameManager.Instance.Tutor.OnStepFromSaveDefined.OnNext("Tavern");
        }
    }
}