﻿using Core.Conversation;
using Core.Tutorial.TutorialStateMachine.AdventureTutorialStepsHierarchy;
using Core.Tutorial.TutorialStateMachine.ITutorialHelper;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Tutorial.TutorialStateMachine.NotAdventureTutorialStepsHierarchy
{
    public class TavernCollectMoney : AdventureStateHierarchyParent, IStepFinish, IStepStart, IStepInProgress
    {
        public TavernCollectMoney()
        {
            GoldCount = GameSetting.Builds.Find("Tavern").BasePrice;
            FoodCount = GameSetting.Builds.Find("Tavern").BasePriceFood;
            
            HintText = HintData.TavernCollectMoney.Message.
                Replace("{gold}", NumberFormat.ConvertNumberToFormat(GoldCount)).
                Replace("{food}", NumberFormat.ConvertNumberToFormat(FoodCount));
            
            DialogFinished.Subscribe(_ => DoStep());
            OnCollectMoneyFinish.Subscribe(_ => Handle2());
        }

        public override void Handle1()
        {
            if (IsStarted || SceneManager.GetActiveScene().name != "Adventure") return;
            IsStarted = true;
            
            StartStep();
        }

        public override void Handle2()
        {
            if (IsFinished || !IsInProgress) return;
            
            OnCollectMoneyFinish.Dispose();
            FinishStep();
        }
        
        public void StartStep()
        {
            GameManager.Instance.Conversation.ExecuteConversationWithEvent(
                ConversationDataFromJson.BuildTavern);
        }
        
        public void DoStep()
        {
            if (IsInProgress) return;
            IsInProgress = true;
            
            TutorialSceneManagerHelper.Instance.StartLoopCoroutine(
                () => 
                {
                    if (GameManager.Instance.Model.Gold.Value >= GameSetting.Builds.Find("Tavern").BasePrice &&
                        GameManager.Instance.Model.Food.Value >= GameSetting.Builds.Find("Tavern").BasePriceFood)
                    {
                        HintText = HintData.GoToCity.Message;
                        if (SceneManager.GetActiveScene().name == "Adventure")
                        {
                            GameManager.Instance.Tutor.Context._state.OnCollectMoneyFinish.OnNext(Unit.Default);
                        }
                    }
                });
        }
        
        public void FinishStep()
        {
            IsFinished = true;
            Context.StopLoopCoroutineEvent.OnNext(Unit.Default);
            //GameManager.Instance.Tutor.IsBarrackBlockFinished = false;
            
            Context.TransitionTo(new BuyTavern());
            GameManager.Instance.Tutor.NextStep(ETutorialStep.BuyTavern);
        }
    }
}