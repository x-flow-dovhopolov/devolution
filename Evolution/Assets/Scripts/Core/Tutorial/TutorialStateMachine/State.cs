﻿using UniRx;

namespace Core.Tutorial.TutorialStateMachine
{
    public abstract class State
    {
        protected Context Context;

        protected bool IsStarted;
        protected bool IsInProgress;
        protected bool IsFinished;

        protected string HintText
        {
            set => GameManager.Instance.Tutor.Context.HintText.Value = value;
        }
        
        public Subject<Unit> OnNpcTaped {get;} = new Subject<Unit>();
        public Subject<Unit> OnStockOpened {get;} = new Subject<Unit>();
        public Subject<Unit> StartSpawnNpc {get;} = new Subject<Unit>(); 
        public Subject<Unit> DialogFinished {get;} = new Subject<Unit>();
        public Subject<Unit> OnSceneChanged {get;} = new Subject<Unit>();
        public Subject<Unit> OnFogDiscovered {get;} = new Subject<Unit>();
        public Subject<Unit> OnNpcBoughtInShop {get;} = new Subject<Unit>();
        public Subject<Unit> OnBuildingUpgrade {get;} = new Subject<Unit>();
        public Subject<Unit> OnTavernGamePlayed {get;} = new Subject<Unit>();
        public Subject<Unit> OnRareItemCollected {get;} = new Subject<Unit>();
        public Subject<Unit> OnEnvironmentPassed {get;} = new Subject<Unit>();
        public Subject<Unit> OnManagerAbilityUsed {get;} = new Subject<Unit>();
        public Subject<Unit> OnCollectMoneyFinish {get;} = new Subject<Unit>();
        public Subject<Unit> OnTutorialManagerBought {get;} = new Subject<Unit>();
        public Subject<Unit> OnStockCloseAfterExplain {get;} = new Subject<Unit>();
        public Subject<Unit> OnCurrencyBuildingBought {get;} = new Subject<Unit>();
        public Subject<Unit> OnMillSceneFinishTutorial {get;} = new Subject<Unit>();
        
        public Subject<Unit> OnUseShopStepStarted {get;} = new Subject<Unit>(); 
        public void SetContext(Context context) => Context = context;
        public abstract void Handle1();
        public abstract void Handle2();
        public abstract void PreparingToLoadScene();
    }
}