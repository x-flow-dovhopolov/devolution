﻿using System.Collections.Generic;
using Core;
using Core.ModelData.Buildings;
using Core.ModelData.Buildings.NpcModel;
using Core.Tutorial;
using GameObjects.Buildings.Units;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameObjects.Buildings.BuildingTypeView
{
    public class BuildingView : View
    {
        public Building Building;
        private NPC spawningNpc;
        [SerializeField] public TextMeshProUGUI buildingLevel;
        [SerializeField] public SpriteRenderer BuildingRoadSprite;
        [SerializeField] public SpriteRenderer CurrencyBuildingGround;
        [SerializeField] public SpriteRenderer BuildingBarrel;
        [SerializeField] public SpriteRenderer FirstUniqueBuldingItem;
        [SerializeField] public SpriteRenderer SecondUniqueBuldingItem;
        [SerializeField] public GameObject axisXLeft;
        [SerializeField] public GameObject axisXRight;
        [SerializeField] public GameObject axisYBottom;
        [SerializeField] public GameObject axisYUp;
        [SerializeField] public List<GameObject> CurrencyBuildingSpriteChilds;
        [SerializeField] public List<GameObject> UniqueItemsSprites;
        [HideInInspector] public float x1;
        [HideInInspector] public float x2;
        [HideInInspector] public float y1;
        [HideInInspector] public float y2;
        
        private void Start()
        {
            Building = GameManager.Instance.Model.CurrentBuilding;
            
            BuildingRoadSprite.sprite = Resources.Load<Sprite>(Building.RoadImage);
            if (Building.BuildingType == BuildingType.Mill)
            {
                CurrencyBuildingSpriteChilds[0].SetActive(true);
                UniqueItemsSprites[0].SetActive(true);
            }
            else if (Building.BuildingType == BuildingType.Farm)
            {
                CurrencyBuildingSpriteChilds[1].SetActive(true);
                UniqueItemsSprites[1].SetActive(true);
            }
            else if (Building.BuildingType == BuildingType.Barracks)
            {
                CurrencyBuildingSpriteChilds[2].SetActive(true);
                UniqueItemsSprites[2].SetActive(true);
            }
            
            BuildingBarrel.sprite = Resources.Load<Sprite>(Building.CurrencyBuildingBarrel);
            FirstUniqueBuldingItem.sprite = Resources.Load<Sprite>(Building.UniqueItem1);
            SecondUniqueBuldingItem.sprite = Resources.Load<Sprite>(Building.UniqueItem2);
            CurrencyBuildingGround.sprite = Resources.Load<Sprite>(Building.CurrencyBuildingGround);
            //gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(Building.CurrencySceneImage);
            
            OnBuildingLevelIncreased(Building.BuildingLevelProperty);
            
            Building.NpcDestroyed.Subscribe(OnNpcDestroyed).AddTo(gameObject);
            Building.LevelIncreased.Subscribe(OnBuildingLevelIncreased).AddTo(gameObject);
            Building.NpcBought.Subscribe(newNpc =>
            {
                OnNpcCreatedVector(newNpc, null);
            }).AddTo(gameObject);
            
            Building.OnNpcSpawned.Subscribe(modelData => OnNpcCreatedVector(modelData.Item1, modelData.Item2))
                .AddTo(gameObject);
            
            RangeForNpcToAppear();
            SetEarlierCreatedUnits();
        }

        private void SetEarlierCreatedUnits()
        {
            foreach (var npc in Building.UnitsList)
            {
                var spawnedSubject = Instantiate(Building.FarmNpcPrefabFactory(npc.Level), 
                    transform, true);
                spawnedSubject.transform.GetChild(0).GetComponent<NpcView>().Npc = npc;
                spawnedSubject.transform.position = new Vector3(Random.Range(x1, x2), Random.Range(y1, y2), 0);
            }
        }

        private void RangeForNpcToAppear()
        {
            x1 = axisXLeft.transform.position.x;
            x2 = axisXRight.transform.position.x;
            
            y1 = axisYBottom.transform.position.y ; 
            y2 = axisYUp.transform.position.y;
        }
        
        public override void OnPointerClick(PointerEventData eventData)
        {
            Building.SetSpawnFinishTime();
        }
        
        public void MassNpsSplash(NpcView npcView)
        {
            if (GameManager.Instance.Tutor.CurrentStep < ETutorialStep.MergeNpc) return;
            
            var childCount = transform.childCount;
            if (childCount <= 1) return;
            
            var npcList2 = new List<SplashSupportClass>();
            var counter = 0;
            npcList2.Add(new SplashSupportClass( npcView, counter++));
            
            /*Собирает список НПС в радиусе сплеша*/
            for (var i = 0; i < childCount; i++)
            {
                var tr = transform.GetChild(i).transform;//.GetChild(0);
                
                if(npcView.NpcLevel == Building.MaxLevel) continue; 
                if (tr.localPosition == npcView.transform.parent.localPosition) continue;
                var distanceSqr = (tr.position - npcView.transform.parent.position).sqrMagnitude;
                /*if (tr.localPosition == npcView.transform.localPosition) continue;
                var distanceSqr = (tr.position - npcView.transform.position).sqrMagnitude;*/
//                Debug.Log($"distanceSqr = {distanceSqr}");
                if (distanceSqr <= 2f)
                {
                    npcList2.Add(new SplashSupportClass(tr.GetChild(0).GetComponent<NpcView>(), counter++));
                    //Debug.Log(tr.GetComponent<NpcView>().name);
                }
            }
            //Debug.Log($"npcList2 = {npcList2.Count}");
            
            /*дополнительные проверки и мерж нпс*/
            for (var i = 0; i < npcList2.Count; i++)
            {
                var tr = npcList2[i];
                
                if(tr.isSplashed) continue;
                
                for (var j = 0; j < npcList2.Count; j++)
                {
                    var tr1 = npcList2[j];
                    if(tr.index == tr1.index || tr1.isSplashed) continue;
                    
                    if (tr.Level == tr1.Level)
                    {
                        tr.isSplashed = true;
                        tr1.isSplashed = true;
                        MergeUnits(tr.NpcView, tr1.NpcView);
                        break;
                    }
                }
            }
        }
        
        private void MergeUnits(NpcView npcView1, NpcView npcView2)
        {
            var afterMergePosition = (npcView1.transform.position + npcView2.transform.position) / 2f;
            Building.Merge(npcView1.Npc, npcView2.Npc, afterMergePosition);

            npcView1.Npc.CollectRareItemsOnMergeParent();
            npcView2.Npc.CollectRareItemsOnMergeParent();
        }

        private void OnNpcDestroyed(NPC npc)
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                var view = transform.GetChild(i).transform.GetChild(0).GetComponent<NpcView>();

                if (view == null) continue;

                if (view.Npc == npc) Destroy(transform.GetChild(i).gameObject);
            }
        }

        private void OnNpcCreatedVector(NPC npc, Vector3? position)
        {
            var spawnedSubject = Instantiate(Building.FarmNpcPrefabFactory(npc.Level), 
                transform, true);
            spawnedSubject.transform.GetChild(0).GetComponent<NpcView>().SetModel(npc);
            spawnedSubject.transform.position =
                position ?? new Vector3(Random.Range(x1, x2), Random.Range(y1, y2), 0);
        }

        private void OnBuildingLevelIncreased(int level) => buildingLevel.text = "" + level;
    }
}