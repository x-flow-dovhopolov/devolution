﻿using Core;
using Core.ModelData.Buildings.SupportBuildings;
using TMPro;
using UI;
using UI.Environment;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects.Buildings.BuildingTypeView
{
    public class CastleView : View
    {
        [SerializeField] private GameObject CastleUpgradePanel = default;
        [SerializeField] private GameObject Navigator = default;
        [HideInInspector] public Castle Castle;
        [SerializeField] public LocationGuildingUIView CastleUIScript;

        private void Start()
        {
            Castle = GameManager.Instance.Model.Castle;
            InitBuildingUiElement();
        }

        private void InitBuildingUiElement()
        {
            CastleUIScript.SetMarkEnable(false);
            CastleUIScript.SetBuildingName(Castle.Name);
            CastleUIScript.SetBuildingLevel(Castle.Level);
            
            Castle.LevelIncreased.Subscribe(level => CastleUIScript.SetBuildingLevel(level)).AddTo(gameObject);
        }
        public override void OnPointerClick(PointerEventData eventData)
        {
            var castleUpgrade = CreateCastleUpgrade();
        }
        
        private CastleUIView CreateCastleUpgrade()
        {
            var realCanvas = AdventureMainUI.Instance.Canvas;
            return Instantiate(CastleUpgradePanel, realCanvas.transform).GetComponent<CastleUIView>();
        }
    }
}
