﻿using GameObjects.Buildings.Units;
using UnityEngine;

namespace GameObjects.Buildings.BuildingTypeView
{
    public class SplashSupportClass
    {
        public int index;
        public bool isSplashed;
        public NpcView NpcView;
        public int Level => NpcView.NpcLevel;

        public SplashSupportClass(NpcView npcView, int index, bool isSplashed = false)
        {
            this.isSplashed = isSplashed;
            this.index = index;
            NpcView = npcView;
        }
    }
}