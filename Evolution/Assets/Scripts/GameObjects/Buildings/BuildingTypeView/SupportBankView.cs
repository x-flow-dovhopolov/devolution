﻿using Core;
using Core.ModelData.Buildings.SupportBuildings;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects.Buildings.BuildingTypeView
{
    public class SupportBankView : View
    {
        private Bank bank;

        private void Start()
        {
            gameObject.SetActive(false);

            bank = GameManager.Instance.Model.Bank;

            bank.FarmBuilt.Subscribe(_ => EnableItself()).AddTo(gameObject);

            if (bank.IsActive)
            {
                gameObject.SetActive(true);
            }
        }

        private void EnableItself() => gameObject.SetActive(true);
    }
}