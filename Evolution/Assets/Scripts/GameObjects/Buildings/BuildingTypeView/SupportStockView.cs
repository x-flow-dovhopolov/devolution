﻿using Core;
using Core.ModelData.Buildings.SupportBuildings;
using Core.Tutorial;
using UI;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects.Buildings.BuildingTypeView
{
    public class SupportStockView : View
    {
        private Stock stock;

        private void Start()
        {
            stock = GameManager.Instance.Model.Stock;

            stock.MillBuilt.Subscribe(_ => EnableItself()).AddTo(gameObject);

            /*cant use stock until it own tutorial step*/
            if (GameManager.Instance.Tutor.CurrentStep < ETutorialStep.ExplainStock)
            {
                
                gameObject.GetComponent<PolygonCollider2D>().enabled = false;
                //gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
            
            if (!stock.IsActive)
            {
                gameObject.SetActive(false);
            }
        }

        public override void OnPointerClick(PointerEventData eventData) =>
            AdventureMainUI.Instance.DisableUiElementsAndActiveStock();

        private void EnableItself() => gameObject.SetActive(true);
    }
}