﻿using System;
using Core;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace GameObjects.Buildings.Managers
{
    public class FillBarView : View
    {
        public float FillAmount;
        private float barMaskWidth;
        private RawImage bar;
        private RectTransform barMaskTransform;
        private const int Max = 100;

        private void Start()
        {
            barMaskTransform = transform.Find("bar_mask").GetComponent<RectTransform>();
            bar = transform.Find("bar_mask").Find("bar").GetComponent<RawImage>();
            barMaskWidth = barMaskTransform.sizeDelta.x;

            FillAmount = 0;

            if (GameManager.Instance.Model.CurrentBuilding.IsSpawning)
            {
                FillAmount = 100 * (1 - (GameManager.Instance.Model.CurrentBuilding.CreateTime - Time.time) /
                                    GameManager.Instance.Model.CurrentBuilding.GetBuildingSpawnTime());
            }

            UpdateBarMask();

            GameManager.Instance.Model.CurrentBuilding.OnNpcSpawned.Subscribe().AddTo(gameObject);
        }

        public void Update()
        {
            if (GameManager.Instance.Model.OpenedBuildingIndex == 9999) return;

            if (GameManager.Instance.Model.CurrentBuilding.IsSpawning)
            {
                UpdateFillBar();
                var uvRect = bar.uvRect;
                uvRect.x += .2f * Time.deltaTime;
                bar.uvRect = uvRect;

                UpdateBarMask();
            }

            if (Math.Abs(FillAmount - 100) < 0.0000001)
            {
                FillAmount = 0f;
                UpdateBarMask();
            }
        }

        private void UpdateFillBar()
        {
            var tmp = 100 / GameManager.Instance.Model.CurrentBuilding.GetBuildingSpawnTime();
            FillAmount += tmp * Time.deltaTime;
            FillAmount = Mathf.Clamp(FillAmount, 0f, Max);
        }

        private void UpdateBarMask()
        {
            var barMaskSizeDelta = barMaskTransform.sizeDelta;
            barMaskSizeDelta.x = Normal() * barMaskWidth;
            barMaskTransform.sizeDelta = barMaskSizeDelta;
        }

        private float Normal()
        {
            var tmp = FillAmount / Max;
            return tmp;
        }
    }
}