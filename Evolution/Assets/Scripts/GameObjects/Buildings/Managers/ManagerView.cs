﻿using Core;
using Core.ModelData.Buildings.Managers;
using UI.CurrencyBuildings;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameObjects.Buildings.Managers
{
    public class ManagerView : View
    {
        public Manager BManager;
        [SerializeField] private GameObject Managers;
        [SerializeField] private SpriteRenderer ManagerSprite;
        [SerializeField] private SpriteRenderer ManagerBackgroundSprite;
        
        [SerializeField] private GameObject ManagerUIPanel;
        [SerializeField] private Button ManagerAbilityBtn;
        
        private void Start()
        {
            BManager = GameManager.Instance.Model.CurrentBuilding.Manager;
            SubscribeToManagersEvents();
            if (BManager.ManagerDataProperty.managerType != ManagerType.Default)
            {
                OnManagerModelDataChanged();
            }
            
            GameManager.Instance.Model.CurrentBuilding.OnManagerSetToBuilding
                .Where(manager =>
                {
                    Debug.Log($"New Manager is Set {manager.ManagerDataProperty.managerType}");
                    return manager.ManagerDataProperty.managerType != ManagerType.Default;
                })
                .Subscribe(manager =>
                {
                    BManager = manager;
                    SubscribeToManagersEvents();
                    OnManagerModelDataChanged();
                }).AddTo(this);
                
            if (BManager.AbilityIsOnProperty)
                OnAbilityActive();
            else if (!BManager.IsAbilityEnableProperty)
                OnAbilityDeactivate();
        }

        private void SubscribeToManagersEvents()
        {
            BManager.ActivateAbility.Subscribe(_ => OnAbilityActive()).AddTo(gameObject);
            BManager.IsAbilityActiveEvent.Subscribe(_ => OnAbilityEnable()).AddTo(gameObject);
            BManager.IsAbilityDiActiveEvent.Subscribe(_ => OnAbilityDeactivate()).AddTo(gameObject);
        }
        
        public override void OnPointerClick(PointerEventData eventData)
        {
            if (BManager.ManagerDataProperty.managerType != ManagerType.Default) 
                return;
            
            InsideBuildingUiHelper.Instance.OpenManagerPanel();
            /*if (BManager.ManagerDataProperty.managerType != ManagerType.Default)
            {
                BManager.CheckAbility();
            }
            else
            {
                InsideBuildingUiHelper.Instance.OpenManagerPanel();
            }*/
        }

        private void OnAbilityDeactivate()
        {
            ManagerAbilityBtn.interactable = false;
            ManagerAbilityBtn.GetComponent<Image>().color = Color.white;
            Debug.Log("Ability deactivated");
            //ManagerBackgroundSprite.color = Color.gray;
        }

        private void OnAbilityActive()
        {
            //ManagerAbilityBtn.interactable = false;
            ManagerAbilityBtn.GetComponent<Image>().color = Color.red;
            Debug.Log("Ability activated");
            //ManagerBackgroundSprite.color = Color.red;
        }

        private void OnAbilityEnable()
        {
            ManagerAbilityBtn.interactable = true;
            Debug.Log("Ability ready");
            //ManagerBackgroundSprite.color = Color.green;
        }

        private void OnManagerModelDataChanged()
        {
            ManagerUIPanel.SetActive(true);
            ManagerAbilityBtn.OnClickAsObservable().Subscribe(_ => BManager.CheckAbility()).AddTo(this);
            
            ManagerSprite.sprite = Resources.Load<Sprite>(BManager.ManagerSprite);
            ManagerSprite.gameObject.transform.localScale = new Vector3(0.7f,0.7f, 1f);
        }
    }
}