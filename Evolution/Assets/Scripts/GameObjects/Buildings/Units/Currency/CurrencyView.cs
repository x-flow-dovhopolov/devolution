﻿using UnityEngine;

namespace GameObjects.Buildings.Units.Currency
{
    public class CurrencyView : MonoBehaviour
    {
        private const float Speed = 0.5f;
        [SerializeField] private TextMesh currencyCount;

        public void Start() => Destroy(gameObject, 1.5f);

        private void Update() => transform.Translate(Time.deltaTime * Speed * Vector3.up);

        public void SetCurrencyAmount(int value) => currencyCount.text = "" + value;
    }
}