﻿using TMPro;
using UnityEngine;

namespace GameObjects.Buildings.Units.Dialog
{
    public class DialogView : MonoBehaviour
    {
        private const float Speed = 0.5f;
        [SerializeField] private TextMeshProUGUI DialogText;

        public void Start() =>  Destroy(gameObject, 2f);

        public void SetCurrencyAmount(string value) => DialogText.text = value;
    }
}