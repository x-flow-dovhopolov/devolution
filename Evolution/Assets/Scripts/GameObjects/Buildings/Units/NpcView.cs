﻿using Core;
using Core.ModelData.Buildings;
using Core.ModelData.Buildings.NpcModel;
using Core.ModelData.StockItems;
using Core.Tutorial;
using DG.Tweening;
using GameObjects.Buildings.BuildingTypeView;
using GameObjects.Buildings.Units.Currency;
using GameObjects.Buildings.Units.Dialog;
using GameObjects.Buildings.Units.RareItems;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects.Buildings.Units
{
    public class NpcView : View, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private bool isMoving;
        private BuildingView bv;
        private float СanSpawnByClick = -1f;
        private GameObject screenMainBuilding;
        
        public int NpcLevel;
        public NPC Npc;
        public GameObject Coin;
        public GameObject Dialog;
        public GameObject RareFruit;
        
        private void Start()
        {
            /*screenMainBuilding = GameObject.Find("CurrencySceneMainBuilding");
            bv = screenMainBuilding.transform.GetChild(0).GetComponent<BuildingView>();*/
            bv = GameObject.Find("Building").GetComponent<BuildingView>();;

            Npc.NpcDialog.Subscribe(CreateNpcDialog).AddTo(gameObject);
            Npc.RareItemSpawned.Subscribe(CreateRareCoin).AddTo(gameObject);
            Npc.CoinSpawnedWithValue.Subscribe(CreateCoinWithValue).AddTo(gameObject);

            SetEarlierCreatedRareItems();
        }

        public void SetModel(NPC modelNpc) => Npc = modelNpc;

        private void SetEarlierCreatedRareItems()
        {
            foreach (var rareItem in Npc.RareItemsList)
            {
                CreateRareCoin(rareItem);
            }
        }

        public void OnBeginDrag(PointerEventData eventData) => isMoving = true;

        public void OnDrag(PointerEventData eventData)
        {
            var position = Camera.main.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, 25f));
            if (position.x <= bv.x1 || position.x >= bv.x2 || 
                position.y <= bv.y1 || position.y >= bv.y2)
            {
                return;
            }
            transform.parent.position = new Vector3(position.x, position.y, transform.position.z);
            /*var go = GameObject.Find("Scroll and LevelUp Navigator");
            
            var vec = Camera.main.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, 25f));
            
            if (vec.y + (GetComponent<SpriteRenderer>().bounds.size.y / 2) >= go.transform.position.y)
            {
                transform.position = new Vector3(vec.x, go.transform.position.y - (GetComponent<SpriteRenderer>().bounds.size.y / 2), vec.z);
                return;
            }
            transform.parent.position = vec;*/
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            isMoving = false;
            if (NpcLevel == Building.MaxLevel) return;
            bv.MassNpsSplash(this);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (isMoving) return;
            if (!(Time.time > СanSpawnByClick)) return;
            
            Npc.ShowNpcDialog();
            Npc.CurrencyCombine();
            
            bv.MassNpsSplash(this);
            
            СanSpawnByClick = Time.time + Npc.SpawnRateByClick;
            
            if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.TapOnNpc)
            {
                GameManager.Instance.Tutor.Context._state.OnNpcTaped.OnNext(Unit.Default);
            }
        }

        private void CreateCoinWithValue(int value)
        {
            var go = Instantiate(Coin, transform.position + new Vector3(0, 0.85f, 0), Quaternion.identity);
            go.GetComponent<CurrencyView>().SetCurrencyAmount(value);
        }

        private void CreateRareCoin(StockItem rareItem)
        {
            var position = transform.position;
            var rareItemView = Instantiate(RareFruit, position, Quaternion.identity);
            rareItemView.GetComponent<RareItemView>().SetModelItem(rareItem);

            rareItemView.transform.DOMoveX(position.x + Random.Range(-0.25f, 0.25f), 1);
            rareItemView.transform.DOMoveY(position.y - Random.Range(4f, 0.75f), 1).OnComplete(() =>
            {
                /*Debug.Log($"(rareItemView.transform.position = {rareItemView.transform.position}" +
                          $" -- {rareItemView.transform.localPosition}");*/
                if (rareItemView.transform.position.y <= -30)
                {
                    rareItemView.transform.DOMoveY(position.y + 2f, 1);
                }
                /*
                var positionLighthouse = GameObject.Find("Rare fruit position lighthouse");
                Debug.Log($"GetComponentInParent<Transform>().position.y {GetComponentInParent<Transform>().position.y}");
                if (GetComponentInParent<Transform>().localPosition.y < positionLighthouse.transform.position.y)
                {
                    rareItemView.transform.DOMoveY(position.y + 1f, 1);
                }
*/

            });
        }
        
        private void CreateNpcDialog(string dialogText)
        {
            var canvas = GameObject.Find("World Canvas");
            //var go = Instantiate(Dialog, canvas.transform, true);
            //var c = Camera.main.WorldToScreenPoint(transform.parent.position + new Vector3(3f, 3f, 0));
            var go = Instantiate(Dialog, canvas.transform);
            go.GetComponent<RectTransform>().localScale = new Vector3(0.025f, 0.025f, 1f);
            var c = transform.position + new Vector3(2f, 2f, 0);
            
            
            
            go.GetComponent<RectTransform>().position = c;
                
            /*var c = Utils.WorldToUiSpace(canvas.GetComponent<Canvas>(),
                parentPosition + new Vector3(3f, 3f, 0));*/
                
            go.GetComponent<DialogView>().SetCurrencyAmount(dialogText);
        }
    }
}