﻿using Core;
using Core.ModelData.StockItems;
using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.PlayerLoop;

namespace GameObjects.Buildings.Units.RareItems
{
    public class RareItemView : MonoBehaviour, IPointerClickHandler
    {
        private StockItem Item;
        
        public void OnPointerClick(PointerEventData eventData) => Collect();

        public void SetModelItem(StockItem item)
        {
            Item = item;
            Item.ParentMerged.Subscribe(_ => Collect()).AddTo(gameObject);
            
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(Item.Image);
        }

        private void Collect()
        {/*
            var position1 = Camera.main.ScreenToWorldPoint(GameObject.Find("Panel").
                GetComponent<RectTransform>().position);*/
            var position1 = GameObject.Find("Building").transform.position;
            
            transform.DOMoveY(position1.y, 1);
            transform.DOMoveX(position1.x, 1).OnComplete( () =>
            {
                if (GameManager.Instance.Tutor.Context._state.OnRareItemCollected.HasObservers)
                {
                    GameManager.Instance.Tutor.Context._state.OnRareItemCollected.OnNext(Unit.Default); 
                }
                /*if (!GameManager.Instance.Tutor.IsRareItemCollected)
                {
                    if (GameManager.Instance.Tutor.Context._state.OnRareItemCollected.HasObservers)
                    {
                        GameManager.Instance.Tutor.Context._state.OnRareItemCollected.OnNext(Unit.Default); 
                    }
                }*/
                
                GameManager.Instance.DestroyTutorialMask();
                
                Item.OnCollectRareItem();
                Destroy(gameObject);
            });
        }
    }
}