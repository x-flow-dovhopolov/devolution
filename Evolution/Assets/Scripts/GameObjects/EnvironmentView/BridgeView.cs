﻿using Core;
using Core.ModelData.Environment;
using Core.Tutorial;
using UI;
using UI.Environment;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects.EnvironmentView
{
    public class BridgeView : EnvironmentView
    {
        [SerializeField] private Sprite FixedBridge;
        [SerializeField] private GameObject BrokenBridge;
        [SerializeField] private GameObject FixedBridgeG;
        public GameObject QuestMarker;

        protected override void Start()
        {
            base.Start();
            DoSpecialLogic();
        }
        
        private void Update()
        {
            if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.RepairBridge)
            {
                GetComponent<PolygonCollider2D>().enabled = true;
                QuestMarker.gameObject.SetActive(true);
            }
        }
        protected override void ActiveDiscoverPanel()
        {
            MainPanel.gameObject.SetActive(true);

            MainPanel.Gold = Slot.GoldDiscoverCount;
            MainPanel.Food = Slot.FoodDiscoverCount;
            MainPanel.GoldText.text ="" + NumberFormat.ConvertNumberToFormat(Slot.GoldDiscoverCount);
            MainPanel.FoodText.text ="" + NumberFormat.ConvertNumberToFormat(Slot.FoodDiscoverCount);
            MainPanel.ButtonText.text = "Чинить";
            MainPanel.HeaderText.text = "Сломанный мост";

            GameManager.Instance.Tutor.OnTutorialUIEnable();
        }
        
        protected override void DoBuildingBusinessLogic()
        {
            BrokenBridge.SetActive(false);
            FixedBridgeG.SetActive(true);
            //GetComponent<SpriteRenderer>().sprite = FixedBridge;
            GetComponent<PolygonCollider2D>().enabled = false;
            //GetComponent<BoxCollider2D>().enabled = false;
            enabled = false;
        }
    }
}