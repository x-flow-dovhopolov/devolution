﻿using Core;
using Core.ModelData.Environment;
using Core.Tutorial;
using UI.Environment;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects.EnvironmentView
{
    public class EnemyBuildingView : EnvironmentView
    {
        public GameObject QuestMarker;
        
        protected override void Start()
        {
            base.Start();
            
            DoSpecialLogic();
        }
        
        private void Update()
        {
            if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.TombDiscover)
            {
                GetComponent<PolygonCollider2D>().enabled = true;
                QuestMarker.gameObject.SetActive(true);
            }
        }
        
        public override void OnPointerClick(PointerEventData eventData)
        {
            if (!FogData.fogSlot[0].IsPassed.Value)
            {
                Debug.Log("Repair bridge");
                return;
            }

            base.OnPointerClick(eventData);
        }

        protected override void ActiveDiscoverPanel()
        {
            MainPanel.gameObject.SetActive(true);
            
            MainPanel.Food = Slot.FoodDiscoverCount;
            MainPanel.Power = Slot.PowerDiscoverCount;
            
            MainPanel.GoldText.text = "" + NumberFormat.ConvertNumberToFormat(Slot.PowerDiscoverCount);
            MainPanel.FoodText.text = "" + NumberFormat.ConvertNumberToFormat(Slot.FoodDiscoverCount);
            MainPanel.ButtonText.text = "Захватить";
            MainPanel.HeaderText.text = "Гробница";

            GameManager.Instance.Tutor.OnTutorialUIEnable();
        }
        
        protected override void DoBuildingBusinessLogic()
        {
            QuestMarker.gameObject.SetActive(false);
            GetComponent<PolygonCollider2D>().enabled = false;
            /*gameObject.SetActive(false);
            enabled = false;*/
        }
    }
}