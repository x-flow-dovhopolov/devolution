﻿using Core;
using Core.ModelData.Environment;
using Core.Tutorial;
using UI;
using UI.Environment;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects.EnvironmentView
{    
    public class EnvironmentView : View
    {
        public int Index;
        public FogSlot FogData;
        public InsideFogSlot Slot;
        public EnvironmentDiscover MainPanel;
        
        protected virtual void Start()
        {
            FogData = GameManager.Instance.Model.FogSlots;
            
            if(Index >= 0)
                Slot = GameManager.Instance.Model.FogSlots.fogSlot[Index];
        }

        protected virtual void DoSpecialLogic()
        {
            FogData.fogSlot[Index].IsPassed
                .Where(isPassed => isPassed)
                .Subscribe(_ => { DoBuildingBusinessLogic();})
                .AddTo(this); 
        }
        
        public override void OnPointerClick(PointerEventData eventData)
        {
            if (FogData.fogSlot[Index].IsPassed.Value)
            {
                Debug.Log("Already built");
                return;
            }
            
            //GameManager.Instance.Model.GOOnMapSelected = this;
            ActiveDiscoverPanel();
        }
        
        protected virtual void ActiveDiscoverPanel()
        {
        }
        
        public void SetPointedPassed() => FogData.fogSlot[Index].IsPassedProperty = true;
        
        protected virtual void DoBuildingBusinessLogic()
        {
            if (GetComponent<BoxCollider2D>() != null)
            {
                GetComponent<BoxCollider2D>().enabled = false;
            }

            if (GetComponent<PolygonCollider2D>() != null)
            {
                GetComponent<PolygonCollider2D>().enabled = false;
            }
            enabled = false;
            Debug.Log("Default logic");
        }
    }
}