﻿using Core;
using Core.Tutorial;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects.EnvironmentView
{
    public class FogView : EnvironmentView
    {
        [SerializeField] private GameObject Fog;
        [SerializeField] private GameObject Compass;

        protected override void Start()
        {
            Index = -1;
            FogData = GameManager.Instance.Model.FogSlots;
            //base.Start();
            
            /*if (GameManager.Instance.Tutor.CurrentStep < ETutorialStep.MapDiscover || 
                GameManager.Instance.Tutor.CurrentStep > ETutorialStep.MapDiscover)
            {
                GetComponent<BoxCollider2D>().enabled = false;
            }*/
            if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.MapDiscover)
            {
                Debug.Log("GetComponent<PolygonCollider2D>().enabled = true;");
                Compass.SetActive(true);
                GetComponent<PolygonCollider2D>().enabled = true;
                
                //GetComponent<BoxCollider2D>().enabled = true;
            }
            DoSpecialLogic();
        }

        private void Update()
        {
            if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.MapDiscover)
            {
                GetComponent<PolygonCollider2D>().enabled = true;
                Compass.SetActive(true);
                //GetComponent<BoxCollider2D>().enabled = true;
            }
        }

        protected override void DoSpecialLogic()
        {
            FogData.IsDiscoveredProperty
                .Where(isDiscovered =>
                {
                    //Debug.Log($"FogData.IsDiscoveredProperty = {isDiscovered}");
                    return isDiscovered;
                })
                .Subscribe(_ => { OnFogModelDataChanged(); })
                .AddTo(this);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            //GameManager.Instance.Model.GOOnMapSelected = this;
            ActiveDiscoverPanel();
        }

        protected override void ActiveDiscoverPanel()
        {
            MainPanel.gameObject.SetActive(true);
            
            MainPanel.Gold = FogData.FogGoldDiscoverCount;
            MainPanel.Food = FogData.FogFoodDiscoverCount;
            MainPanel.GoldText.text = "" + NumberFormat.ConvertNumberToFormat(FogData.FogGoldDiscoverCount);
            MainPanel.FoodText.text = "" + NumberFormat.ConvertNumberToFormat(FogData.FogFoodDiscoverCount);
            MainPanel.ButtonText.text = "Открыть";
            MainPanel.HeaderText.text = "Новые земли";
            
            GameManager.Instance.Tutor.OnTutorialUIEnable();
        }

        private void OnFogModelDataChanged()
        {
            Fog.gameObject.SetActive(false);
            GetComponent<BoxCollider2D>().enabled = false;
            //GetComponent<PolygonCollider2D>().enabled = false;
            //GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<FogView>().enabled = false;
        }
    }
}