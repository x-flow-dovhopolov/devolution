﻿using Core;
using Core.ModelData;
using Core.ModelData.Environment.EnvironmentTypes;
using Core.Tutorial;
using UI.Environment;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects.EnvironmentView
{
    public class TravelerView : EnvironmentView
    {
        public TravelerDiscover TravelerMainPanel;
        public GameObject QuestMarker;
        
        protected override void Start()
        {
            base.Start();
            
            DoSpecialLogic();
            
            TravelerMainPanel = MainPanel as TravelerDiscover;
        }
        
        private void Update()
        {
            if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.HelpTraveler)
            {
                GetComponent<PolygonCollider2D>().enabled = true;
                QuestMarker.gameObject.SetActive(true);
                //GetComponent<BoxCollider2D>().enabled = true;
            }
        }
        
        public override void OnPointerClick(PointerEventData eventData)
        {
            if (!FogData.fogSlot[0].IsPassed.Value) return;

            base.OnPointerClick(eventData);
        }

        protected override void ActiveDiscoverPanel()
        {
            TravelerMainPanel.gameObject.SetActive(true);

            var travel = Slot as TravelerSlot;
            TravelerMainPanel.SetRequiredItems(travel.requiredItemslist);
            
            /*is this case GoldText is Reward text*/
            TravelerMainPanel.Reward = 3000;
            TravelerMainPanel.Food = Slot.FoodDiscoverCount;
            TravelerMainPanel.GoldText.text = "" + NumberFormat.ConvertNumberToFormat(3000);
            TravelerMainPanel.FoodText.text = "" + NumberFormat.ConvertNumberToFormat(Slot.FoodDiscoverCount);

            GameManager.Instance.Tutor.OnTutorialUIEnable();
        }
        
        protected override void DoBuildingBusinessLogic()
        {
            gameObject.SetActive(false);
            GetComponent<PolygonCollider2D>().enabled = false;
            enabled = false;
        }
    }
}