﻿using System.Collections.Generic;
using UnityEngine;

namespace GameObjects
{
    public class ImageDatabase : MonoBehaviour
    {
        [SerializeField] public List<Sprite> MillSpriteList;
        [SerializeField] public List<Sprite> FarmSpriteList;
        [SerializeField] public List<Sprite> BarrackSpriteList;

        public Sprite Find(string type, int level)
        {
            Sprite sprite = null;
            if (level < 1) return sprite;
            
            //TODO этот свитч еще в двух местах
            switch (type)
            {
                case "Mill" : 
                    sprite = MillSpriteList[level - 1];
                    break;
                case "Farm" : 
                    sprite = FarmSpriteList[level - 1];
                    break;
                case "Barrack" : 
                    sprite = BarrackSpriteList[level - 1];
                    break;
                default: 
                    break;
            }

            return sprite;
        }
    }
}