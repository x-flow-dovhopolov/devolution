﻿using Core;
using Core.ModelData;
using UI;
using UI.CurrencyBuildings.SceneSupport.Builder;
using UI.Parallax;
using UI.Parallax.NewParallax;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects
{
    public sealed class SlotView :  View
    {
        public int Index;
        public Slot Slot;
        [SerializeField] private TextMesh BuildingLevel;
        [SerializeField] private SpriteRenderer BuildingSpriteRenderer;
        [SerializeField] private GameObject BuiltState;
        [SerializeField] private GameObject UnBuiltState;
        [SerializeField] public LocationGuildingUIView CastleUIScript;
        
        private void Start()
        {
            Slot = GameManager.Instance.Model.SlotsList[Index];
            
            //GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(Slot.Building.Value.Image);
            //BuildingSpriteRenderer.sprite = null;
            
            Slot.Building.
                Where(_ => Slot.Building.Value.BuildingType != BuildingType.Default)
                .Subscribe(x =>
                {
                    BuiltState.SetActive(true);
                    UnBuiltState.SetActive(false);
                    //BuildingSpriteRenderer.sprite = Resources.Load<Sprite>(x.AdventureSceneImage);
                    if (Slot.Building.Value.BuildingType != BuildingType.Tavern)
                    {
                        InitBuildingUiElement();
                       //BuildingLevel.GetComponent<TextMesh>().text = "" + Slot.Building.Value.BuildingLevelProperty;
                    }
                })
                .AddTo(this);
            
            /*
            if (Slot.Building.Value.BuildingType == BuildingType.Barracks)
            {
                transform.localScale = new Vector3(0.75f, 0.75f, 1);
            }*/
        }
        

        private void InitBuildingUiElement()
        {
            CastleUIScript.gameObject.SetActive(true);
            CastleUIScript.SetMarkEnable(false);
            CastleUIScript.SetBuildingName(Slot.Building.Value.Name);
            CastleUIScript.SetBuildingLevel(Slot.Building.Value.BuildingLevelProperty);
            Slot.Building.Value.LevelIncreased.Subscribe(level => CastleUIScript.SetBuildingLevel(level)).AddTo(gameObject);
        }
        
        public override void OnPointerClick(PointerEventData eventData)
        {
            return;
            GameManager.Instance.Model.OpenedBuildingIndex = Index;
            
            if (Slot.Building.Value.BuildingType == BuildingType.Default)
            {
                GainBuildingCreateControl();
            }
            else
            {
                GameManager.Instance.GoToAnotherScene(Slot.Building.Value.BuildingType);
            }
        }

        public void GainBuildingCreateControl()
        {
            GameManager.Instance.DestroyTutorialMask();
            GameManager.Instance.BuildMenuPanel.SetActive(true);
            //GameManager.Instance.BuildMenuPanel.GetComponent<BuilderPanelView>().SetBuilderItemsEnable();
            // Костыли
            //var go = GameObject.Find("Space for improve");
            var go = GameObject.Find("Space for impove NewYearVersion");
            go.GetComponent<BuilderPanelViewTest>().SetBuilderItemsEnable();
        }
    }
}
