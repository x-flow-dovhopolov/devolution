﻿using UI.Parallax.NewParallax;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameObjects
{
    public abstract class View : MonoBehaviour, IPointerClickHandler
    {
        public virtual void OnPointerClick(PointerEventData eventData)
        {
            
        }
    }
}