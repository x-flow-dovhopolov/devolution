﻿using UniRx;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MixerLink : MonoBehaviour
{
    public AudioMixer mixer;
    public string mixerParameter;

    public float maxAttenuation = 0.0f;
    public float minAttenuation = -80.0f;
    
    public void SliderValueChange(int value, bool isMute)
    {
        mixer.SetFloat(mixerParameter, value);
        gameObject.GetComponent<BackgroundMusicPlayer>().MusicIsMute = isMute;
    }
}