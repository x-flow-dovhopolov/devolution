using System;
using System.Collections.Generic;
using System.Globalization;

public class NumberFormat
{
    private static string[] _formats = {"N3", "N2", "N0"};

    public static string ConvertNumberToFormat(double number)
    {
        int length = GetNumberLength(number);

        string result;

        if (length < 5)
        {
            // 999 without fractional part
            result = number.ToString("N0", CultureInfo.InvariantCulture);

            // 9 999 insert space
            if (length == 4)
            {
                if (result[1] == ',') // if 9,999 => replace to space 
                {
                    result = result.Replace(',', ' ');
                }

                return result; //.Insert(1, " ");
            }

            return result;
        }

        //length > 4
        // 99.99k 999k 9 999k 99.99M ....  with letter

        var roundNumber = (number / Math.Pow(10, (length - 1) / 3 * 3));
        var roundNumberLength = GetNumberLength(roundNumber); // 1 => 9,999    2 => 99,99    3 => 999

        result = roundNumber.ToString(_formats[roundNumberLength - 1], CultureInfo.InvariantCulture);

        if (result[1] == '.') // if 9,999 => replace to space 
        {
            result = result.Replace('.', ' ');
        }

        return $"{result}{ConvertNumberToName(length, true)}";
    }

    private static int GetNumberLength(double number)
    {
        return number < 1 ? 1 : (int) Math.Floor(Math.Log10(Math.Abs(number)) + 1);
    }

    private static string ConvertNumberToName(int numberLength, bool isShortName)
    {
        var cashFormats = new Dictionary<int, string>();
        cashFormats.Add(0, "K");
        cashFormats.Add(1, "M");
        cashFormats.Add(2, "B");

        int index = ((numberLength - 2) / 3) - 1;

        if (index >= 0)
        {
            int elementCount = cashFormats.Count;

            if (index >= elementCount) return cashFormats[elementCount - 1];

            return cashFormats[index];
        }

        return "";
    }
}