﻿using System.Linq;
using DG.Tweening;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

namespace Tavern
{
    public class Game : MonoBehaviour, IPointerClickHandler
    {
        public Vector3 position;
        public Vector3 startPosition;
        public bool isOpened;
        public GameObject go;      
        public GameObject reward; 

        public TavernUI TavernUi;
        
        private void Start()
        {
            TavernUi = FindObjectsOfType<Canvas>().First(c => c.gameObject.name == "Canvas").GetComponent<TavernUI>();
        }

        public void MySequence()
        {
            var mySequence = DOTween.Sequence();
            var x1 = -4.75f;
            var x2 = 6f;
            mySequence.
                Append(transform.DOJump(position, 2f, (int)Random.Range(1f, 2f), 0.75f, false).
                    OnComplete(HideRewards))
                .Append(transform.DOMoveX(Random.Range(x1, x2), 0.5f))
                .Append(transform.DOMoveX(Random.Range(x1, x2), 0.5f))
                .Append(transform.DOMoveX(Random.Range(x1, x2), 0.5f))
                .Append(transform.DOMoveX(Random.Range(x1, x2), 0.5f))
                .Append(transform.DOMoveX(Random.Range(x1, x2), 0.5f))
                .Append(transform.DOMoveX(Random.Range(x1, x2), 0.5f))
                .PrependInterval(1)
                .Append(transform.DOMoveX(position.x, 1))
                .Append(transform.DOScale(new Vector3(1.75f,1.75f,1.75f), 1f))
                .Append(transform.DOScale(new Vector3(1,1,1), 0.1f)).OnComplete(Shock);
        }

        private void HideRewards() => reward.GetComponent<SpriteRenderer>().enabled = false;

        private void EnableBoxCollider2d() => GetComponent<BoxCollider2D>().enabled = true;
        
        private void Shock()
        {
            EnableBoxCollider2d();
            
            //if (go == null) return;
            //go.transform.DOShakeScale(1, new Vector3(0.25f, 0.25f, 1), 10, 90f, true);
        }


        public void OnPointerClick(PointerEventData eventData)
        {
            isOpened = true;
            TavernUi.Open(gameObject);
        }

        public void OpenAfterEffect()
        {
            isOpened = false;
            reward.GetComponent<SpriteRenderer>().enabled = true;
            
            if (isOpened) return;

            transform.DOMoveY(1f, 1f);
        }
    }
}
