﻿using Core;
using Core.Tutorial;
using DG.Tweening;
using UI.Environment;
using UI.LoadingScreen;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    // ReSharper disable once InconsistentNaming
    public class AdventureMainUI : MonoBehaviour
    {
        public static AdventureMainUI Instance;
        public Canvas Canvas;
        
        [SerializeField] public GameObject Hint;
        [SerializeField] public GameObject StockPanel;
        [SerializeField] public GameObject NotEnoughMoney;
        [SerializeField] public GameObject Building;
        [SerializeField] public GameObject SelectedPrefab;
        [SerializeField] public Button Cheat;
        [SerializeField] public GameObject ScreenLoader;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) 
                Destroy(gameObject);
        }

        private void Start()
        {
            //GameManager.Instance.BuildMenuPanel = GameObject.Find("Builder");
            GameManager.Instance.BuildMenuPanel = GameObject.Find("General window PBuilder");
            GameManager.Instance.BuildMenuPanel.SetActive(false);
            
            ScreenLoader.GetComponent<LoadingScreen.LoadingScreen>().SetSceneLoader(new SceneLoader());
            /*GameManager.Instance.SceneLoader = GameObject.Find("Loading Screen");
            GameManager.Instance.SceneLoader.SetActive(false);*/
            
            /*Cheat.OnClickAsObservable().Subscribe(_ =>
            {
                GameManager.Instance.Model.Gold.Value += 100000;
                GameManager.Instance.Model.Food.Value += 100000;
                
            });*/
        }

        public void DisableUiElementsAndActiveStock()
        {
            if (SelectedPrefab != null) SelectedPrefab.SetActive(false);

            SelectedPrefab = StockPanel;
            
            Canvas.GetComponent<CanvasGroup>().interactable = false;
            
            SelectedPrefab.SetActive(true);
            GameObject.Find("StockPile").GetComponent<StockUI>().InitItemsOnCanvas();
            //StockUI.InitItemsOnCanvas();
        }

        public void ShowNotEnoughMoneyPrefab(string description)
        {
            var go = Instantiate(NotEnoughMoney, Canvas.transform, false);
            go.GetComponent<NotEnoughMoneyView>().SetText(description);
            Destroy(go, 1f);
        }

        public void TutorialMasKEnable(int index)
        {
            Canvas.GetComponent<CanvasGroup>().interactable = false;

            var built = Building.GetComponent<AdventureSceneTutorialHelper>();
            built.DoByItselfLogic(false, index);

            GameManager.Instance.CreateTutorialMask(built.LighthouseList[index], 3f);
        }

        public void ReenableMainCanvas()
        {
            Canvas.GetComponent<CanvasGroup>().interactable = true;
            Building.GetComponent<AdventureSceneTutorialHelper>().DoByItselfLogic(true);
        }

        public void MoveCameraToCaptureTombStep()
        {
            var gragManager = GameObject.Find("Main Camera");
            
            if (gragManager != null)
            {
                gragManager.transform.DOMoveX(7.42f, 1f).OnComplete(() =>
                {
                    //TutorialMasKEnable(10);
                });
            }
            else
            {
                //TutorialMasKEnable(10);
            }
        }
        
        public void MoveCameraOnBuyTavernStep()
        {
            var gragManager = GameObject.Find("Main Camera");
            
            if (gragManager != null)
            {
                gragManager.transform.DOMoveX(7.42f, 1f).OnComplete(() =>
                {
                    TutorialMasKEnable(10);
                });
            }
            else
            {
                TutorialMasKEnable(10);
            }
        }
    }
}