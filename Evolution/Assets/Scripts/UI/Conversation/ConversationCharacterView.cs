﻿using Core.Conversation;
using UnityEngine;

namespace UI.Conversation
{
    public class ConversationCharacterView : MonoBehaviour
    {
        [SerializeField] private GameObject loras = default;
        [SerializeField] private GameObject jafar = default;
        [SerializeField] private GameObject camilla = default;

        public void SetData(ConversationCharacter character)
        {
            loras.SetActive(character == ConversationCharacter.Loras);
            jafar.SetActive(character == ConversationCharacter.Jafar);
            camilla.SetActive(character == ConversationCharacter.Camilla);
        }
    }
}