﻿using Core.Conversation;
using TMPro;
using UnityEngine;

namespace UI.Conversation
{
    public class ConversationPanelView : MonoBehaviour
    {
        [SerializeField] private ConversationCharacterView primaryCharacterView = default;
        [SerializeField] private ConversationCharacterView secondaryCharacterView = default;
        [SerializeField] private TextMeshProUGUI textField;

        public void SetData(ConversationLine conversationLine)
        {
            textField.text = conversationLine.Message;
            
            primaryCharacterView.gameObject.SetActive(conversationLine.PrimaryCharacter != ConversationCharacter.None);
            
            secondaryCharacterView.gameObject.SetActive(conversationLine.SecondaryCharacter != ConversationCharacter.None);
            
            if (conversationLine.PrimaryCharacter != ConversationCharacter.None)
                primaryCharacterView.SetData(conversationLine.PrimaryCharacter);
            
            if (conversationLine.SecondaryCharacter != ConversationCharacter.None)
                secondaryCharacterView.SetData(conversationLine.SecondaryCharacter);
        }
    }
}