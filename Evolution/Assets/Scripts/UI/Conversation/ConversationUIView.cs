using Core.Conversation;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Conversation
{
    public class ConversationUIView : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private ConversationPanelView LeftConversationPanel = default;
        [SerializeField] private ConversationPanelView RightConversationPanel = default;

        public Subject<Unit> ClickStream = new Subject<Unit>();

        public void SetData(ConversationLine conversationLine)
        {
            if (conversationLine.Side == ConversationSide.Left)
            {
                RightConversationPanel.gameObject.SetActive(false);
                LeftConversationPanel.SetData(conversationLine);
            }
            else if (conversationLine.Side == ConversationSide.Right)
            {
                LeftConversationPanel.gameObject.SetActive(false);
                RightConversationPanel.SetData(conversationLine);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            ClickStream.OnNext(Unit.Default);
        }
    }
}