﻿using DG.Tweening;
using UnityEngine;

namespace UI.CurrencyBuildings
{
    public class ArrowMono : MonoBehaviour
    {
        private GameObject target;
        private Canvas _canvas;
        private Vector2 _targetScreenPosition;
        private Vector2 _center;

        public void SetTarget(GameObject obj, float fromX, float fromY)
        {
            target = obj;

            transform.DOKill();

            if (_canvas == null)
            {
                _canvas = GameObject.FindObjectOfType<Canvas>();
            }

            _center = new Vector2(Screen.width * fromX, Screen.height * fromY);
            transform.position = _center;

            _targetScreenPosition = RectTransformUtility.PixelAdjustPoint(target.transform.position, target.transform, _canvas);
            Vector2 vec = _targetScreenPosition - _center;
            transform.up = vec;

            transform.DOMove(_targetScreenPosition, 1f).SetEase(Ease.OutBounce);
            //Debug.Log();
        }
    }
}