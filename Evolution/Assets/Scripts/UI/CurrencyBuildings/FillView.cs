﻿using System.Collections;
using Core;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.CurrencyBuildings
{
    public class FillView : MonoBehaviour
    {
        public Image IncomeProgress;
        public float TimeLeftToGetIncome;

        private void Start()
        {
            if (GameManager.Instance.Model.CurrentBuilding.IsSpawning)
            {
                var var = Mathf.Abs((GameManager.Instance.Model.CurrentBuilding.GetBuildingSpawnTime() -
                           GameManager.Instance.Model.CurrentBuilding.CreateTime) /
                          GameManager.Instance.Model.CurrentBuilding.GetBuildingSpawnTime());
                Debug.Log($"GetBuildingSpawnTime() = {GameManager.Instance.Model.CurrentBuilding.GetBuildingSpawnTime()}" +
                          $", CreateTime = {GameManager.Instance.Model.CurrentBuilding.CreateTime}" +
                          $" var {var}");
                IncomeProgress.fillAmount = var;
                //время игры
                // время когда создасться
                // время сколько всего создается
                
                Debug.Log($".CreateTime = {GameManager.Instance.Model.CurrentBuilding.CreateTime}" +
                          $" б Time.deltaTime = {Time.deltaTime}");
                StartCoroutine(Fill(
                    GameManager.Instance.Model.CurrentBuilding.CreateTime - Time.time, 
                    var));
            }

            GameManager.Instance.Model.CurrentBuilding.OnStartSpawn.Subscribe(StartFillBarProcess).AddTo(gameObject);
        }

        private void StartFillBarProcess(float spawnTime) => StartCoroutine(Fill(spawnTime));

        private IEnumerator Fill(float spawnTime, float delta = 0f)
        {
            float timeToEarn = Mathf.Approximately(TimeLeftToGetIncome, 0) ? spawnTime : 0f;

            float time = GameManager.Instance.Model.CurrentBuilding.GetBuildingSpawnTime(); ;//spawnTime;
            
            /*Debug.Log($"spawnTime = {spawnTime} /// timeToEarn = {timeToEarn} /// time = {time}");
            Debug.Break();*/
            
            while (timeToEarn > 0)
            {
                timeToEarn -= Time.deltaTime;

                var tmp = Mathf.Abs(timeToEarn / time - 1);
                float progress;

                if (delta != 0f)
                {
                    progress = tmp;
                }
                else
                {
                    progress = tmp;
                }
                //Debug.Log($"time {timeToEarn}  ///// tmp = {tmp}");
                IncomeProgress.fillAmount = progress;
                yield return null;
            }

            TimeLeftToGetIncome = 0;
            IncomeProgress.fillAmount = TimeLeftToGetIncome;
        }
    }
}