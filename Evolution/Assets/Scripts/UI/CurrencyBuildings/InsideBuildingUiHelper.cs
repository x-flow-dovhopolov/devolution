﻿using Core;
using Core.Tutorial;
using UI.CurrencyBuildings.SceneSupport;
using UI.CurrencyBuildings.SceneSupport.SupportHeadView;
using UniRx;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

namespace UI.CurrencyBuildings
{
    public class InsideBuildingUiHelper : MonoBehaviour
    {
        public static InsideBuildingUiHelper Instance;
        private GameObject selectedPrefab;

        [SerializeField] public GameObject Hint;
        [SerializeField] private GameObject Canvas;
        [SerializeField] private GameObject BackButton;
        [SerializeField] private GameObject NewCreature;
        [SerializeField] private GameObject NotEnoughMoneyText;
        
        [SerializeField] private GameObject ManagerPanel;
        [SerializeField] private BoxCollider2D BuildingBoxCollider2D;
        [SerializeField] private ShopButtonView ShopButtonView;

        [SerializeField] private GameObject RedStripe;
        private GameObject createdReStripe;
        private Canvas CanvasShaders;
        
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) 
                Destroy(gameObject);
        }

        private void Start()
        {
            GameManager.Instance.Model.NpcDiscoverManager.NewNpcJustDiscovered
                .Subscribe(data => ShowNewCreature(data.Item1, data.Item2, data.Item3))
                .AddTo(this);

            //ShopButtonView.CheckIsEnable();
            CanvasShaders = GetComponent<Canvas>();
        }

        private void Update()
        {
            return;
            if (CanvasShaders.additionalShaderChannels != AdditionalCanvasShaderChannels.None)
            {
                CanvasShaders.additionalShaderChannels = AdditionalCanvasShaderChannels.None;
            }
            Debug.Log($"{CanvasShaders.additionalShaderChannels}");
        }
        public void OpenManagerPanel()
        {
            ManagerPanel.GetComponent<BuildingManagersView>().SetManagerData();
            DisableUiElementsAndActiveSecondaryPanel(ManagerPanel);
        }

        public void EnableUiElements()
        {
            selectedPrefab.SetActive(false);

            Canvas.GetComponent<CanvasGroup>().interactable = true;
            BuildingBoxCollider2D.enabled = true;
        }

        public void DisableUiElementsAndActiveSecondaryPanel(GameObject uiElementToOpen)
        {
            if (selectedPrefab != null) selectedPrefab.SetActive(false);

            selectedPrefab = uiElementToOpen;
            Canvas.GetComponent<CanvasGroup>().interactable = false;
            
            if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.UpdateBuilding)
            {
                BuildingBoxCollider2D.enabled = false;
            }

            selectedPrefab.SetActive(true);
        }
        
        public void InstantiateTutorialRedStripe(Vector3 targetObject, GameObject building)
        {
            var position = new Vector3(targetObject.x - 1f,
                targetObject.y, 0);
            createdReStripe =  Instantiate(RedStripe, position, Quaternion.identity);
            createdReStripe.transform.eulerAngles = new Vector3(0f,0f, 90f);
            //createdReStripe.GetComponent<ArrowMono>().SetTarget(building, 0.5f, 0.5f);
            createdReStripe.SetActive(true);
        }

        public void DestroyRedStripe()
        {
            if(createdReStripe != null)
                Destroy(createdReStripe.gameObject);
        }

        public void ShowErrorPrefab(string description)
        {
            var go = Instantiate(NotEnoughMoneyText, Canvas.transform, false);
            go.GetComponent<NotEnoughMoneyView>().SetText(description);
            Destroy(go, 1f);
        }

        private void ShowNewCreature(int level, Sprite image, string npcName)
        {
            var go = Instantiate(NewCreature, Canvas.transform, false);
            go.GetComponent<NewCreatureView>().SetData(level, image, npcName);
        }

        public void TurnInteractableCanvas(bool isEnable) => Canvas.GetComponent<CanvasGroup>().interactable = isEnable;

        public void SetBackToMainSceneButtonEnable(bool isEnable) => BackButton.gameObject.SetActive(isEnable);
    }
}