﻿using System.Collections.Generic;
using Core;
using Core.ModelData.Buildings;
using TMPro;
using UniRx;
using UnityEngine;

namespace UI.CurrencyBuildings.SceneSupport.Builder
{
    public class BuilderPanelView : BuildingUiSupportView
    {
        [SerializeField] protected TextMeshProUGUI PanelNameText;
        [SerializeField] private List<SingleBuildingView> buildingsContent;
        [SerializeField] private List<Sprite> buildingImage;
        private List<bool> buildingEnableList;
        protected new void Start()
        {
            PanelNameText.text = LocaleRU.Generals.Find("builder").Value;
            
            CloseBtn.OnClickAsObservable().Subscribe(_ =>
            {
                gameObject.SetActive(false);
            });

            SetItemsList();
        }
        
        public override void SetItemsList()
        {
            Building mill = new Mill();
            buildingsContent[0].SetSingleData(mill, buildingImage[0]);
            
            Building farm = new Farm();
            buildingsContent[1].SetSingleData(farm, buildingImage[1]);
            
            Building academy = new Barracks();
            buildingsContent[2].SetSingleData(academy, buildingImage[2]);
            
            Building tavern = new Core.ModelData.Buildings.SupportBuildings.Tavern();
            buildingsContent[3].SetSingleData(tavern, buildingImage[3]);
        }

        public void SetBuilderItemsEnable()
        {
            buildingEnableList = GameManager.Instance.Tutor.SetBuilderItems();
            
            buildingsContent[0].SetItemEnable(buildingEnableList[0]);
            buildingsContent[1].SetItemEnable(buildingEnableList[1]);
            buildingsContent[2].SetItemEnable(buildingEnableList[2]);
            buildingsContent[3].SetItemEnable(buildingEnableList[3]);
        }
    }
}
