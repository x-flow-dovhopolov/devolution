﻿using Core;
using Core.ModelData.Buildings;
using Core.ModelData.Buildings.SupportBuildings;
using UI.Parallax;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.CurrencyBuildings.SceneSupport.Builder
{
    public class SingleBuildingView : SingleSupportItemView
    {
        [SerializeField] public Image ItemImage;
        [SerializeField] private GeneralButtonScript generalButtonScript;
        private BuildingType BuildingType;
        
        protected new void Start()
        {
            //base.Start();
        }
        
        public void SetSingleData(Building building, Sprite buildingSprite)
        {
            Name.text = building.Name;
            
            generalButtonScript.SetButtonData("Купить", 
                NumberFormat.ConvertNumberToFormat(building.BuildingPriceProperty),
                NumberFormat.ConvertNumberToFormat(building.BuildingFoodPriceProperty));
            
            /*TODO refactor. если не сетить значения то BuyBuilding() не будет работать правильно*/
            Gold.Value = building.BuildingPriceProperty;
            Food.Value = building.BuildingFoodPriceProperty;

            BuildingType = building.BuildingType;
            ItemImage.sprite = buildingSprite;
        }

        public void SetItemEnable(bool isButtonEnable)
        {
            SetBuildButtonEnable(isButtonEnable);
        }
        
        private void SetBuildButtonEnable(bool isEnable)
        {
            //Action.GetComponent<Button>().interactable = isEnable;
            generalButtonScript.GeneralButton.interactable = isEnable;
            
            if (isEnable)
            {
                //generalButtonScript.SetButtonOnClickAction(BuyBuilding);
                Action.OnClickAsObservable().Subscribe(_ => BuyBuilding());
            }
        }
        
        private void BuyBuilding()
        {
            Building building = null;
            
            if (BuyHouse(Gold.Value, Food.Value))
            {
                building = BuildingFactory.CreateBuilding(BuildingType);
                building.ActivateSupportBuilding();
                GameManager.Instance.Tutor.SetBuildingStep();
                Debug.Log($"Bought BuildingType = {building.BuildingType}");
            }
            
            if (building != null)
            {
                GameManager.Instance.Model.CurrentBuilding = building;
                GameManager.Instance.Tutor.Context._state.OnCurrencyBuildingBought.OnNext(Unit.Default);
            }
            
            GameManager.Instance.BuildMenuPanel.SetActive(false);
        }
        
        private bool BuyHouse(int gold, int food)
        {
            if (GameManager.Instance.Model.Gold.Value >= gold 
                && GameManager.Instance.Model.Food.Value >= food)
            {
                GameManager.Instance.DeductPurchasePrice(gold, food, 0);
                return true;
            }

            //TODO add to locale
            AdventureMainUI.Instance.ShowNotEnoughMoneyPrefab("Not enough money to Buy House");
            
            return false;
        }

    }
}
