﻿using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.CurrencyBuildings.SceneSupport
{
    public abstract class BuildingUiSupportView : MonoBehaviour
    {
        protected List<SingleSupportItemView> itemsList = new List<SingleSupportItemView>();
        protected SingleSupportItemView singleView;
        
        [SerializeField] protected TextMeshProUGUI HeaderText;
        [SerializeField] protected Button CloseBtn;
        [SerializeField] protected GameObject Canvas;
        [SerializeField] protected GameObject SingleUiObject;
        [SerializeField] protected GameObject Content;

        
        protected void Start()
        {
            CloseBtn.OnClickAsObservable().Subscribe(_ =>
            {
                ClearUpgradeList();
                gameObject.SetActive(false);
            });
        }
        
        public abstract void SetItemsList();
        protected void BakeSingleViewForItem() => singleView = CreateSingleListItem();

        protected SingleSupportItemView CreateSingleListItem()
        {
            var singleObj = Instantiate(SingleUiObject, Canvas.transform);
            singleObj.transform.SetParent(Content.transform);
            return singleObj.GetComponent<SingleSupportItemView>();
        }

        protected void ClearUpgradeList()
        {
            foreach (var singleImprove in itemsList)
            {
                if (singleImprove != null) Destroy(singleImprove.gameObject);
            }
            itemsList.Clear();
        }
    }
}