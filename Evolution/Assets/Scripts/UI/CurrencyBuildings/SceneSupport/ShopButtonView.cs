﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Tutorial;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;

namespace UI.CurrencyBuildings.SceneSupport
{
    public class ShopButtonView : MonoBehaviour
    {
        [SerializeField] public GameObject Shop;
        private int discoveredCount;
        private Button btn;
        public Subject<Unit> NpcLibraryCnt = new Subject<Unit>();
        private void Start()
        {
            gameObject.GetComponent<Button>().OnClickAsObservable().Subscribe(_ => StartShop());
            
           // SetItselfEnableWhenNpcCntMore3AndTutorialUseShopStep();
            
            /*GameManager.Instance.Model.NpcDiscoverManager.TotalItemCount
                .Where(cnt => cnt >= 3)
                .Subscribe( _ =>
                {
                    NpcLibraryCnt.OnNext(Unit.Default);
                    gameObject.GetComponent<Button>().interactable = true;
                }).AddTo(this);*/

            /*if (GameManager.Instance.Tutor.CurrentStep < ETutorialStep.UseShop)
            {
                gameObject.GetComponent<Button>().interactable = false;
            }*/
            btn = gameObject.GetComponent<Button>();
        }

        private void Update()
        {
            if (GameManager.Instance.Tutor.CurrentStep < ETutorialStep.UseShop)
            {
                btn.interactable = false;
            }
            else
            {
                btn.interactable = true;
            }
        }

        private void SetItselfEnableWhenNpcCntMore3AndTutorialUseShopStep()
        {
            var observablesListOfCheckMethods = new List<IObservable<UniTask<bool>>>();
            observablesListOfCheckMethods.Add(IsEnoughIObservable());
            observablesListOfCheckMethods.Add(IsEnoughIObservable2());
            
            observablesListOfCheckMethods.WhenAll()
                .ObserveOnMainThread()
                .Subscribe(xs =>
                {
                    var result = xs.All(x => true);
                    Debug.Log($"{xs.Length} = result => {result}");
                    
                    if (result)
                    {
                        gameObject.GetComponent<Button>().interactable = true;
                    }
                });
        }
        private IObservable<UniTask<bool>> IsEnoughIObservable() => Observable.Start(IsUseShopStep);
        private IObservable<UniTask<bool>> IsEnoughIObservable2() => Observable.Start(IsNpcCntMore3);
        
        private async UniTask<bool> IsUseShopStep()
        {
            await GameManager.Instance.Tutor.Context._state.OnUseShopStepStarted.First();
            Debug.Log(" IsUseShopStep()");
            return true;
        }
        private async UniTask<bool> IsNpcCntMore3()
        {
            await NpcLibraryCnt.First();
            Debug.Log(" IsNpcCntMore3()");
            return true;
        }
        
        public void CheckIsEnable()
        {
            switch (GameManager.Instance.Model.CurrentBuilding.BuildingType)
            {
                case BuildingType.Mill:
                    discoveredCount = GameManager.Instance.Model.NpcDiscoverManager.MillCreatures.Count;
                    break;
                case BuildingType.Farm:
                    discoveredCount = GameManager.Instance.Model.NpcDiscoverManager.FarmCreatures.Count;
                    break;
                case BuildingType.Barracks:
                    discoveredCount = GameManager.Instance.Model.NpcDiscoverManager.BarrackCreatures.Count;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Debug.Log($"discoveredCount {discoveredCount}");
            SetItselfEnable(discoveredCount);
        }

        private void SetItselfEnable(int count)
        {
            //gameObject.SetActive(count > 2);
            if (count >= 3)
            {
                gameObject.GetComponent<Button>().interactable = true;
            }
        }
        
        private void StartShop()
        {
            GameManager.Instance.DestroyTutorialMask();
            Shop.gameObject.SetActive(true);
        }
    }
}
