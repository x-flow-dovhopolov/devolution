﻿using Core;
using Core.ModelData.Buildings.BuildingUpgrades;
using Core.ModelData.ExceptionHierarchy;
using Core.Tutorial;
using UniRx;

namespace UI.CurrencyBuildings.SceneSupport.SingleItems
{
    public class SingleBuildingUpgradeView : SingleSupportItemView
    {
        private IUpgrade upgrade;
        private Item BuildingUpgradeItem;
        private new void Start()
        {
            base.Start();
            Action.OnClickAsObservable().Subscribe(_ => UpdateBuilding());
        }

        public override void SetSingleData(Item item)
        {
            BuildingUpgradeItem = item;
            base.SetSingleData(item);
            upgrade =  item as IUpgrade;
            
            if((item is IncreaseMaxNpcOnScene || item is IncreaseSpawnedNpcLevel) &&
               GameManager.Instance.Tutor.CurrentStep == ETutorialStep.UpdateBuilding)
            {
                Action.interactable = false;
            }
        }
        
        private void UpdateBuilding()
        {
            try
            {
                upgrade.Upgrade();
                /*if upgrade success refresh cost and description*/
                CalculateMutableValues(BuildingUpgradeItem);
            }
            catch (AdventureException e)
            {
                InsideBuildingUiHelper.Instance.ShowErrorPrefab(e.Description);
            }
        }
    }
}
