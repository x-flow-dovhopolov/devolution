﻿using System;
using Core;
using Core.ModelData.Buildings.BuildingUpgrades;
using Core.ModelData.Buildings.Managers;
using Core.ModelData.ExceptionHierarchy;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.CurrencyBuildings.SceneSupport.SingleItems
{
    public class SingleManagerView : SingleSupportItemView
    {
        [SerializeField] private Image ManagerImage;
        private ManagerItemData ManagerItemData;
        private new void Start()
        {
            base.Start();
            Action.OnClickAsObservable().Subscribe(_ => BuyManager());
        }
        
        public override void SetSingleData(ManagerItemData item)
        {
            ManagerItemData = item;
            ManagerImage.sprite = Resources.Load<Sprite>(item.ManagerImage);
            base.SetSingleData(item as Item);
        }

        private void BuyManager()
        {
            try
            {
                GameManager.Instance.DeductPurchasePrice(Gold.Value, Food.Value, 0);
                
                GameManager.Instance.Model.CurrentBuilding.SetManager(ManagerItemData.managerType);

                InsideBuildingUiHelper.Instance.EnableUiElements();
                
                GameManager.Instance.Tutor.Context._state.OnTutorialManagerBought.OnNext(Unit.Default);
                /*if (!GameManager.Instance.Tutor.IsTutorialManagerPlayed)
                {
                    GameManager.Instance.Tutor.Context._state.OnTutorialManagerBought.OnNext(Unit.Default);
                }*/
            }
            catch (AdventureException e)
            {
                InsideBuildingUiHelper.Instance.ShowErrorPrefab("Недостаточно ресурсов");
            }
        }
    }
}