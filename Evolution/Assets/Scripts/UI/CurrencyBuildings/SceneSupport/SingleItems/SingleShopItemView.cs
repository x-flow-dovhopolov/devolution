﻿using Core.ModelData.Buildings.NpcModel;
using Core.ModelData.ExceptionHierarchy;
using Core.ModelData.NpcDiscover;
using UI.CurrencyBuildings.SceneSupport.SupportHeadView;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.CurrencyBuildings.SceneSupport.SingleItems
{
    public class SingleShopItemView : SingleSupportItemView
    {
        [SerializeField] private Image ItemImage;
        
        private ShopManagerView ShopManagerView;
        private int level;
        
        private new void Start()
        {
            base.Start();
            Action.OnClickAsObservable().Subscribe(_ => BuyItem());
        }

        public void SetShopDataWithSprite(SingleNpcDiscover<NPC> item, ShopManagerView shopManagerView)
        {
            Debug.Log($"level = {level} // {item.Sprite}");
            SetData(item);
            level = item.Level;
            ItemImage.sprite = item.Sprite;
            ShopManagerView = shopManagerView;
        }

        private void BuyItem()
        {
            try
            {
                ShopManagerView.Buy(Gold.Value, Food.Value, level);
            }
            catch (AdventureException e)
            {
                InsideBuildingUiHelper.Instance.ShowErrorPrefab(e.Description);
            }
        }
    }
}