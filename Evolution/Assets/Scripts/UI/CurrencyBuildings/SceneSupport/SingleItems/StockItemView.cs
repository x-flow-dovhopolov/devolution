﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.CurrencyBuildings.SceneSupport.SingleItems
{
    public class StockItemView : MonoBehaviour
    {
        [SerializeField] private Image ItemImage;
        [SerializeField] private TextMeshProUGUI CountText;

        public void SetItemImage(Sprite image)
        {
            ItemImage.gameObject.SetActive(true);
            ItemImage.sprite = image;
        }

        public void SetItemCount(int count) => CountText.text = count + "";
    }
}
