﻿using Core.ModelData.Buildings.BuildingUpgrades;
using Core.ModelData.Buildings.Managers;
using Core.ModelData.Buildings.NpcModel;
using Core.ModelData.NpcDiscover;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.CurrencyBuildings.SceneSupport
{
    public abstract class SingleSupportItemView : MonoBehaviour
    {
        public ReactiveProperty<int> Gold = new ReactiveProperty<int>(0);
        public ReactiveProperty<int> Food = new ReactiveProperty<int>(0);
        
        [SerializeField] protected Button Action;
        [SerializeField] protected TextMeshProUGUI Name;
        [SerializeField] protected TextMeshProUGUI Description;
        [SerializeField] protected TextMeshProUGUI ImproveValue;
        [SerializeField] protected TextMeshProUGUI ItemGoldPrice;
        [SerializeField] protected TextMeshProUGUI ItemFoodPrice;

        protected void Start()
        {
            Gold.Subscribe(x => { ItemGoldPrice.text = NumberFormat.ConvertNumberToFormat(x); }).AddTo(this);
            Food.Subscribe(x => { ItemFoodPrice.text = NumberFormat.ConvertNumberToFormat(x); }).AddTo(this);
        }
        
        public virtual void SetSingleData(Item item)
        {
            Name.text = item.Name;
            Description.text = item.Description;
            CalculateMutableValues(item);
            /*ImproveValue.text = item.CalculateUpgrade();
            Gold.Value = item.CalculateGoldCosts();
            Food.Value = item.CalculateFoodCosts();*/
        }

        protected void CalculateMutableValues(Item item)
        {
            ImproveValue.text = item.CalculateUpgrade();
            Gold.Value = item.CalculateGoldCosts();
            Food.Value = item.CalculateFoodCosts();
//            Debug.Log($"{ImproveValue.text} = {Gold.Value} = {Food.Value}");
        }
        
        public virtual void SetSingleData(ManagerItemData item)
        {
            
        }
        
        protected void SetData(SingleNpcDiscover<NPC> item)
        {
            Name.text = item.Name;;
            Gold.Value = item.Gold;
            Food.Value = item.Food;
        }
    }
}