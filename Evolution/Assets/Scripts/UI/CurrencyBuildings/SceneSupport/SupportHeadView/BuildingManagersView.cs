﻿using System;
using Core;
using Core.ModelData.Buildings.Managers;
using Core.ModelData.Buildings.Managers.VariousManagers;
using Core.ModelData.Buildings.Managers.VariousManagers.ManagerSupportData;
using UniRx;

namespace UI.CurrencyBuildings.SceneSupport.SupportHeadView
{
    public class BuildingManagersView : BuildingUiSupportView
    {
        private ManagerItemData managerItemData;
        private new void Start()
        {
            base.Start();
        }

        public void SetManagerData() => SetSingleManagerViewIntoManagerPanel(); //SetItemsList();
        
        public override void SetItemsList()
        {
            BakeSingleViewForItem();
            
            singleView.SetSingleData(managerItemData);
            itemsList.Add(singleView);
        }
        
        private void SetSingleManagerViewIntoManagerPanel()
        {
            managerItemData = new GoldManagerItemData();
            SetItemsList();
            managerItemData = new SpeedManagerItemData();
            SetItemsList();
            managerItemData = new TimeManagerItemData();
            SetItemsList();
        }
    }
}
