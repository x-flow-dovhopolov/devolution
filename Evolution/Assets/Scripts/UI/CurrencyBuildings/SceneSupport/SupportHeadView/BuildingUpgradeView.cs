﻿using System;
using Core;
using Core.ModelData.Buildings.BuildingUpgrades;
using Core.ModelData.ExceptionHierarchy;
using TMPro;
using UI.CurrencyBuildings.SceneSupport.SingleItems;
using UnityEngine;
using UnityEngine.UI;

namespace UI.CurrencyBuildings.SceneSupport.SupportHeadView
{
    public class BuildingUpgradeView : BuildingUiSupportView
    {
        /*[SerializeField] private Image BuildingImage;
        [SerializeField] private TextMeshProUGUI BuildingName;
        [SerializeField] private TextMeshProUGUI BuildingDescription;*/

        private BuildingUpgrade upgrade;
        
        private new void Start()
        {
            base.Start();
        }

        public override void SetItemsList()
        {
            BakeSingleViewForItem();

            var upgrade1 = upgrade;
            singleView.SetSingleData(upgrade1);
            itemsList.Add(singleView);
        }

        public void OpenBuildingUpgradePanel()
        {
            HeaderText.text = "Улучшение здания";
            GameManager.Instance.DestroyTutorialMask();
            SetSingleImproveIntoImprovePanelWithNewUpgrades();
            InsideBuildingUiHelper.Instance.DisableUiElementsAndActiveSecondaryPanel(gameObject);
        }
        
        private void SetSingleImproveIntoImprovePanelWithNewUpgrades()
        {
            upgrade = new ReduceSpawnTime(GameManager.Instance.Model.CurrentBuilding);
            SetItemsList();
            upgrade = new IncreaseMaxNpcOnScene(GameManager.Instance.Model.CurrentBuilding);
            SetItemsList();
            upgrade = new IncreaseSpawnedNpcLevel(GameManager.Instance.Model.CurrentBuilding);
            SetItemsList();
        }
    }
}
