﻿using System;
using Core;
using Core.ModelData.Buildings.NpcModel;
using Core.ModelData.NpcDiscover;
using Core.ModelData.Shop;
using UI.CurrencyBuildings.SceneSupport.SingleItems;
using UniRx;

namespace UI.CurrencyBuildings.SceneSupport.SupportHeadView
{
    public class ShopManagerView : BuildingUiSupportView
    {
        public ShopManager ShopManager;

        private new void Start()
        {
            HeaderText.text = LocaleRU.Generals.Find("shop").Value;
            
            CloseBtn.OnClickAsObservable().Subscribe(_ =>
            {
                gameObject.SetActive(false);
            });
            
            ShopManager = GameManager.Instance.Model.ShopManager;
            ShopManager.SingleItemDataEvent.Subscribe(SetSingleItemShop).AddTo(this);
            ShopManager.SetAvailableShopItems();
        }

        public override void SetItemsList()
        {
            return;
            BakeSingleViewForItem();
        }

        private void SetSingleItemShop(SingleNpcDiscover<NPC> shopCreatures)
        {
            var item = CreateSingleListItem() as SingleShopItemView;
            if (item != null)
            {
                item.SetShopDataWithSprite(shopCreatures, this);
                itemsList.Add(item);
            }
        }

        public void Buy(int gold, int food, int level) => ShopManager.Buy(gold, food, level);
    }
}