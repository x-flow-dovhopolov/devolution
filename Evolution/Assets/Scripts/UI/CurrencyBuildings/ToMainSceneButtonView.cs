﻿using Core;
using UI.LoadingScreen;
using UniRx.Async;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.CurrencyBuildings
{
    public class ToMainSceneButtonView : MonoBehaviour
    {
        [SerializeField] public GameObject SceneLoader = default;
        
        public void BackToMainScene()
        {
            GameManager.Instance.Model.OpenedBuildingIndex = 9999;
            
            SceneLoader.SetActive(true);
            SceneLoader.GetComponent<LoadingScreen.LoadingScreen>().SetSceneLoader(new SceneLoader());
            SceneLoader.GetComponent<LoadingScreen.LoadingScreen>().LoadSceneAsync("Adventure").Forget();
        }
    }
}
