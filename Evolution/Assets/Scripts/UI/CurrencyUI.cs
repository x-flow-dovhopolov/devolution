﻿using Core;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CurrencyUI : MonoBehaviour
    {
        public TextMeshProUGUI goldText;
        public TextMeshProUGUI gemText;
        public TextMeshProUGUI powerText;
        public TextMeshProUGUI foodText;

        private void Start()
        {    
            GameManager.Instance.Model.Gold.Subscribe(x  => { goldText.text = NumberFormat.ConvertNumberToFormat(x); }).AddTo(this);
             
            GameManager.Instance.Model.Gems.Subscribe(x  => { gemText.text = NumberFormat.ConvertNumberToFormat(x); }).AddTo(this);
            
            GameManager.Instance.Model.Power.Subscribe(x => { powerText.text = NumberFormat.ConvertNumberToFormat(x); }).AddTo(this);
            
            GameManager.Instance.Model.Food.Subscribe(x  => { foodText.text = NumberFormat.ConvertNumberToFormat(x); }).AddTo(this);
        }
    }
}
