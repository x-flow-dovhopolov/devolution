﻿using Core;
using Core.ModelData.Buildings.SupportBuildings;
using Core.ModelData.ExceptionHierarchy;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Environment
{
    public class CastleUIView: MonoBehaviour
    {
        public TextMeshProUGUI DescriptionCurrentLevel;
        public TextMeshProUGUI CastleLevel;
        public TextMeshProUGUI DescriptionNextLevel;
        public ReactiveProperty<int> GoldCost;
        public TextMeshProUGUI GoldCosts;
        public Button Upgrade;
        public Button CloseBtn;
        public Castle Castle;

        private void Start()
        {
            Castle = GameManager.Instance.Model.Castle;
            
            //DescriptionCurrentLevel.text = Castle.Description1;
            //DescriptionNextLevel.text = Castle.Description2;
            GoldCost = new ReactiveProperty<int>(Castle.GoldAmount);
            
            //GoldCost.SubscribeToText(GoldCosts);
            GoldCost.ObserveEveryValueChanged(GoldCostV => GoldCostV.Value)
                .Subscribe(x => GoldCosts.text = "" + x);
            CastleLevel.text = "level " + GameManager.Instance.Model.Castle.Level;
            GameManager.Instance.Model.Castle.LevelIncreased.Subscribe(level =>
            {
                CastleLevel.text = "level " + level;
            }).AddTo(this);
            
            Upgrade.OnClickAsObservable().Subscribe(_ => UpgradeCastle());
            CloseBtn.OnClickAsObservable().Subscribe(_ => Close());
        }

        public void UpgradeCastle()
        {
            try
            {
                Castle.Upgrade();
            }
            catch (BalanceException e)
            {
                AdventureMainUI.Instance.ShowNotEnoughMoneyPrefab(e.Description);
            }

            Close();
        }

        public void Close() => Destroy(gameObject);
    }
}