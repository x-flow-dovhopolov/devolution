﻿using Core;
using Core.ModelData.Environment;
using Core.ModelData.ExceptionHierarchy;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Environment
{
    public class EnvironmentDiscover : MonoBehaviour
    {
        [SerializeField] protected Button Close;
        [SerializeField] protected Button Repair;
        [SerializeField] public TextMeshProUGUI GoldText;
        [SerializeField] public TextMeshProUGUI FoodText;
        [SerializeField] public TextMeshProUGUI HeaderText;
        [SerializeField] public TextMeshProUGUI ButtonText;
        
        public int Gold = 0;
        public int Food = 0;
        public int Power = 0;
        public int Reward = 0;
        
        public void ClosePanel() => gameObject.SetActive(false);

        protected void Start()
        {
            Close.OnPointerClickAsObservable().Subscribe(_ => ClosePanel());
            //Repair.OnClickAsObservable().Subscribe(_ => FogDiscover());
            gameObject.SetActive(false);
        }
        
        
        private bool CheckIsEnoughCurrencyToDiscover()
        {
            try
            {
                GameManager.Instance.DeductPurchasePrice(Gold, Food, Power);
                Debug.Log("After GameManager.Instance.DeductPurchasePricel");
            }
            catch (BalanceException e)
            {
                Debug.Log($"{e.Description}");
                AdventureMainUI.Instance.ShowNotEnoughMoneyPrefab(e.Description);
                return false;
            }

            return true;
        }

        public void FogDiscover()
        {
            Debug.Log("public void FogDiscover()");
            if (CheckIsEnoughCurrencyToDiscover())
            {
                GameManager.Instance.Model.FogSlots.SetFogIsDiscovered();
                ClosePanelAfterSuccessfulDiscover();
            }
        }
        
        public void DiscoverBridge()
        {
            Debug.Log("public void DiscoverBridge()");
            if (CheckIsEnoughCurrencyToDiscover())
            {
                GameManager.Instance.Model.FogSlots.fogSlot[0].PointedPassed();
                ClosePanelAfterSuccessfulDiscover();
            }
        }
        
        public void DiscoverTomb()
        {
            Debug.Log("public void DiscoverTomb()");
            if (CheckIsEnoughCurrencyToDiscover())
            {
                GameManager.Instance.Model.FogSlots.fogSlot[2].PointedPassed();
                ClosePanelAfterSuccessfulDiscover();
            }
        }

        private void ClosePanelAfterSuccessfulDiscover()
        {
            GameManager.Instance.DestroyTutorialMask();
            ClosePanel();   
        }
    }
}