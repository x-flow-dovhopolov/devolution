﻿using UnityEngine;

namespace UI.Environment
{
    public class PositionManager : MonoBehaviour
    {
        private GameObject go;

        private void Start()
        {
            go = GameObject.Find("Scroll and LevelUp Navigator");
            transform.position = Utils.WorldToUiSpace(GameObject.Find("Canvas").GetComponent<Canvas>(),
                go.transform.position);
        }
    }
}