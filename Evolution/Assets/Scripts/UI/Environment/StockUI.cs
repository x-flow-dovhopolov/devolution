﻿using System.Collections.Generic;
using Core;
using Core.Conversation;
using Core.ModelData.StockItems;
using Core.Tutorial;
using UI.CurrencyBuildings.SceneSupport;
using UI.CurrencyBuildings.SceneSupport.SingleItems;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Environment
{
    public class StockUI : BuildingUiSupportView
    {
        private static List<StockItemView> SlotsList;
        [SerializeField] public List<StockItemView> List;
        [SerializeField] private GameObject Panel;

        private new void Start()
        {
            HeaderText.text = LocaleRU.Generals.Find("warehouse").Value;
            
            SlotsList = new List<StockItemView>();

            /*for (var i = 0; i < Panel.transform.childCount; i++)
            {
                SlotsList.Add(Panel.transform.GetChild(i).gameObject.GetComponent<StockItemView>());
            }*/
            
            CloseBtn.OnClickAsObservable().Subscribe(_ =>
            {
                if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.ExplainStock)
                {
                    GameManager.Instance.Tutor.Context._state.OnStockCloseAfterExplain.OnNext(Unit.Default);
                }
                gameObject.SetActive(false);
            });
            
            gameObject.SetActive(false);
        }

        public override void SetItemsList()
        {
            return;
        }

        private void Update()
        {
            if (!enabled) return;

            if (GameManager.Instance.Tutor.Context._state.OnStockOpened.HasObservers)
            {
                GameManager.Instance.Tutor.Context._state.OnStockOpened.OnNext(Unit.Default);
            }

            AdventureMainUI.Instance.ReenableMainCanvas();
            GameManager.Instance.DestroyTutorialMask();
        }

        public void InitItemsOnCanvas()
        {
            var val = 0;
            
            foreach (var keyValue in GameManager.Instance.Model.FruitCollection.allCollectionDictionary)
            {
                if (keyValue.Value.Count == 0) continue;

                List[val].SetItemImage(Resources.Load<Sprite>(keyValue.Value.Image));
                List[val].SetItemCount(keyValue.Value.Count);
                val++;
            }
        }
    }
}