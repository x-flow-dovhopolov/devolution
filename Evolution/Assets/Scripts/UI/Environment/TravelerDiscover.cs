﻿using System.Collections.Generic;
using Core;
using Core.ModelData.Environment.EnvironmentTypes;
using Core.ModelData.StockItems;
using TMPro;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Environment
{
    public class TravelerDiscover : EnvironmentDiscover
    {
        public StockItem item1;
        public int item1count;
        public StockItem item2;
        public int item2count;
        private List<RequiredItem> requiredList;
        [SerializeField] public TextMeshProUGUI FirstItemCountText;
        [SerializeField] public TextMeshProUGUI SecondItemCountText;
        [SerializeField] public Image FirstItemCountImage;
        [SerializeField] public Image SecondItemCountImage;

        private new void Start()
        {
            base.Start();
            Repair.OnClickAsObservable().Subscribe(_ => DiscoverTraveler());
            Close.OnClickAsObservable().Subscribe(_ => ClosePanel());
        }

        public void SetRequiredItems(List<RequiredItem> requiredItems)
        {
            requiredList = requiredItems;
            int t1 = requiredItems[0].count;
            string t2 = requiredItems[0].item.Image;
            int t3 = requiredItems[1].count;
            string t4 = requiredItems[1].item.Image;

            Debug.Log($"{t1} = {t2} / {t3} = {t4}");
            item1 = requiredItems[0].item;
            item1count = t1;
            item2 = requiredItems[1].item;
            item2count = t3;
            //Debug.Log($"t2 = {t2} and t4 = {t4}");
            FirstItemCountText.text = t1 + "";
            FirstItemCountImage.sprite = Resources.Load<Sprite>(t2);
            SecondItemCountText.text = t3 + "";
            SecondItemCountImage.sprite = Resources.Load<Sprite>(t4);
            ButtonText.text = "Помочь";
            HeaderText.text = "Жаждущий путник";
        }

        private void DiscoverTraveler()
        {
            Debug.Log(" private void DiscoverTraveler()");
            /*var fruitsUsed = false;
            var task = GameManager.Instance.Model.FruitCollection.BuyUsingRareFruitsList(requiredList);
            if (await UniTask.WhenAny(task, UniTask.Delay(500)) == (true, true ))
            {
                fruitsUsed = task.Result;
                Debug.Log($"task result = {task.Status}   / {task.Result}");
            }else { }*/
            
            var isEnoughFruits = GameManager.Instance.Model.FruitCollection.CheckIsEnoughFruits(requiredList);
            
            if (isEnoughFruits)
            {
                
                /*if (!GameManager.Instance.Model.GOOnMapSelected.FogData.IsDiscoveredProperty.Value)
                {
                    GameManager.Instance.Model.GOOnMapSelected.FogData.IsDiscoveredProperty.Value = true;
                }
                else
                {
                    GameManager.Instance.Model.GOOnMapSelected.SetPointedPassed();
                }*/
                /**/
                
                GameManager.Instance.Model.Gold.Value += Reward;
                GameManager.Instance.Model.FogSlots.fogSlot[1].PointedPassed();// SetPointedPassed();
            }
            else
            {
                //AdventureMainUI.Instance.ShowNotEnoughMoneyPrefab("Not enough fruits");
                AdventureMainUI.Instance.ShowNotEnoughMoneyPrefab(LocaleRU.Generals.Find("alert_no_resource").Value);
            }

            GameManager.Instance.DestroyTutorialMask();
            ClosePanel();
        }
    }
}