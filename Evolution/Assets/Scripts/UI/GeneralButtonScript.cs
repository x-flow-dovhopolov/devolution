﻿using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GeneralButtonScript : MonoBehaviour
    {
        [SerializeField] public Button GeneralButton;
        [SerializeField] private TextMeshProUGUI ButtonHeaderText;
        [SerializeField] private Image ButtonCurrencyOneImage;
        [SerializeField] private TextMeshProUGUI ButtonCurrencyOneText;
        [SerializeField] private Image ButtonCurrencyTwoImage;
        [SerializeField] private TextMeshProUGUI ButtonCurrencyTwoText;

        public void SetButtonData(string btnText, 
            string firstCurrencyCountText,
            string secondCurrencyCountText)
        {
            ButtonHeaderText.text = btnText;
            ButtonCurrencyOneText.text = firstCurrencyCountText;
            ButtonCurrencyTwoText.text = secondCurrencyCountText;
        }

        public void SetButtonOnClickAction(Action buttonAction)
        {
            GeneralButton.OnClickAsObservable().Subscribe(_ => buttonAction());
        }
    }
}
