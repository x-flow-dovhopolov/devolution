﻿using System.Threading;
using UniRx.Async;
using UnityEngine;

namespace UI.LoadingScreen
{
    public abstract class Dialog : MonoBehaviour
    {
        protected Animator Animator;

        public bool AutoDestroy = true;
        
        protected virtual void Awake()
        {
            Animator = GetComponent<Animator>();
            if (Animator == null)
            {
                State = DialogState.Appeared;
            }
        }

        protected virtual void Start()
        {
            if (Animator != null)
            {
                Animator.Update(0);
            }

            Appear();
        }

        public DialogState State { get; protected set; } = DialogState.Appearing;

        public UniTask WaitUntilAppearedAsync(CancellationToken ct = default)
        {
            return UniTask.WaitUntil(() => State == DialogState.Appeared, cancellationToken: ct)
                .SuppressCancellationThrow();
        }

        public UniTask WaitUntilDisappearedAsync(CancellationToken ct = default)
        {
            return UniTask.WaitUntil(() => State == DialogState.Disappeared, cancellationToken: ct)
                .SuppressCancellationThrow();
        }

        protected void Appear()
        {
            /*Statistics.Event("window_open", false,
                new Dictionary<string, object>()
                {
                    {"id", GetType().ToString().Split('.').Last()}
                });
            
            if (appearSound != SoundId.None)
                audioPlayer.PlaySound(appearSound);*/
        }

        public virtual void Disappear()
        {
            
            if (Animator != null)
            {
                State = DialogState.Disappearing;
                foreach (var animator in GetComponentsInChildren<Animator>())
                {
                    animator.SetTrigger("Disappear");
                }
            }
            else
            {
                OnDisappeared();
            }
            
        }

        protected virtual void OnAppeared()
        {
            State = DialogState.Appeared;
        }

        protected virtual void OnDisappeared()
        {
            State = DialogState.Disappeared;
            if (AutoDestroy) Destroy(gameObject);
        }
    }

    public enum DialogState
    {
        Appearing,
        Appeared,
        Disappearing,
        Disappeared
    }
}