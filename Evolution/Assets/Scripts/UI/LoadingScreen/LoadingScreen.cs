﻿using Core;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;

namespace UI.LoadingScreen
{
    public class LoadingScreen : Dialog
    {
        private static GameObject persistentCanvas;

        [SerializeField] private SceneLoader sceneLoader;

        protected override void Start()
        {
            GameManager.Instance.SceneLoader = GameObject.Find("Loading Screen");
            //GameManager.Instance.SceneLoader.SetActive(false);
            gameObject.SetActive(false);
        }

        public void SetSceneLoader(SceneLoader sceneLoader2)
        {
            sceneLoader = sceneLoader2;
        }
        
        public async UniTask LoadSceneAsync(string name)
        {
            gameObject.SetActive(true);
            sceneLoader.IsLoading = true;
            
            await WaitUntilAppearedAsync();
            
            var canvas = PersistentCanvas;
            canvas.SetActive(true);
            transform.SetParent(canvas.transform);
            
            var loadingOperation = sceneLoader.LoadSceneAsync(name);
            
            await UniTask.WaitUntil(() => loadingOperation.IsLoaded);
            
            loadingOperation.ActivateScene();
            
            await UniTask.Delay(500);
            
            Disappear();
            
            await WaitUntilDisappearedAsync();
            
            canvas.SetActive(false);
            sceneLoader.IsLoading = false;
        }

        private static GameObject PersistentCanvas
        {
            get
            {
                if (persistentCanvas != null) return persistentCanvas;
                persistentCanvas = new GameObject("Persistent Canvas");
                var canvas = persistentCanvas.AddComponent<Canvas>();
                persistentCanvas.AddComponent<GraphicRaycaster>();
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
                canvas.sortingOrder = 1000;
                DontDestroyOnLoad(persistentCanvas);
                return persistentCanvas;
            }
        }

        //public class Factory : UIFactory<LoadingScreen> { }
    }
}