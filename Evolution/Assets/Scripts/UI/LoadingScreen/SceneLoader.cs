﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.LoadingScreen
{
    public class SceneLoader //: ITickable
    {
        private readonly List<Scene> loadedScenes = new List<Scene>();
        private List<LoadOperation> currentOperations = new List<LoadOperation>();

        public SceneLoader()
        {
            SceneManager.sceneLoaded += (scene, mode) => loadedScenes.Add(scene);
            SceneManager.sceneUnloaded += scene => loadedScenes.Remove(scene);
        }

        public bool IsLoading { get; set; }

        public LoadOperation LoadSceneAsync(string name, LoadSceneMode mode = LoadSceneMode.Single)
        {
            var operation = new LoadOperation(SceneManager.LoadSceneAsync(name, new LoadSceneParameters(mode)));
            currentOperations.Add(operation);
            return operation;
        }
        
        public class LoadOperation
        {
            private readonly AsyncOperation operation;
            private readonly Subject<float> onProgressChanged = new Subject<float>();
            private float progress;

            public LoadOperation(AsyncOperation operation, bool immediate = false)
            {
                this.operation = operation;
                operation.allowSceneActivation = immediate;
                GameManager.Instance.StartCoroutine(AsynchronousLoad());
            }
            
            IEnumerator AsynchronousLoad()
            {
                yield return null;
 
                while (!operation.isDone)
                {
                    Debug.Log($"operation.progress = {operation.progress}");
                    if (operation.progress == 0.9f)
                    {
                        progress = 1f;
                        onProgressChanged.OnCompleted();
                        Debug.Log($"AsynchronousLoad finished");
                    }
 
                    yield return null;
                }
            }
            
            public bool IsLoaded => progress >= 1f;
            
            public void ActivateScene()
            {
                operation.allowSceneActivation = true;
            }
        }
    }
}