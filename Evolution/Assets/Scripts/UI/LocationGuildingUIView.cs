﻿using TMPro;
using UnityEngine;

namespace UI
{
    public class LocationGuildingUIView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI buildingLevel;
        [SerializeField] private TextMeshProUGUI buildingName;
        [SerializeField] private GameObject Mark;

        public void SetMarkEnable(bool isEnable)
        {
            Mark.SetActive(isEnable);
        }

        public void SetBuildingLevel(int level)
        {
            //Debug.Log($"castle level = P{level}");
            buildingLevel.text = level.ToString();
        }

        public void SetBuildingName(string name)
        {
           // Debug.Log($"castle name = P{name}");
            buildingName.text = name;
        }
    }
}
