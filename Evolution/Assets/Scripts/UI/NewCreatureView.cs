﻿using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class NewCreatureView : MonoBehaviour    
    {
        [SerializeField] private Image image;
        //[SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI panelHeader;
        [SerializeField] private Button close;
        [SerializeField] private Button closeOk;

        private void Start()
        {
            //TODO add to locale
            panelHeader.text = "Открыт новый рабочий";
            close.OnClickAsObservable().Subscribe(_ => Destroy(gameObject));
            closeOk.OnClickAsObservable().Subscribe(_ => Destroy(gameObject));
        }

        public void SetData(int level, Sprite sprite, string npcName)
        {
            //levelText.text = ", уровень " + level;
            var info = npcName + ", уровень " + level;
            nameText.text = info;
            image.sprite = sprite;
        }
    }
}