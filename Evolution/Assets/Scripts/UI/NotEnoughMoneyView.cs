﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class NotEnoughMoneyView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI ErrorTextPro;

        public void SetText(string message) => ErrorTextPro.text = message;
    }
}
