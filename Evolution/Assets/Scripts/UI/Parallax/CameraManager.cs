﻿using UnityEngine;

namespace UI.Parallax
{
    public class CameraManager : MonoBehaviour
    {
        Vector2 firstPressPos;
        Vector2 secondPressPos;
        float distance;
        public float correction = 1f;
        
        bool _mousePressed;
        
        private bool isGrag;
        void FixedUpdate()
        {
            if (Input.GetMouseButtonDown(0))
            {
                firstPressPos = Input.mousePosition;
                _mousePressed = true;
            }
            
            if (Input.GetMouseButtonUp(0))
            {
                _mousePressed = false;
            }

            if (_mousePressed)
            {
                secondPressPos = Input.mousePosition;
                distance = secondPressPos.x - firstPressPos.x;

                var directionMultiplier = -distance * correction;
                          
                var border = transform.position.x + directionMultiplier;
                /*Debug.Log($"distance = {distance} === directionMultiplier = {directionMultiplier}" +
                          $", border = {border}");*/
                
        
                if (border < 0 || border > 15) return;
                transform.position = new Vector3(border, 0f, -25f);
            }

            if (_mousePressed)
            {
                firstPressPos = secondPressPos;
            }
        }
    }
}