﻿using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Parallax.NewParallax
{
    public class CameraScroller : MonoBehaviour
    {
        [SerializeField] private Camera camera;
        [SerializeField] private Transform leftBorder;
        [SerializeField] private Transform rightBorder;
        [SerializeField] private Transform leftSpring;
        [SerializeField] private Transform rightSpring;
        [SerializeField] private float frictionFactor;
        [SerializeField] private float bounceTime;

        [SerializeField] private LocationView locationView;
        [SerializeField] private CameraScroller cameraScroller;
        [SerializeField] private LocationParallax locationParallax;
        
        private float cameraBorder;
        private float speed;
        private bool dragging;
        private float Position => camera.transform.position.x;
        
        public Subject<Unit> DragBeginStream { get; } = new Subject<Unit>();

        /*[Inject]
        public void Construct(
            LocationView locationView,
            CameraScroller cameraScroller,
            LocationParallax locationParallax)
        {
            this.locationParallax = locationParallax;
            this.cameraScroller = cameraScroller;
            this.locationView = locationView;
        }*/

        private void Start()
        {         
            CalculateCameraBorder();
            locationView.gameObject.OnBeginDragAsObservable().Subscribe(OnBeginDrag).AddTo(this);
            locationView.gameObject.OnDragAsObservable().Subscribe(OnDrag).AddTo(this);
            locationView.gameObject.OnEndDragAsObservable().Subscribe(OnEndDrag).AddTo(this);
        }

        public void AddDraggables(IEnumerable<GameObject> draggables)
        {
            foreach (var draggable in draggables)
            {
                draggable.OnBeginDragAsObservable().Subscribe(OnBeginDrag).AddTo(this);
                draggable.OnDragAsObservable().Subscribe(OnDrag).AddTo(this);
                draggable.OnEndDragAsObservable().Subscribe(OnEndDrag).AddTo(this);
            }
        }
        
        private void CalculateCameraBorder()
        {
            //var frustumHeight = 2.0f * -camera.transform.position.z * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
            //var frustumWidth = frustumHeight * camera.aspect;
            //cameraBorder = frustumWidth / 2f;
            
            float height = 2f * camera.orthographicSize;
            float width = height * camera.aspect;
            cameraBorder = width / 2f;
        }
                
        private void OnBeginDrag(PointerEventData eventData)
        {
            dragging = true;
            speed = 0f;
            DragBeginStream.OnNext(Unit.Default);
        }

        private void OnDrag(PointerEventData eventData)
        {
            camera.transform.Translate(Vector3.right * -eventData.delta.x * (cameraBorder*2f/Screen.width));
            speed = -eventData.delta.x * (cameraBorder * 2f / Screen.width);
        }

        private void OnEndDrag(PointerEventData eventData)
        {
            dragging = false;
        }

        private void Update()
        {
#if UNITY_EDITOR
            CalculateCameraBorder();
#endif
                     
            if (!dragging)
            {
                if (Position < leftSpring.position.x + cameraBorder)
                {
                    camera.transform.position = camera.transform.position.WithX(Mathf.SmoothDamp(Position, leftSpring.position.x + cameraBorder, ref speed, bounceTime));
                }
                else if (Position > rightSpring.position.x - cameraBorder)
                {
                    camera.transform.position = camera.transform.position.WithX(Mathf.SmoothDamp(Position, rightSpring.position.x - cameraBorder, ref speed, bounceTime));
                }                              
                else if (!Mathf.Approximately(speed, 0f))
                {
                    camera.transform.Translate(Vector3.right * speed);
                    speed = Mathf.MoveTowards(speed, 0f, Time.deltaTime*frictionFactor);
                }                
            }            
            
            camera.transform.position = camera.transform.position.WithX(Mathf.Clamp(
                camera.transform.position.x,
                leftBorder.transform.position.x + cameraBorder,
                rightBorder.transform.position.x - cameraBorder));
            
            locationParallax.SetShift(camera.transform.position.x);
        }
    }
}