﻿using UniRx;
using UnityEngine;

namespace UI.Parallax.NewParallax
{
    public class LocationClickable : MonoBehaviour
    {
        [SerializeField] private CameraScroller cameraScroller;

        private bool waitPointerUp;
        
        public Subject<Unit> ClickedStream { get; } = new Subject<Unit>();

        private void Start()
        {
            gameObject.OnPointerDownAsObservable().Subscribe(_ => { waitPointerUp = true; }).AddTo(this);
            gameObject.OnPointerUpAsObservable().Subscribe(_ =>
            {
                if (waitPointerUp) OnClick();
            }).AddTo(this);
            
            cameraScroller.DragBeginStream.Subscribe(_ => waitPointerUp = false).AddTo(this);
        }

        protected virtual void OnClick()
        {
            Debug.Log("BASE override void OnClick()");
            ClickedStream.OnNext(Unit.Default);
        }
    }
}