﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Parallax.NewParallax
{
    public class LocationParallax : MonoBehaviour
    {
        [SerializeField] private List<ParallaxLayer> layers;

        public void SetShift(float shift)
        {
            foreach (var parallaxLayer in layers)
            {
                parallaxLayer.gameObject.transform.localPosition =
                    Vector3.zero.WithX(shift * parallaxLayer.coeffecient);
            }
        }
    }

    [Serializable]
    public class ParallaxLayer
    {
        public GameObject gameObject;
        public float coeffecient;
    }
}