﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace UI.Parallax.NewParallax
{
    public class LocationView : MonoBehaviour
    {
        [SerializeField] private List<LocationObjectSettings> locationObjects;
        
        [SerializeField] private CameraScroller cameraScroller;
        
        private void Start()
        {
            cameraScroller.AddDraggables(locationObjects.Select(los => los.GameObject));
            
            foreach (var locationObject in locationObjects)
            {
                locationObject.GameObject.GetComponent<LocationClickable>().ClickedStream.Subscribe(_ =>
                {
                    ObjectClicked(locationObject.Id);
                }).AddTo(this);
            }
        }

        private void ObjectClicked(LocationObjectId objectId)
        {
            Debug.Log("Object clicked " + objectId.ToString());
        }
    }

    [Serializable]
    public class LocationObjectSettings
    {
        public LocationObjectId Id;
        public GameObject GameObject;
    }

    public enum LocationObjectId
    {
        Castle,
        Farm,
        Mill,
        Barracks,
        Tavern,
        Temple,
        Bridge,
        Traveler,
        Stock
    }
}