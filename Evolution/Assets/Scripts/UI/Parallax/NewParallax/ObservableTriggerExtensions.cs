﻿using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Parallax.NewParallax
{
    public static class ObservableTriggerExtensions
    {
        public static IObservable<PointerEventData> OnDragAsObservable(this GameObject gameObject)
        {
            if (gameObject == null) return Observable.Empty<PointerEventData>();
            return GetOrAddComponent<ObservableDragTrigger>(gameObject).OnDragAsObservable();
        }

        public static IObservable<PointerEventData> OnBeginDragAsObservable(this GameObject gameObject)
        {
            if (gameObject == null) return Observable.Empty<PointerEventData>();
            return GetOrAddComponent<ObservableBeginDragTrigger>(gameObject).OnBeginDragAsObservable();
        }

        public static IObservable<PointerEventData> OnEndDragAsObservable(this GameObject gameObject)
        {
            if (gameObject == null) return Observable.Empty<PointerEventData>();
            return GetOrAddComponent<ObservableEndDragTrigger>(gameObject).OnEndDragAsObservable();
        }
        
        public static IObservable<PointerEventData> OnPointerDownAsObservable(this GameObject gameObject)
        {
            if (gameObject == null) return Observable.Empty<PointerEventData>();
            return GetOrAddComponent<ObservablePointerDownTrigger>(gameObject).OnPointerDownAsObservable();
        }

        public static IObservable<PointerEventData> OnPointerEnterAsObservable(this GameObject gameObject)
        {
            if (gameObject == null) return Observable.Empty<PointerEventData>();
            return GetOrAddComponent<ObservablePointerEnterTrigger>(gameObject).OnPointerEnterAsObservable();
        }

        public static IObservable<PointerEventData> OnPointerExitAsObservable(this GameObject gameObject)
        {
            if (gameObject == null) return Observable.Empty<PointerEventData>();
            return GetOrAddComponent<ObservablePointerExitTrigger>(gameObject).OnPointerExitAsObservable();
        }

        public static IObservable<PointerEventData> OnPointerUpAsObservable(this GameObject gameObject)
        {
            if (gameObject == null) return Observable.Empty<PointerEventData>();
            return GetOrAddComponent<ObservablePointerUpTrigger>(gameObject).OnPointerUpAsObservable();
        }
        
        private static T GetOrAddComponent<T>(GameObject gameObject) where T : Component
        {
            var component = gameObject.GetComponent<T>();
            if (component == null)
            {
                component = gameObject.AddComponent<T>();
            }

            return component;
        }
    }
}