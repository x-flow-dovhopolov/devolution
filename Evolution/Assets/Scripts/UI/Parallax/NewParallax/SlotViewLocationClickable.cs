﻿using Core;
using GameObjects;
using UnityEngine;

namespace UI.Parallax.NewParallax
{
    public class SlotViewLocationClickable : LocationClickable
    {
        private SlotView SlotView;
        protected override void OnClick()
        {
            Debug.Log("protected override void OnClick()");
            SlotView = GetComponent<SlotView>();
            GameManager.Instance.Model.OpenedBuildingIndex = SlotView.Index;
            
            Debug.Log($"SlotView.Slot.Building.Value.BuildingType  = {SlotView.Slot.Building.Value.BuildingType }");
            if (SlotView.Slot.Building.Value.BuildingType == BuildingType.Default)
            {
                GetComponent<SlotView>().GainBuildingCreateControl();
            }
            else
            {
                GameManager.Instance.GoToAnotherScene(SlotView.Slot.Building.Value.BuildingType);
            }
        }
    }
}