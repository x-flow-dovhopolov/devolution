﻿using UnityEngine;

namespace UI.Parallax
{
    public class Parallax : MonoBehaviour
    {
        private float startPos;
        public float parallaxEffect;
        [SerializeField] private GameObject Cam;
        [SerializeField] private GameObject DragObject;
        
        void Start()
        {
            DragObject = GameObject.Find("GragManager");
            startPos = transform.position.x;
        }

        void Update()
        {
            float dist = DragObject.transform.position.x * parallaxEffect;
            //float dist = Cam.transform.position.x * parallaxEffect;
//            Debug.Log($"DragObject = {DragObject} and dist = {dist}");
            transform.position = new Vector3(startPos - dist, transform.position.y, transform.position.z);
        }
    }
}
