﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SoundButton : MonoBehaviour
    {
        public bool IsSoundOn;
        
        [SerializeField] private Sprite soundOn;
        [SerializeField] private Sprite soundOff;
        [SerializeField] private Button setVolume;
        [SerializeField] private Image btnSprite;
        [SerializeField] private MixerLink MixerLink;
        
        private void Start()
        {
            IsSoundOn = BackgroundMusicPlayer.Instance.MusicIsMute;
            MixerLink = GameObject.Find("BackgroundMusicPlayer").GetComponent<MixerLink>();
            setVolume.OnClickAsObservable().Subscribe(_ => SetVolume()).AddTo(gameObject);

            SetVolume();
        }

        private void SetVolume()
        {
            if (IsSoundOn)
            {
                btnSprite.sprite = soundOff;
                IsSoundOn = false;
                MixerLink.SliderValueChange(-60, true);
            }
            else
            {
                btnSprite.sprite =  soundOn;
                IsSoundOn = true;
                MixerLink.SliderValueChange(2, false);
            }
        }
    }
}