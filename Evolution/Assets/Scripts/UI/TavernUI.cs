﻿using System.Collections;
using Core;
using Core.Tutorial;
using DG.Tweening;
using Tavern;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class TavernUI : MonoBehaviour
    {
        public static TavernUI Instance;
        public Button backBtn;
        public GameObject cap1;
        public GameObject cap2;
        public GameObject cap3;

        public GameObject reward1;
        public GameObject reward2;
        public GameObject reward3;
        public Button PlayButton;
        public TextMeshProUGUI Text;

        [SerializeField] public GameObject Hint;
        
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this) 
                Destroy(gameObject);
        }

        public void BackToMainScene()
        {
            //GameManager.Instance.Tutor.Context._state.OnTavernGamePlayed.OnNext(Unit.Default);
            if (GameManager.Instance.Tutor.Context._state.OnTavernGamePlayed.HasObservers)
                GameManager.Instance.Tutor.Context._state.OnTavernGamePlayed.OnNext(Unit.Default);

            SceneManager.LoadScene("Adventure");
        }

        public void PlayGame()
        {
            PlayButton.interactable = false;
            Text.text = "Play 0/1";

            //OnEnableNewGame();
            cap1.GetComponent<Game>().MySequence();
            cap2.GetComponent<Game>().MySequence();
            cap3.GetComponent<Game>().MySequence();
        }

        public void Open(GameObject selectedCap)
        {
            SetCapsCollider(false);

            selectedCap.transform.DOMoveY(1f, 1f).OnComplete(() =>
            {
                if (GameManager.Instance.Tutor.Context._state.OnTavernGamePlayed.HasObservers)
                    GameManager.Instance.Tutor.Context._state.OnTavernGamePlayed.OnNext(Unit.Default);
                OpenOtherCaps();
                backBtn.interactable = true;
            });
            selectedCap.GetComponent<Game>().reward.GetComponent<SpriteRenderer>().enabled = true;
        }

        private void OpenOtherCaps() => StartCoroutine(OpenOther());

        private IEnumerator OpenOther()
        {
            yield return new WaitForSeconds(1f);

            cap1.GetComponent<Game>().OpenAfterEffect();
            cap2.GetComponent<Game>().OpenAfterEffect();
            cap3.GetComponent<Game>().OpenAfterEffect();
        }

        private void SetCapsCollider(bool isEnable)
        {
            cap1.GetComponent<BoxCollider2D>().enabled = isEnable;
            cap2.GetComponent<BoxCollider2D>().enabled = isEnable;
            cap3.GetComponent<BoxCollider2D>().enabled = isEnable;
        }
    }
}