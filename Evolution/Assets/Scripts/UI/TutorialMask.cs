using System.Linq;
using UniRx;
using UnityEngine;

namespace UI
{
    public class TutorialMask : MonoBehaviour
    {
        [SerializeField] private GameObject mask = default;

        public void SetTarget(GameObject target, float scale)
        {
            var canvasRectTransform = FindObjectsOfType<Canvas>()
                .First(c => c.gameObject.name == "Canvas")
                .GetComponent<RectTransform>();
            var camera = Camera.main;
            var maskRectTransform = mask.GetComponent<RectTransform>();

            mask.transform.localScale = Vector3.one * scale;

            if (target.GetComponent<RectTransform>() != null)
            {
                var targetRectTransform = target.GetComponent<RectTransform>();
                SetRectPosition();
                Observable.EveryUpdate().Subscribe(_ => { SetRectPosition(); }).AddTo(gameObject);

                void SetRectPosition()
                {
                    maskRectTransform.position = targetRectTransform.position;
                }
            }
            else
            {
                SetPosition();
                Observable.EveryUpdate().Subscribe(_ => { SetPosition(); }).AddTo(gameObject);

                void SetPosition()
                {
                    if (target == null) return;

                    Vector2 viewportPosition = camera.WorldToViewportPoint(target.transform.position);
                    var sizeDelta = canvasRectTransform.sizeDelta;
                    Vector2 worldObjectScreenPosition = new Vector2(
                        ((viewportPosition.x * sizeDelta.x) -
                         (sizeDelta.x * 0.5f)),
                        ((viewportPosition.y * sizeDelta.y) -
                         (sizeDelta.y * 0.5f)));
                    maskRectTransform.anchoredPosition = worldObjectScreenPosition;
                }
            }
        }
    }
}