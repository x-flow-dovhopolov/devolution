﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCanvasView : MonoBehaviour
{
    void Start()
    {
        GetComponent<Canvas>().additionalShaderChannels = AdditionalCanvasShaderChannels.None;
        //Debug.Log($"{GetComponent<Canvas>().additionalShaderChannels}");
    }
}
