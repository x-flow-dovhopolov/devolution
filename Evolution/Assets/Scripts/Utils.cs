using System.Linq;
using UI;
using UnityEngine;

public static class Utils
{
    public static void CanvasAlign(GameObject canvasObject, GameObject targetObject)
    {
        var canvasRectTransform = GameObject.FindObjectsOfType<Canvas>()
            .First(c => c.gameObject.name == "Canvas")
            .GetComponent<RectTransform>();
        var camera = Camera.main;
        var objectRectTransform = canvasObject.GetComponent<RectTransform>();

        if (targetObject.GetComponent<RectTransform>() != null)
        {
            var targetRectTransform = targetObject.GetComponent<RectTransform>();
            objectRectTransform.position = targetRectTransform.position;
        }
        else
        {
            Vector2 viewportPosition = camera.WorldToViewportPoint(targetObject.transform.position);
            var sizeDelta = canvasRectTransform.sizeDelta;
            Vector2 worldObjectScreenPosition = new Vector2(
                ((viewportPosition.x * sizeDelta.x) - (sizeDelta.x * 0.5f)),
                ((viewportPosition.y * sizeDelta.y) - (sizeDelta.y * 0.5f)));
            objectRectTransform.anchoredPosition = worldObjectScreenPosition;
        }
    }
    
    public static Vector3 WorldToUiSpace(Canvas parentCanvas, Vector3 worldPos)
    {
        var screenPos = Camera.main.WorldToScreenPoint(worldPos);
        
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos,
            parentCanvas.worldCamera, out var movePos);

        return parentCanvas.transform.TransformPoint(new Vector3(movePos.x, movePos.y));
    }
}