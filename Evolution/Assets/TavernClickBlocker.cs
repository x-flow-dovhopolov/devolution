﻿using Core;
using Core.Tutorial;
using UnityEngine;

public class TavernClickBlocker : MonoBehaviour
{
    private PolygonCollider2D objectCollider;
    
    private void Start()
    {
        objectCollider = GetComponent<PolygonCollider2D>();
    }

    void Update()
    {
        if (GameManager.Instance.Tutor.CurrentStep == ETutorialStep.BuyTavern)
        {
            objectCollider.enabled = true;
        }
    }
}
