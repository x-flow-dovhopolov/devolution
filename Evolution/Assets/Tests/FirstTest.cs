﻿using System.Collections;
using Core;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests
{
    public class FirstTest
    {
        private GameObject gameManager;
        
        [SetUp]
        public void SetUp()
        {
            SceneManager.LoadScene("Start scene");
        }

        [TearDown]
        public void SetDefaultArea()
        {
            //SceneManager.LoadScene("Start scene");
        }
        
        [UnityTest]
        public IEnumerator CheckSceneName1()
        {
            Assert.AreEqual("Start scene", SceneManager.GetActiveScene().name);
            yield return null;
        }
        
        [UnityTest]
        public IEnumerator CheckSceneName()
        {
            gameManager = GameObject.Find("GameManager");
            Assert.AreNotEqual(null, gameManager);
            yield return null;
        }
        
        /*[UnityTest]
        public IEnumerator TestGoToAnotherScene()
        {
            gameManager = GameObject.Find("GameManager");
            var exepting = EditorSceneManager.GetActiveScene().name;
            gameManager.GetComponent<GameManager>().GoToAnotherScene(BuildingType.Default);
            yield return null;
            Assert.AreEqual(exepting, EditorSceneManager.GetActiveScene().name);
        }
        
        [UnityTest, Ignore("no experience")]
        public IEnumerator TestGoToAnotherSceneFarm()
        {
            gameManager = GameObject.Find("GameManager");
            var exepting = "Farm";
            gameManager.GetComponent<GameManager>().GoToAnotherScene(BuildingType.Farm);
            yield return null;
            Assert.AreEqual(exepting, EditorSceneManager.GetActiveScene().name);
        }
        
        [UnityTest, Ignore("no experience")]
        public IEnumerator TestGoToAnotherSceneMill()
        {
            gameManager = GameObject.Find("GameManager");
            var exepting = "Mill";
            gameManager.GetComponent<GameManager>().GoToAnotherScene(BuildingType.Mill);
            yield return null;
            Assert.AreEqual(exepting, EditorSceneManager.GetActiveScene().name);
        }
        
        [UnityTest, Ignore("no experience")]
        public IEnumerator TestGoToAnotherSceneBarrack()
        {
            var exepting = "Warrior Guild";
            gameManager.GetComponent<GameManager>().GoToAnotherScene(BuildingType.Barracks);
            yield return null;
            Assert.AreEqual(exepting, EditorSceneManager.GetActiveScene().name);
        }*/
        
    }
}
