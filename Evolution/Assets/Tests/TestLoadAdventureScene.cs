﻿using System.Collections;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestLoadAdventureScene
    {
        [SetUp]
        public void SetUp()
        {
            SceneManager.LoadScene("Start scene");
        }
        [TearDown]
        public void TearDown()
        {
            //EditorSceneManager.LoadScene("Start scene");
            //yield return new WaitForSeconds(1f);
        }
        [UnityTest]
        public IEnumerator Test1()
        {
            yield return new WaitForEndOfFrame(); // Awake
            yield return new WaitForEndOfFrame(); // Start
            yield return null;
            Assert.AreEqual("Adventure", EditorSceneManager.GetActiveScene().name);
        }
        
        [UnityTest]
        public IEnumerator Test2()
        {
            yield return new WaitForEndOfFrame(); // Awake
            yield return new WaitForEndOfFrame(); // Start
            yield return null;
            Assert.AreEqual("Adventure", EditorSceneManager.GetActiveScene().name);
        }

        /*[UnityTest]
        public IEnumerator TutorialHelperTest()
        {
            SceneManager.LoadScene("Start scene");
            yield return null;
            yield return null;
            //yield return new WaitForSeconds(0.2f);
            var go = GameObject.Find("TutorialHelper");
            Assert.NotNull(go);
        }
        
        [UnityTest]
        public IEnumerator AdventureSceneTutorialHelperTest()
        {
            SceneManager.LoadScene("Start scene");
            yield return null;
            var go = GameObject.Find("TutorialHelper").GetComponent<AdventureSceneTutorialHelper>();
            Assert.NotNull(go);
        }
        
        [UnityTest]
        public IEnumerator CheckSceneNameAfterOneSecond()
        {
            SceneManager.LoadScene("Start scene");
            yield return null;
            //yield return new WaitForSeconds(0.2f);
            Assert.AreEqual("Adventure", EditorSceneManager.GetActiveScene().name);
        }
        
        [UnityTest]
        public IEnumerator FindGmBuildMenuPanel()
        {
            SceneManager.LoadScene("Start scene");
            //yield return new WaitForSeconds(0.2f);
            yield return null;
            var go = GameObject.Find("GameManager").GetComponent<GameManager>();
            Assert.IsNotNull(go.BuildMenuPanel);
        }*/
        
        
    }
}