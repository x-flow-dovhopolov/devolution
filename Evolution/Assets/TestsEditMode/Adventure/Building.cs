﻿using System.Collections;
using Core;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.TestTools;

namespace TestsEditMode.Adventure
{
    public class Building
    {
        private GameObject gameManager;
        
        [SetUp]
        public void SetUp()
        {
            EditorSceneManager.OpenScene("Assets/Scenes/Adventure.unity");
            gameManager = new GameObject("GameManager");
            gameManager.AddComponent<GameManager>();
        }
        
        [TearDown]
        public void TearDown()
        {
            Object.DestroyImmediate(gameManager);
        }
        
        [UnityTest]
        public IEnumerator Building_Child_Count()
        {
            yield return null;
            var go = GameObject.Find("Buildings");
            Assert.That(7, Is.EqualTo(go.transform.childCount));
        }
        
        [UnityTest]
        public IEnumerator Environment_Child_Count()
        {
            yield return null;
            var go = GameObject.Find("Environment");
            Assert.That(4, Is.EqualTo(go.transform.childCount));
        }
        
    }
}