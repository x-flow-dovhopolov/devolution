﻿using System.Collections;
using Core;
using Core.ModelData.ExceptionHierarchy;
using Core.Tutorial;
using NUnit.Framework;
using UniRx;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.TestTools;

namespace TestsEditMode.Adventure.ScriptsTest
{
    public class GameManagerTest
    {
        private GameManager gameManager;
        
        [SetUp]
        public void SetUp()
        {
            EditorSceneManager.OpenScene("Assets/Scenes/Adventure.unity");
            gameManager = new GameObject().AddComponent<GameManager>();
            GameManager.Instance = gameManager;
            
            LocaleRU.Initialize(Resources.Load("LocaleRU").ToString());
            
            var targetFile = Resources.Load<TextAsset>("GameSetting");
            if (targetFile != null)
            {
                GameSetting.Initialize(targetFile.text);
            }
            gameManager.Tutor = new TutorialManager();
            gameManager.Model = new Model();
            /*gameManager = new GameObject().AddComponent<GameManager>();
            GameManager.Instance = gameManager;
            
            var targetFile = Resources.Load<TextAsset>("GameSetting");
            if (targetFile != null)
            {
                GameSetting.Initialize(targetFile.text);
            }
            
            gameManager.Tutor = new TutorialManager(true);
            gameManager.Model = new Model();*/
        }
        
        [TearDown]
        public void TearDown()
        {
            Object.DestroyImmediate(gameManager);
        }
        
        [Test]
        public void CheckGameManager()
        {
            Assert.NotNull(gameManager);
        }
        
        [Test]
        public void CheckGameManagerModel()
        {
            Assert.NotNull(gameManager.Model);
        }
        
        [Test]
        public void CheckForEnoughMoney1()
        {
            var tmp = gameManager.CheckForEnoughMoney(0, 0, 0);
            Assert.IsTrue(tmp);
        }
        
        [Test]
        public void CheckForEnoughMoney2()
        {
            Assert.That(() => gameManager.CheckForEnoughMoney(0, 100, 100), Throws.InstanceOf<BalanceException>());
        }
        
        [Test]
        public void CheckForEnoughMoney3()
        {
            gameManager.Model.Gold.Value = 100;
            Assert.That(() => gameManager.CheckForEnoughMoney(101), Throws.InstanceOf<BalanceException>());
        }
        
        
        [Test]
        public void CheckForEnoughMoney4()
        {
            gameManager.Model.Gold.Value = 101;
            var tmp = gameManager.CheckForEnoughMoney(101);
            Assert.IsTrue(tmp);
        }
        
        [Test]
        public void CheckForEnoughMoney5()
        {
            gameManager.Model.Gold.Value = 0;
            var tmp = gameManager.CheckForEnoughMoney(0);
            Assert.IsTrue(tmp);
        }
        
        
        [Test]
        public void CheckForEnoughMoney6()
        {
            gameManager.Model.Gold.Value = 6666;
            gameManager.Model.Food.Value = 6666;
            gameManager.Model.Power.Value = 6666;
            var tmp = gameManager.CheckForEnoughMoney(6666, 6666, 6666);
            Assert.IsTrue(tmp);
        }
        
        [Test]
        public void CheckForEnoughMoney7()
        {
            gameManager.Model.Gold.Value = 2;
            gameManager.Model.Food.Value = 2;
            gameManager.Model.Power.Value = 2;
            Assert.That(() => gameManager.CheckForEnoughMoney(-5, 2, 3), Throws.InstanceOf<BalanceException>());
        }
        
        [Test]
        public void CheckDeductMoney()
        {
            gameManager.Model.Gold.Value = 2;
            gameManager.Model.Food.Value = 2;
            gameManager.Model.Power.Value = 2;
            gameManager.DeductPurchasePrice(2, 2, 2);
            Assert.That(gameManager.Model.Gold.Value, Is.EqualTo(0));
            Assert.That(gameManager.Model.Food.Value, Is.EqualTo(0));
            Assert.That(gameManager.Model.Power.Value, Is.EqualTo(0));
        }
        
        [Test]
        public void CheckDeductMoney2()
        {
            gameManager.Model.Gold.Value = 2;
            gameManager.Model.Food.Value = 2;
            gameManager.Model.Power.Value = 2;
            
            Assert.That(() => gameManager.DeductPurchasePrice(2, 2, 3), Throws.InstanceOf<BalanceException>());
        }
        
        [Test]
        public void CheckDeductMoney3()
        {
            gameManager.Model.Gold.Value = 2;
            gameManager.Model.Food.Value = 2;
            gameManager.Model.Power.Value = 2;
            
            gameManager.DeductPurchasePrice(0, 0, 0);
            Assert.That(gameManager.Model.Gold.Value, Is.EqualTo(2));
            Assert.That(gameManager.Model.Food.Value, Is.EqualTo(2));
            Assert.That(gameManager.Model.Power.Value, Is.EqualTo(2));
        }
        
        
        [Test]
        public void CheckDeductMoney4()
        {
            gameManager.Model.Gold.Value = 9999;
            gameManager.Model.Food.Value = 9999;
            gameManager.Model.Power.Value = 9999;
            
            gameManager.DeductPurchasePrice(9999, 9999, 9999);
            Assert.That(gameManager.Model.Gold.Value, Is.EqualTo(0));
            Assert.That(gameManager.Model.Food.Value, Is.EqualTo(0));
            Assert.That(gameManager.Model.Power.Value, Is.EqualTo(0));
        }
        
        
        [Test]
        public void CheckDeductMoney5()
        {
            gameManager.Model.Gold.Value = 9999;
            gameManager.Model.Food.Value = 9999;
            gameManager.Model.Power.Value = 9999;
            
            gameManager.DeductPurchasePrice(9999, 9999);
            Assert.That(gameManager.Model.Gold.Value, Is.EqualTo(0));
            Assert.That(gameManager.Model.Food.Value, Is.EqualTo(0));
            Assert.That(gameManager.Model.Power.Value, Is.EqualTo(9999));
        }
    }
}