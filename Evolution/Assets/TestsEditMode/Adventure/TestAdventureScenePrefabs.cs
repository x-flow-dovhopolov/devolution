﻿using System.Collections;
using Core;
using Core.Tutorial;
using NUnit.Framework;
using UI;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.TestTools;

namespace TestsEditMode.Adventure
{
    public class TestAdventureScenePrefabs
    {
        private GameObject gameManager;
        
        [SetUp]
        public void SetUp()
        {
            EditorSceneManager.OpenScene("Assets/Scenes/Adventure.unity");
            gameManager = new GameObject("GameManager");
            gameManager.AddComponent<GameManager>();
        }
        
        [TearDown]
        public void TearDown()
        {
            Object.DestroyImmediate(gameManager);
        }
        
        [Test]
        public void FirstTest()
        {
            Assert.AreEqual("Adventure", EditorSceneManager.GetActiveScene().name);
        }
        
        [UnityTest]
        public IEnumerator FindGm()
        {
            yield return null;
            var go = GameObject.Find("GameManager");
            Assert.NotNull(go);
        }
        
        [UnityTest]
        public IEnumerator FindGmBuildMenuPanel()
        {
            yield return null;
            var go = GameObject.Find("GameManager").GetComponent<GameManager>();
            Assert.IsNull(go.BuildMenuPanel);
        }
        
        [UnityTest]
        public IEnumerator FindTutorialHelper()
        {
            yield return null;
            var go = GameObject.Find("TutorialHelper");
            UnityEngine.Assertions.Assert.IsNotNull(go);
        }
        
        [UnityTest]
        public IEnumerator FindTutorialHelperSize()
        {
            yield return null;
            var go = GameObject.Find("TutorialHelper").GetComponent<AdventureSceneTutorialHelper>();
            Assert.GreaterOrEqual(11, go.List.Count);
        }
        
        [UnityTest]
        public IEnumerator FindCanvas()
        {
            yield return null;
            var go = GameObject.Find("Canvas").GetComponent<AdventureMainUI>();
            UnityEngine.Assertions.Assert.IsNotNull(go);
        }
        
        [UnityTest]
        public IEnumerator FindCanvasStockPanel()
        {
            yield return null;
            var go = GameObject.Find("Canvas").GetComponent<AdventureMainUI>();
            UnityEngine.Assertions.Assert.IsNotNull(go.StockPanel);
        }
        
        [UnityTest]
        public IEnumerator FindCanvasNotEnoughMoney()
        {
            yield return null;
            var go = GameObject.Find("Canvas").GetComponent<AdventureMainUI>();
            UnityEngine.Assertions.Assert.IsNotNull(go.NotEnoughMoney);
        }
        
        [UnityTest]
        public IEnumerator FindCanvasBuilding()
        {
            yield return null;
            var go = GameObject.Find("Canvas").GetComponent<AdventureMainUI>();
            UnityEngine.Assertions.Assert.IsNotNull(go.Building);
        }
        
        [UnityTest]
        public IEnumerator FindCanvasSelectedPrefab()
        {
            yield return null;
            var go = GameObject.Find("Canvas").GetComponent<AdventureMainUI>();
            UnityEngine.Assertions.Assert.IsNull(go.SelectedPrefab);
        }
        
        //[UnityTest]
        public IEnumerator FindCanvasChildAndNullCheck()
        {
            yield return null;
            var go = GameObject.Find("Currency panel");
            UnityEngine.Assertions.Assert.IsNotNull(go);
            
            var go3 = GameObject.Find("Cheat");
            UnityEngine.Assertions.Assert.IsNotNull(go3);

            var g6 = GameObject.Find("Repair bridge");
            UnityEngine.Assertions.Assert.IsNotNull(g6);
            
            var g7 = GameObject.Find("Traveler panel");
            UnityEngine.Assertions.Assert.IsNotNull(g7);
            
            var g8 = GameObject.Find("Fog panel");
            UnityEngine.Assertions.Assert.IsNotNull(g8);
        }
    }
}