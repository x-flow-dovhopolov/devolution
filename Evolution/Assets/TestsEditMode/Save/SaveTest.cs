﻿using System.IO;
using Core;
using Core.SaveLoad;
using Core.Tutorial;
using NUnit.Framework;
using UnityEngine;

namespace TestsEditMode.Save.Save
{
    public class SaveTest : MonoBehaviour
    {
        private GameManager gameManager;
        private SaveManager _saveManager;
        private TutorialManager _tutorialManager;
        private readonly string path = Path.Combine(Application.dataPath, "PlayerData.json");
        
        [SetUp]
        public void SetUp()
        {
            gameManager = new GameObject().AddComponent<GameManager>();
            GameManager.Instance = gameManager;
            
            LocaleRU.Initialize(Resources.Load("LocaleRU").ToString());
            
            var targetFile = Resources.Load<TextAsset>("GameSetting");
            if (targetFile != null)
            {
                GameSetting.Initialize(targetFile.text);
            }
            gameManager.Tutor = new TutorialManager();
            gameManager.Model = new Model();
            
            _saveManager = gameManager.Model.GetSaveManager();
            _tutorialManager = gameManager.Tutor;
        }
        
        [TearDown]
        public void TearDown()
        {
            SaveManager.ClearPlayerProgress();
            DestroyImmediate(gameManager);
        }
        
        [Test]
        public void CheckGameManager() =>  Assert.NotNull(gameManager);
        
        [Test]
        public void CheckSaveManager() => Assert.NotNull(_saveManager);
        
        [Test]
        public void CheckSaveManageClearProgress() => Assert.IsTrue(File.Exists(path));

        [Test] 
        public void SaveManager_load() => Assert.IsTrue(File.Exists(path));
        
        [Test] 
        public void SaveManager_load_after_clear()
        {
            var playerData = _saveManager.GetLoad();
            Assert.NotNull(playerData);
        }
        
        [Test] 
        public void SaveManager_load_currency__after_clear()
        {
            var goldExpected = 0;
            var foodExpected = 0;
            var powerExpected = 0;
            
            var playerData = _saveManager.GetLoad();
            Assert.AreEqual(goldExpected, playerData.Gold);
            Assert.AreEqual(foodExpected, playerData.Food);
            Assert.AreEqual(powerExpected, playerData.Power);
        }
        
        [Test] 
        public void SaveManager_load_projectVersion()
        {
            var playerData = _saveManager.GetLoad();
            Assert.AreEqual("", playerData.ProjectVersion);
            Assert.AreEqual(0, playerData.TimeOnApplicationExit);
        }
        
        [Test] 
        public void SaveManager_load_tutorial_step_1()
        {
            gameManager.Tutor.NextStep(ETutorialStep.BuyMill);
            var playerData = _saveManager.GetLoad();
            Assert.AreEqual(1, (int)playerData.TutorialManagerData.CurrentStep);
        }
        
        [Test] 
        public void SaveManager_load_tutorial_step_2()
        {
            gameManager.Tutor.NextStep(ETutorialStep.GoToMill);
            var playerData = _saveManager.GetLoad();
            Assert.AreEqual(2, (int)playerData.TutorialManagerData.CurrentStep);
        }
        
        [Test] 
        public void SaveManager_load_tutorial_step_3()
        {
            gameManager.Tutor.NextStep(ETutorialStep.NpcCreate);
            var playerData = _saveManager.GetLoad();
            Assert.AreEqual(3, (int)playerData.TutorialManagerData.CurrentStep);
        }
        
        [Test] 
        public void SaveManager_load_tutorial_step_4()
        {
            gameManager.Tutor.NextStep(ETutorialStep.CreateSecondNpc);
            var playerData = _saveManager.GetLoad();
            Assert.AreEqual(4, (int)playerData.TutorialManagerData.CurrentStep);
        }
        
        [Test] 
        public void SaveManager_load_tutorial_step_5()
        {
            gameManager.Tutor.NextStep(ETutorialStep.TapOnNpc);
            var playerData = _saveManager.GetLoad();
            Assert.AreEqual(5, (int)playerData.TutorialManagerData.CurrentStep);
        }
        
        [Test] 
        public void SaveManager_load_tutorial_bool_flags()
        {
            _tutorialManager.CurrentStep = ETutorialStep.BuyMillConversation;
                
            _saveManager.SavePlayerProgress();
            var playerData = _saveManager.GetLoad();

            var savedTutorial = playerData.TutorialManagerData;
            Assert.AreEqual(0, (int)savedTutorial.CurrentStep);
        }
    }
}
