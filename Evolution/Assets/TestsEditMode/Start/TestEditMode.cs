﻿using System.Collections;
using GameObjects;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.TestTools;

namespace TestsEditMode.Start
{
    public class EditModeTest
    {
        private GameObject gameManager;
        private ImageDatabase imageDatabase;
        private int length = 6;
        
        [SetUp]
        public void FindGameManager()
        {
            EditorSceneManager.OpenScene("Assets/Scenes/Start scene.unity");
            gameManager = GameObject.Find("GameManager");
            imageDatabase = gameManager.GetComponent<ImageDatabase>();
        }
        
        [TearDown]
        public void AfterEveryTest()
        {
        }
        
        [Test]
        public void StartAndCheckGamaManager()
        {
            Assert.NotNull(gameManager);
        }
        
        [Test]
        public void CheckImageDatabase()
        {
            Assert.NotNull(imageDatabase);
        }
        
        [Test]
        public void CheckImageDatabaseMillArray() => Assert.NotNull(imageDatabase.MillSpriteList);
        [Test]
        public void CheckImageDatabaseFarmArray() => Assert.NotNull(imageDatabase.FarmSpriteList);
        [Test]
        public void CheckImageDatabaseBarrackArray() => Assert.NotNull(imageDatabase.BarrackSpriteList);
        
        [Test]
        public void CheckImageDatabaseBarrackArrayLength()
        {
            Assert.AreEqual(imageDatabase.BarrackSpriteList.Count, length);
        }

        [Test]
        public void CheckImageDatabaseMillArrayLength()
        {
            Assert.AreEqual(imageDatabase.MillSpriteList.Count, length);
        }
        
        [Test]
        public void CheckImageDatabaseFarmArrayLength()
        {
            Assert.AreEqual(imageDatabase.FarmSpriteList.Count, length);
        }

        [Test]
        public void CheckFindMethod()
        {
            var expectedName = "Sprite";
            var sprite = imageDatabase.Find("Mill", 1);
            Assert.AreEqual(expectedName,sprite.GetType().Name);
        }

        [Test]
        public void CheckFoundSpriteName()
        {
            var sprite = imageDatabase.Find("Mill", 0);
            Assert.AreEqual(sprite, null);
        }
              
        [Test]
        public void CheckFoundSpriteName1()
        {
            var expectedName = "bunny_1";
            var sprite = imageDatabase.Find("Mill", 1);
            Assert.AreEqual(expectedName, sprite.name);
        }
              
        [Test]
        public void CheckFoundSpriteName6()
        {
            var expectedName = "pig_6";
            var sprite = imageDatabase.Find("Mill", 6);
            Assert.AreEqual(expectedName, sprite.name);
        }
        
        [Test]
        public void CheckFoundFarmSpriteName1()
        {
            var expectedName = "Wheel_1";
            var sprite = imageDatabase.Find("Farm", 1);
            Assert.AreEqual(expectedName, sprite.name);
        }
              
        [Test]
        public void CheckFoundFarmSpriteName6()
        {
            var expectedName = "Watermelon_6";
            var sprite = imageDatabase.Find("Farm", 6);
            Assert.AreEqual(expectedName, sprite.name);
        }
        
        
        [Test]
        public void CheckFoundBarrackSpriteName1()
        {
            var expectedName = "Hedgehog_1";
            var sprite = imageDatabase.Find("Barrack", 1);
            Assert.AreEqual(expectedName, sprite.name);
        }
              
        [Test]
        public void CheckFoundBarrackSpriteName6()
        {
            var expectedName = "Mole_6";
            var sprite = imageDatabase.Find("Barrack", 6);
            Assert.AreEqual(expectedName, sprite.name);
        }
        
        [UnityTest]
        public IEnumerator EditModeTestWithEnumeratorPasses()
        {
            Assert.NotNull( gameManager.GetComponent<ImageDatabase>());
            yield return null;
        }
    }
}