﻿using System.Collections;
using Core;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.TestTools;

namespace TestsEditMode.Start
{
    public class TestGameManager
    {
        private GameManager gameManager;

        [SetUp]
        public void FindGameManager()
        {
            EditorSceneManager.OpenScene("Assets/Scenes/Start scene.unity");
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }

        [TearDown]
        public void AfterEveryTest() => EditorSceneManager.OpenScene("Assets/Scenes/Start scene.unity");

        [Test]
        public void TutorialMaskPrefab() => Assert.NotNull(gameManager.tutorialMaskPrefab);
        [Test]
        public void ConversationUiPrefab() => Assert.NotNull(gameManager.conversationUIPrefab);
        [Test]
        public void ConversationUiPrefabBlocker() => Assert.NotNull(gameManager.conversationUIPrefabBrocker);
        [Test]
        public void HintUiPrefab() => Assert.NotNull(gameManager.hintUIPrefab);
        [Test]
        public void BuildMenuPanel() => Assert.Null(gameManager.BuildMenuPanel);
        [Test]
        public void Reward() => Assert.NotNull(gameManager.Reward);

        [Test]
        public void CheckGameSetting()
        {
            var targetFile = Resources.Load<TextAsset>("GameSetting");
            Assert.AreNotEqual(null, targetFile);
        }
        
        [UnityTest]
        public IEnumerator CheckSceneName()
        {
            EditorSceneManager.OpenScene("Assets/Scenes/Adventure.unity");
            yield return null;
            Assert.AreEqual("Adventure", EditorSceneManager.GetActiveScene().name);
        }
    }
}