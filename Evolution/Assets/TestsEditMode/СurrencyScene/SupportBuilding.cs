﻿using System.Collections;
using Core;
using GameObjects.Buildings.BuildingTypeView;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.TestTools;

namespace TestsEditMode.СurrencyScene
{
    public class SupportBuilding
    {
        private GameObject gameManager;
        
        [SetUp]
        public void SetUp()
        {
            EditorSceneManager.OpenScene("Assets/Scenes/CurrencyScene.unity");
            gameManager = new GameObject("GameManager");
            gameManager.AddComponent<GameManager>();
        }
        
        [TearDown]
        public void TearDown()
        {
            Object.DestroyImmediate(gameManager);
        }
        
        [UnityTest]
        public IEnumerator FindGm()
        {
            yield return null;
            var go = GameObject.Find("Building").GetComponent<BuildingView>();
            Assert.NotNull(go.buildingLevel);
        }
    }
}