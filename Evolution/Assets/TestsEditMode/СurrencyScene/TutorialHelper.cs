﻿using System.Collections;
using Core;
using Core.Tutorial;
using NUnit.Framework;
using UI.CurrencyBuildings;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.TestTools;

namespace TestsEditMode.СurrencyScene
{
    public class TutorialHelper
    {
        private GameObject gameManager;
        
        [SetUp]
        public void SetUp()
        {
            EditorSceneManager.OpenScene("Assets/Scenes/CurrencyScene.unity");
            gameManager = new GameObject("GameManager");
            gameManager.AddComponent<GameManager>();
        }
        
        [TearDown]
        public void TearDown()
        {
            Object.DestroyImmediate(gameManager);
        }
        
        [UnityTest]
        public IEnumerator FindGm()
        {
            yield return null;
            var go = GameObject.Find("GameManager");
            Assert.NotNull(go);
        }
        
        [UnityTest]
        public IEnumerator CheckSceneName()
        {
            yield return null;
            Assert.AreEqual("Mill", EditorSceneManager.GetActiveScene().name);
        }
        
        [UnityTest]
        public IEnumerator CheckTutorialHelper()
        {
            yield return null;
            var go = GameObject.Find("TutorialHelper");
            UnityEngine.Assertions.Assert.IsNotNull(go);
        }
        
        [UnityTest]
        public IEnumerator CheckTutorialHelperScript()
        {
            yield return null;
            var go = GameObject.Find("TutorialHelper");
            UnityEngine.Assertions.Assert.IsNotNull(go.GetComponent<MillTutorialHelper>());
        }
        
        [UnityTest]
        public IEnumerator FindCanvas()
        {
            yield return null;
            var go = GameObject.Find("Canvas");
            UnityEngine.Assertions.Assert.IsNotNull(go);
        }
        
        [UnityTest]
        public IEnumerator FindCanvasScript()
        {
            yield return null;
            var go = GameObject.Find("Canvas").GetComponent<InsideBuildingUiHelper>();
            UnityEngine.Assertions.Assert.IsNotNull(go);
        }
        
        [UnityTest]
        public IEnumerator FindCanvasChildAndNullCheck()
        {
            yield return null;
            
            var go2 = GameObject.Find("Currency panel");
            UnityEngine.Assertions.Assert.IsNotNull(go2);
            
            var go3 = GameObject.Find("Back to Adventure scene button");
            UnityEngine.Assertions.Assert.IsNotNull(go3);
            
            var go4 = GameObject.Find("Building support");
            UnityEngine.Assertions.Assert.IsNotNull(go4);
            
            var g7 = GameObject.Find("NPC Shop");
            UnityEngine.Assertions.Assert.IsNull(g7);
            
            var go = GameObject.Find("Not enough money");
            UnityEngine.Assertions.Assert.IsNull(go);
            
            var go5 = GameObject.Find("Building upgrades"); // SetActive(false) 
            UnityEngine.Assertions.Assert.IsNull(go5);
            
            var g6 = GameObject.Find("Building managers");// SetActive(false) 
            UnityEngine.Assertions.Assert.IsNull(g6);
            
            var g8 = GameObject.Find("Canvas").transform.childCount;
            Assert.That(8, Is.EqualTo(g8));
        }
    }
}